<script type="text/javascript">
    function cambiarpass(){

	       var comando=$('#txtcomand').val();
	       var idusuario=$('#txtusuar').val();
	       var usuario=$('#txtsuariocc').val();
	       var password=$('#txtpasscc').val();
	       var passwordc=$('#txtpassccc').val();

	       if (comando==1)
	       {
	       		if (usuario)
	       		{
	       			if (password)
	       			{
			       		if (password==passwordc)
			       		{
					       var dataString = 'idusuario='+ idusuario +'&usuario='+ usuario +'&password='+ password;
					       $("#error3").show();
					       $("#error3").fadeIn(400).html('<img src="images/loading.gif" />');
					       $.ajax({
					             type: "POST",
					             url: "actualizarpass.php",
					             data: dataString,
					             cache: false,
					             success: function(result){
					             var result=trim(result);
					             $("#error3").hide();
					             if(result==1){
					                  $("#txtsuariocc").attr("disabled", "disabled");
					                  $("#txtpasscc").attr("disabled", "disabled");
					                  $("#txtpassccc").attr("disabled", "disabled");
					                  $("#btcambiar").attr("disabled", "disabled");
					                  $("#btcambiar").css({ background: "#666" });
					                  $("#errorMessage3").css({ color: "#1461C6" });
					                  $("#errorMessage3").html("Se ha cambiado la contraseña con éxito.");
					             }else{
					             	  $("#errorMessage3").css({ color: "red" });
					                  $("#errorMessage3").html(result);
					             }
					        	 }
					  		});
					    }else{
					 		$("#errorMessage3").css({ color: "red" });
			         		$("#errorMessage3").html("Las contraseñas no coinciden.");
			         		$("#txtpasscc").focus();

					    }
					}else{
						$("#errorMessage3").css({ color: "red" });
	         			$("#errorMessage3").html("Debe llenar el campo contraseña.");
	         			$("#txtpasscc").focus();
					}
				}else{
					$("#errorMessage3").css({ color: "red" });
	         		$("#errorMessage3").html("Debe llenar el campo Usuario.");
	         		$("#txtsuariocc").focus();
				}
	      }else{
	      	 $("#errorMessage3").css({ color: "red" });
	         $("#errorMessage3").html("Error de Validación interna.");
	      }
	}

</script> 

<div style="line-height:27px;">
	<input type="hidden" id="txtcomand" name="txtcomand" value="<?=$_GET['olvpass'] ?>" />
	<input type="hidden" id="txtusuar" name="txtusuar" value="<?=$_GET['u'] ?>"/>
	<span>Estimado usuario, Ingrese los siguientes datos:</span>
	<br/>
	<span style="font-weight:bold;">Usuario: &nbsp;</span>
	&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="txtsuariocc" name="txtsuariocc"  size="16" />
	<br/>
	<span style="font-weight:bold;">Password: &nbsp;</span>
	<input type="password" id="txtpasscc" name="txtpasscc"  size="16" />
	<br/>
	<span style="font-weight:bold;">Confirmar Password: &nbsp;</span>
	<input type="password" id="txtpassccc" name="txtpassccc"  size="16" />

	<div id="errorMessage3" class="errorMessage" style="height: 68px;float:left;    width: 260px;">
		<div id="error3" class="error" style="display: none;">
		
		</div>
	</div>
	<div style="float:right;">
		<input type="button" id="btcambiar" name="submit" style="border: 1px solid white; " value="Actualizar" class="boton" onclick="javascript:cambiarpass()">
	</div>
</div>