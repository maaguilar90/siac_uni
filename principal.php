<?php
@session_start();

//die($_SESSION['nameusuario']);
if ($_SESSION['codusuario']==""){ header("Location: index.php"); }
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include('header.php'); ?>
<?php
	$titulo= new datosgenerales();
	$titulo= $titulo->getDatos();
?>
<?php include('menusuperior.php'); ?>
<div id="menulateral" class="menulateral">
<?php include('menulateral.php'); ?>	
</div>
<body>
<div id="contenido" class="contenido">
	<div id="procesando" class="procesando"></div>
	<div id="inicio" class="inicio">
		
		<?php 
			$contenido= new clasesprincipales();
			$contenido= $contenido->getClases(@$_GET['admin']);
			include($contenido);
		?>
	</div>
	
</div>
<input type="hidden"  id="menu" class="menu" value=""/>
<div id="footer" class="footer" align="center">
<?php include('footer.php'); ?>
</div>
</body>
</html>
