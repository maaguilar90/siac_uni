<?php
	$idusuario=$_SESSION['codusuario'];
	$usuario=$_SESSION['nameusuario'];
	$nombres=$_SESSION['nombres'];
	$apellidos=$_SESSION['apellidos'];
	$correo=$_SESSION['correo'];
	$fechanac=$_SESSION['fechanac'];
	$idrol=$_SESSION['idrol'];
	$empresa=$_SESSION['empresa'];
	$cedula=$_SESSION['cedula'];

	$fecha=new funciones();
	$fecha= $fecha->formatoFechaPer($fechanac,'-');
	$fechanac=$fecha;
?>
 <div class="easyui-panel" style="padding:5px; margin-top: 5px;width:100%">
        <a href="javascript: $('#ff').submit();" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'">Guardar</a>
 </div>

<br>
<div>
	
	<div>

		<div id="errorMessage" class="errorMessage" style="line-height: 15px;"></div>
		<div id="error" class="error" style="display:none;    line-height: 15px;"></div>
		<div id="registrado" style="display:none ;    line-height: 15px; margin-bottom: 10px; padding: 5px 5px 3px; width: 240px;    margin-top: 6px;   border: 1px solid#ccc; background: -webkit-gradient(linear,left top,left bottom,from(#FAFAFA),to(#eaeaea));" >
		</div>

   <form id="ff" method="post"  enctype="multipart/form-data">
		<div style="line-height: 25px;  float: lxeft; padding-right: 30px;">
			<div>
				<span class="titulo">Mi perfil</span>
			</div>
			<div style="padding-top: 5px;">
				<span>Avatar: </span><br>
                         <img id="image1" style="height:64px; width=64px;  float: left; padding-right: 20px;" src="<?=$_SESSION['avatar']?>" />
                        <br><input id="f1" value="<?=@$_SESSION['avatar'] ?>" class="f1 easyui-filebox" name="file" style="width:280px;" data-options="
                            prompt:'Seleccione la imagen...',buttonText:'Seleccionar',required:true, missingMessage:'Campo Obligatorio',
                            onChange: function(value){
                                var f = $(this).next().find('input[type=file]')[0];
                                if (f.files && f.files[0]){
                                    var reader = new FileReader();
                                    reader.onload = function(e){
                                        $('#image1').attr('src', e.target.result);
                                    }
                                    reader.readAsDataURL(f.files[0]);
                                }
                            }">
				<br>
			</div>
			<div style="padding-top: 30px;">
				<span>Usuario</span>
				<input id="txtusuario" name="txtusuario" class="easyui-textbox" type="text" size="25" value="<?=$usuario?>" disabled="disabled" />
				<br>
			</div>
			
			<div style="padding-top: 5px;">
				<span>Contraseña</span>
				 <input id="txtpass" name="txtpass" class="easyui-validatebox"  type="password" size="25" data-options="required:true, missingMessage:'Campo Obligatorio',validType:'Length[3]'"/>
				
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Confirmar Contraseña</span>
				 <input id="txtconfpass" name="txtconfpass" class="easyui-validatebox"  type="password" size="25" data-options="required:true, missingMessage:'Campo Obligatorio',validType:'Length[3]'"/>
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Rol</span>
				<select id="sltroles" name="sltroles"   disabled="disabled" class="easyui-combobox" data-options="editable:false">
					<?php 
						$query="CALL SL_ROLES(0,1);";
						$sqlquery= executeQuery($query,"");
						while($fila=mysqli_fetch_object($sqlquery)){
					           $idrolg=$fila->id_rol; 
							   $descripcion=$fila->descripcion;
							   ?>
							   	<option id="<?=$idrolg?>" <?php if($idrolg==$idrol){ echo 'selected="selected"'; } ?> ><?=$descripcion?></option>
							   <?php
						} 
					?>
				</select>
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Cédula</span>
			    <input id="txtcedula" name="txtcedula"  class="easyui-textbox" value="<?=$cedula?>" type="text" size="25" maxlength="10"  value=""  data-options="required:true, missingMessage:'Campo Obligatorio',validType:'CedulaLength[10]'"/>
        		
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Nombres</span>
				<input id="txtnombres" name="txtnombres" class="easyui-textbox" value="<?=$nombres?>"  type="text" size="25" value="" data-options="required:true, missingMessage:'Campo Obligatorio',validType:'Length[3]'"/>

				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Apellidos</span>
				<input id="txtapellidos" name="txtapellidos" class="easyui-textbox" value="<?=$apellidos?>"  type="text" size="25" value="" data-options="required:true, missingMessage:'Campo Obligatorio',validType:'Length[3]'"/>
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Correo</span>
				 <input id="txtcorreo" name="txtcorreo" class="easyui-textbox" value="<?=$correo?>" type="email" size="25" value=""  validType="email" data-options="required:true, missingMessage:'Campo Obligatorio',invalidMessage:'Ingrese un correo válido.'"/>
				<br>
			</div>
			<div style="padding-top: 5px;">			 
						<span>Fecha de Nacimiento: </span>
						<input id="txtfechanac" name="txtfechanac" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser,required:true" style="height:26px" value="<?=($fechanac)?>">
						
						<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Empresa</span>
				<select id="sltempresa" name="sltempresa" disabled="disabled" class="easyui-combobox" data-options="editable:false">
					<?php 
						$query="CALL SP_SL_EMPRESA(1);";
						$sqlquery= executeQuery($query,"");
						while($fila=mysqli_fetch_object($sqlquery)){
					           $idempresa=$fila->idempresa; 
							   $desempresa=$fila->descripcion;
							   ?>
							   	<option id="<?=$idempresa?>" <?php if($empresa==$desempresa){ echo 'selected="selected"'; } ?> ><?=$desempresa?></option>
							   <?php
						} 
					?>
				</select>
				<br>
			</div>
					
			
		
		</div>
		</form>
	</div>	
</div>
<script type="text/javascript">
    $.extend($.fn.validatebox.defaults.rules, {
    CedulaLength: {
        validator: function(value, param){
            //alert(param[0]);
            //console.log(value.length);
            if (value.length == param[0])
            {
                //console.log(1);
                return true;
            }else{
                //console.log(2);
                return false;

            }
            
        },
        message: 'Número de Cédula Incorrecto.'
    }, Length: {
        validator: function(value, param){
            //alert(param[0]);
            //console.log(value.length);
            if (value.length >= param[0])
            {
                //console.log(1);
                return true;
            }else{
                //console.log(2);
                return false;

            }
            
        },
        message: 'Valor Incorrecto.'
    }
});

     $(function(){

$('#ff').form({
            url:'usuarios/actualizarperfil.php',
            onSubmit:function(){
             //console.log('dd')

                return $(this).form('validate');
            },
            success:function(data){
                $.messager.alert('Información', data, 'info',function()
                    {

                        $('#dlg').dialog('close');
                       location.reload();
                    });

            },
            error:function(data){
                $.messager.alert('Información', 'Error al actualizar.', 'info');
            }
        });

    });

</script>