<?php include_once('../../conexion.php'); ?>
<?php include_once('../../class/clasesgenerales.class.php'); ?>

<div style="border: 1px solid #ccc; padding-top: 5px;padding-right: 8px;padding-left: 8px;padding-bottom: 5px; margin-top: 3px;">
 	<a onclick="javascript:agregar_pago()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'">Guardar</a>
	
</div>
<div>
	<div>
		<form method="post" id="frmpago">
		<div style="line-height: 40px;   padding-right: 10px; margin-top:10px;">
		<table border="0" style="border:none;">
			<tr style="border:none;">
				<td style="border:none;"><span>Cédula</span></td>
				<td style="border:none;"><input id="txtcedula" name="txtcedula"  size="25" maxlength="10"  value="" /> <a id="dd" onclick="javascript:verificarEst()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-man'" title="Info Estudiante"></a></td>
				</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Estudiante:</span></td>
				<td style="border:none;">  
					<select id="sltestudiante" name="sltestudiante" class="easyui-combobox" name="state" label="Estudiante:"  labelPosition="top" style="width:98%;">
						<option value="0">&nbsp;</option>
						<?php 
							$inputsced="";
							$query="CALL SP_SL_ESTUDIANTES(0,1);";
							$sqlquery= executeQuery($query,"");
							while($fila=mysqli_fetch_object($sqlquery)){
						           $id=$fila->id_alumno; 
								   $descripcion=$fila->apellidos.' '.$fila->nombres;
								   $cedula=$fila->cedula;
								   $inputsced=$inputsced."<input type='hidden' id='estud".$id."' value='".$cedula."'>"
								  ?>
								   	<option value="<?=$id?>" cedula="0930178462"><?=$descripcion?></option>
								   <?php
							} 
						?>
					</select>
					<?php echo  $inputsced; ?>
				</td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Nivelación: </td>
				<td style="border:none;">
					<select id="sltmodulo" name="sltmodulo" style="width:98%;"  class="easyui-combobox" data-options="editable:false">

						<?php 
							$query="CALL SP_SL_CURSONIV(1);";
							$sqlquery= executeQuery($query,"");
							while($fila=mysqli_fetch_object($sqlquery)){
						           $id=$fila->id; 
								   $descripcion=$fila->descripcion;
								  ?>
								   	<option value="<?=$id?>" ><?=$descripcion?></option>
								   <?php
							} 
						?>
					</select>
				</td>
				</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Rubro: </td>
					<td style="border:none;">
					<select id="sltrubroniv" name="sltrubroniv" style="width:98%;" class="easyui-combobox"  data-options="editable:false">
						<?php 
							$query="CALL SP_SL_RUBROSNIV(1,1);";
							$sqlquery= executeQuery($query,"");
							while($fila=mysqli_fetch_object($sqlquery)){
						           $id=$fila->id; 
								   $descripcion=$fila->descripcion;
								  ?>
								   	<option value="<?=$id?>" ><?=$descripcion?></option>
								   <?php
							} 
						?>
					</select>
				</td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Tipo pago:&nbsp; </span></td>
				<td style="border:none;">
					<select id="slttipopago" name="slttipopago" style="width:98%;" name="slttipodoc" class="easyui-combobox"  data-options="editable:false" >
						<option value="1">Abono</option>
						<option value="2" selected="selected">Pago Total</option>
					</select>
				</td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Tipo Doc.:&nbsp; </span></td>
				<td style="border:none;">
					<select id="slttipodoc" name="slttipodoc" style="width:98%;" name="slttipodoc" class="easyui-combobox"  data-options="editable:false" >
						<option value="1">Factura</option>
						<option value="2" >Nota de Venta</option>
						<option value="3" selected="selected">Comprobante</option>
					</select>
				</td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Valor:&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtvalor" name="txtvalor" class="easyui-numberbox" type="text"  data-options="min:0,precision:2" value="" size="25" style="    width: 98%;" /></td>
			</tr>
			
	</table>

	
			
		</div>
		</form>
	</div>	
	 
</div>
<style type="text/css">
	.spaninfo
	{
		color: #white;
	}

</style>


<script type="text/javascript">
var cedulause=false;

	$('#txtcedula').on('keypress', function (e) {
         if(e.which === 13){
            obtener_datos('estudiantes',$('#txtcedula').val(),'');
         }
   });
	$('#txtvalor').numberbox({
	    filter: function(e){
	    	if (e.keyCode == 13){  // #
	    		//console.log(e.keyCode);
	    		if ($('#txtvalor').numberbox('getValue')>0)
	    		{
	    			$('#frmpago').submit();
	    			return true;    		
	    		}else{
	    			return false;
	    		}
	    		
	    		
	    	} else {
	    		return $.fn.numberbox.defaults.filter.call(this, e);
	    	}
	    },
	    // ...
	})

	function agregar_pago()
	{
		if ($('#txtvalor').numberbox('getValue')>0)
	    		{
	    			$('#frmpago').submit();
	    			return true;    		
	    		}else{
	    			return false;
	    		}
	}
   
	function obtener_datos(action,parametro1,campo)
	{
		var data=null;
		var response=false;
		console.log('function obtener_datos->Request');
		jQuery.ajax({
			type: "POST",
			url: "class/funciones.post.php",
			data: {
		        'accion': action, 
		        'campo': campo,
		        'parametro1': parametro1
		    },
 
			cache: false,
			success: function(result){
				response=true;
				var result = JSON.parse(result);
				if(result){
				  console.log(result);
				  if (campo=='cedula')
				  {
				  	$('#txtcedula').val(result);
				  	

				  }else{
				  	cedulause=true;
				  	verPagos($('#txtcedula').val())
				  	$('#sltestudiante').combobox('setValue',result);
				  	//$('#txtvalor').textbox('textbox').focus().select();
				  	$('#sltestudiante').combobox('textbox').focus().select();
				  	 cedulause=false;

				  }
				  
				 
				  $('#sltmodulo').focus()
				}else{
				  //console.log(result);  
				  //$('#txtnombres').textbox('textbox').focus().select();
				  //$('#txtnombres').focus()
				}
			}
		});
		if (response==false){ $('#sltestudiante').combobox('setValue',''); }

	}



$(function(){
	var tipopagoini= $('#slttipopago').combobox('getText');
	var moduloini= $('#sltmodulo').combobox('getText');
	var rubroini= $('#sltrubroniv').combobox('getText');
	var tipodocini= $('#slttipodoc').combobox('getText');

$('#frmpago').form({
            url:'facturacion/pagos/insertarpago.php',
            onSubmit:function(){
                var espere=mensaje('Espere...','procesar','100px','');
                //console.log('Inicio Form')
                return $(this).form('validate');
            },
            success:function(data){
                //console.log(data)
                if (data==1)
                {
                    //console.log(data)

                    mensaje('Pago ingresado con éxito.','ok','','');
                    imprimir();
                    $('#frmpago').form('clear');
                    $('#sltrubroniv').combobox('setValue', rubroini); 
                    $('#slttipopago').combobox('setValue', tipopagoini); 
                    $('#sltmodulo').combobox('setValue', moduloini); 
                    $('#slttipodoc').combobox('setValue', tipodocini);
                    $('#txtcedula').focus();
                }else{

                    mensaje('Error al ingresar el pago','error','','');
                }
                //$.messager.alert('Información', data, 'info',function()
                  //  {

                       // $('#dlg').dialog('close');
                    //});

            },
            error:function(data){
                $.messager.alert('Información', 'Error al actualizar.', 'info');
            }/*,
            beforeSend:function(data){
                console.log('ddd');
            }*/
        });

    });
 
function imprimir()
{
	var fechapago=fecha_actual();
	var cedula= $('#txtcedula').val();
	var estudiante= $('#sltestudiante').combobox('getText');
	var tipopago= $('#slttipopago').combobox('getText');
	var modulo= $('#sltmodulo').combobox('getText');
	var rubro= $('#sltrubroniv').combobox('getText');
	var tipodoc= $('#slttipodoc').combobox('getText');
	var valor=$('#txtvalor').numberbox('getValue');
	var usuarioc="<?=$_SESSION['nameusuario']?>";

	var html="<div style='line-height:25px; margin-left: 15px;'><strong>Capacitaciones Alvarado - Comprobante de pago</strong> <br>Fecha de Pago:" + fechapago+"<br>";
	html=html+"Cedula: "+cedula+"<br>Estudiante: "+estudiante+"<br>Módulo: "+modulo+"<br>";
	html=html+"Rubro: "+rubro+"<br>Tipo Pago: "+tipopago+"<br>Valor: "+valor+"<br>";
	html=html+/*"Saldo: --<br>"+*/"Usuario: "+usuarioc+"<br><strong>Nota:</strong> --</div>";
	CreateFormPage('Impresión Comprobante',html);
}


// StrPrintName print task name
// PrintDatagrid will print DataGrid
function CreateFormPage(strPrintName, printTicket) {
    window.showModalDialog(url+"facturacion/pagos/print.htm", printTicket,
    "location:No;status:No;help:No;dialogWidth:300px;dialogHeight:300px;scroll:auto;");
}

window.showModalDialog = function (url, arg, feature) {
        var opFeature = feature.split(";");
       var featuresArray = new Array()
        if (document.all) {
           for (var i = 0; i < opFeature.length - 1; i++) {
                var f = opFeature[i].split("=");
               featuresArray[f[0]] = f[1];
            }
       }
        else {

            for (var i = 0; i < opFeature.length - 1; i++) {
                var f = opFeature[i].split(":");
               featuresArray[f[0].toString().trim().toLowerCase()] = f[1].toString().trim();
            }
       }



       var h = "200px", w = "400px", l = "100px", t = "100px", r = "yes", c = "yes", s = "no";
       if (featuresArray["dialogheight"]) h = featuresArray["dialogheight"];
        if (featuresArray["dialogwidth"]) w = featuresArray["dialogwidth"];
       if (featuresArray["dialogleft"]) l = featuresArray["dialogleft"];
        if (featuresArray["dialogtop"]) t = featuresArray["dialogtop"];
        if (featuresArray["resizable"]) r = featuresArray["resizable"];
       if (featuresArray["center"]) c = featuresArray["center"];
      if (featuresArray["status"]) s = featuresArray["status"];
        var modelFeature = "height = " + h + ",width = " + w + ",left=" + l + ",top=" + t + ",model=yes,alwaysRaised=yes" + ",resizable= " + r + ",celter=" + c + ",status=" + s;

        var model = window.open(url, "", modelFeature, null);

       model.dialogArguments = arg;

    }
$.extend($.fn.combobox.defaults.inputEvents, {
	focus: function(e){
		var target = this;
		var len = $(target).val().length;
		setTimeout(function(){
			if (target.setSelectionRange){
				target.setSelectionRange(0, len);
			} else if (target.createTextRange){
				var range = target.createTextRange();
				range.collapse();
				range.moveEnd('character', len);
				range.moveStart('character', 0);
				range.select();
			}
		},0);
	}
})

 

 $('#sltestudiante').combobox({
        onChange: function(value){ 
          	console.log(cedulause);
          	if (cedulause==false)
          	{
          		console.log($('#sltestudiante').combobox('getText'));
	          	var cedula = $('#estud'+value).val();
	          	if (cedula){ $('#txtcedula').val(cedula); console.log(cedula); cargarRubros(cedula);	 }
	          	
	          	
	          	//obtener_datos('estudiantes',$('#txtcedula').val(),'cedula');
	          	//actualizarGrid('#lista','facturacion/pagos/listapagos.query.php?idc='+value)
          	}

          	
}});
//var tb = $('#addfaktur1_bukti').combobox('textbox');
/*var tb = $('#txtvalor').numberbox('textbox');
tb.focus();
tb.bind('blur', function(e){
    alert('blur');
});
*/

$("#txtcedula").focus();


</script>
<script type="text/javascript">//<![CDATA[
$(window).load(function(){
	//console.log('cargo');
	//$("#txtcedula").focus();
/*var tb = $('#addfaktur1_bukti').combobox('textbox');
tb.focus();
tb.bind('blur', function(e){
    alert('blur');
});
*/
});//]]> 

function verificarEst()
{
	var cedula=$('#txtcedula').val();
	var estudiante=$('#sltestudiante').combobox('getText');
	if (cedula)
	{
		if (estudiante)
		{
			verPagos(cedula)
			
		}
	}
}

function verPagos(cedula)
	{
		var data=null;
		var response=false;
		console.log('function verPagos->Request');
		jQuery.ajax({
			type: "POST",
			url: "facturacion/pagos/consultapagos.query.php",
			data: {
		        'cedula': cedula,
		        'limite': 1
		    },
 
			cache: false,
			success: function(result){
				response=true;
				//var result = JSON.parse(result);
				if(result!=0){
				  console.log(result);
				  
					var str = result;
				var jsonObj = $.parseJSON('[' + str + ']');
				//console.log(jsonObj[0]['fecha'])
				 var fecha=(jsonObj[0]['fecha']);
				 var modulo=(jsonObj[0]['modulo']);
				 var valor=(jsonObj[0]['valor']);
				 var tipopago=(jsonObj[0]['tipopago']);

				 var content='<span style="font-weight: bold">Fecha Último Pago: </span><span>'+fecha+'</span><br><span style="font-weight: bold">Pago Módulo: </span><span>'+modulo+'</span><br><span style="font-weight: bold">Valor: </span><span>'+valor+'</span><br><span style="font-weight: bold">Tipo de Pago: </span><span>'+tipopago+'</span>';
				  	$('#dd').tooltip({
			    position: 'right',
			    content: content,
			    onShow: function(){
			        $(this).tooltip('tip').css({
			            backgroundColor: 'white',
			            borderColor: '#666'
			        });
			    }
			});
				}else{
				 var content='<span style="font-weight: bold">Fecha Último Pago: </span><span>No encontrado</span><br><span style="font-weight: bold">Pago Módulo: </span><span>No encontrado</span><br><span style="font-weight: bold">Valor: </span><span>No encontrado</span><br><span style="font-weight: bold">Tipo de Pago: </span><span>No encontrado</span>';
				  	$('#dd').tooltip({
			    position: 'right',
			    content: content,
			    onShow: function(){
			        $(this).tooltip('tip').css({
			            backgroundColor: 'white',
			            borderColor: '#666'
			        });
			    }
			});

				}
			}
		});


		//if (response==false){ $('#sltestudiante').combobox('setValue',''); }

	}








</script>