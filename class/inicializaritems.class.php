<?php
@session_start('imag');
//include('../Mysqllocal.php');
class menusuperior
{
	public $idmenu;
	public $nombre;
	public $link;
	public $accion;
	private $menu;

	public function getMenuHtml($admin)
	{
		  $menu=null;
		  if (!isset($_SESSION['menusuperior'])){ $this->extraerMenu();  }
		  
		  foreach($_SESSION['menusuperior'] as $key=>$value){
		  


		  if (in_array(@$admin,$value->clases))
		  {
		  	$_SESSION['idmenuactivo']= $value->idmenu;
			$style='background: linear-gradient(#388FCB,#388FCB); color: white; border-bottom: 1px solid #388FCB;';
			$stylenot='background: white; color: #1461C6;'; 
		  }else{
		  	  if ($admin=='' && ($value->nombre=='INICIO') )
			  { 
			  	$_SESSION['idmenuactivo']= $value->idmenu;
			  	$style='background: linear-gradient(#388FCB,#388FCB); color: white; border-bottom: 1px solid #388FCB;';
			  	$stylenot=''; 
			  }else{
			  	$style='';
		  		$stylenot='';
			  }
		  	
		  }
		  
		  $visible=true;
		  if ($value->nombre=='MANTENIMIENTO' && !$_SESSION['mantenimientos']){	$visible=false; }
		  if ($value->nombre=='SISTEMAS' && !$_SESSION['sistema']){	$visible=false; }
		  if ($value->nombre=='GESTION COMERCIAL' && !$_SESSION['visitasclientes']){	$visible=false; }
		  if ($value->nombre=='PROYECTOS' && !$_SESSION['proyectos']){	$visible=false; }

		  if ($visible)
		  {
			  $menu.='<li class="menuprincipal" onclick="'.$value->accion.'" style="'.$style.'">'.$value->nombre;
			  if ($value->especial)
			  { 
					$menu.='&nbsp;<span id="identnotificaciones" class="circulo" style="display:none;'.$stylenot.'"></span>';
			  }else{
				  	$menu.='</li>';
			  } 
		  }
		  	
		}

		return $menu;  
			
	}
 
	private function extraerMenu()
	{
			$menusuperior= array();
			$query="CALL SP_SL_MENUSUPERIOR(1);";
			$sqlquery= executeQuery($query);
			while($fila=mysqli_fetch_object($sqlquery))
			{
		           $idmenu=$fila->idmenu; 
				   $descripcion=$fila->descripcion; 
				   $enlace=$fila->enlace; 
				   $especial=$fila->especial; 
				   $orden=$fila->orden; 
				   $imagen=$fila->imagen; 
				   $permiso=$fila->permiso; 
				   $accion=$fila->accion; 
				   $estatus=$fila->estatus; 
				   
				   $itemclase=null;
				   $itemclase=new ArrayObject();
				   $clases=array();
				   $queryclase="CALL SP_SL_MENUCLASE(".$idmenu.",1);";
				   $sqlqueryclase= executeQuery($queryclase);
				   while($filaclase=mysqli_fetch_object($sqlqueryclase))
				   {
				   		
				   		//echo $filaclase->clase;
				   		//$itemclase->clase=$filaclase->clase;
				   		array_push($clases,$filaclase->clase);
				   }

				  // var_dump($itemclase);
				  
				   $item=new ArrayObject();
				   $item-> idmenu=$idmenu;
				   $item-> nombre=$descripcion;
				   $item-> link= $enlace;
				   $item-> accion=$accion;
				   $item-> especial=$especial;
				   $item-> clases=$clases;
				 
				   array_push($menusuperior ,$item);
			}
			$_SESSION['menusuperior']= $menusuperior;
	}
		

}


class menulateral
{
	public $idmenu;
	public $nombre;
	public $link;
	public $accion;
	private $menu;

	public function getMenuHtml($admin)
	{
		  $menu=null;
		  if (!isset($_SESSION['menulateral'])){ $this->extraerMenu();  }
		  
		  foreach($_SESSION['menulateral'] as $key=>$value){
		  
		  if ($value->get==$admin)
		  {  
			$style='background: linear-gradient(#388FCB,#388FCB);color: white;border-bottom: 1px solid #388FCB; ';
			
		  }else{
		  	
			  	$style='';
		  			  
		  }
		  
		  if (@$_SESSION['idmenuactivo']==$value->idmenusuperior)
		  {
		  	$menu.='<li><a href="'.$_SESSION['SITE'].'/principal.php?admin='.$value->link.'" style="'.$style.'">'.$value->nombre.'</a></li>';
			  
		  }
		  /*$visible=true;
		  if ($value->nombre=='MANTENIMIENTO' && !$_SESSION['mantenimientos']){	$visible=false; }
		  if ($value->nombre=='SISTEMAS' && !$_SESSION['sistema']){	$visible=false; }
		  if ($value->nombre=='GESTION COMERCIAL' && !$_SESSION['visitasclientes']){	$visible=false; }
		  if ($value->nombre=='PROYECTOS' && !$_SESSION['proyectos']){	$visible=false; }
*/
		 
			  
		  
		  	
		}

		return $menu;  
			
	}
 
	private function extraerMenu()
	{
			$menulateral= array();
			$query="CALL SP_SL_MENULATERAL(0,1);";
			$sqlquery= executeQuery($query);
			while($fila=mysqli_fetch_object($sqlquery))
			{
				   $idmenu=$fila->idmenulateral; 
		           $idmenusup=$fila->idmenu; 
				   $descripcion=$fila->descripcion; 
				   $enlace=$fila->enlace; 
				   $especial=$fila->especial; 
				   $orden=$fila->orden; 
				   $imagen=$fila->imagen; 
				   $get=$fila->enlace; 
				   $accion=$fila->accion; 
				   $estatus=$fila->estatus; 
				   
				

				  // var_dump($itemclase);
				  
				   $item=new ArrayObject();
				   $item-> idmenu=$idmenu;
				   $item-> idmenusuperior=$idmenusup;
				   $item-> nombre=$descripcion;
				   $item-> link= $enlace;
				   $item-> accion=$accion;
				   $item-> especial=$especial;
				   $item-> get=$get;
				 
				   array_push($menulateral ,$item);
			}
			$_SESSION['menulateral']= $menulateral;
	}
		

}


class clasesprincipales
{
	public $idclass;
	public $get;
	public $url;

	public function getClases($get)
	{
		if (!isset($_SESSION['clasesprincipal'])){ $this->extraerClases();  }

			$include=false;
			foreach($_SESSION['clasesprincipal'] as $key=>$value){
			  if (@$get==$value->get)
			  {
			  		$url=$value->url;
			  		$include=true;
			  }	
			}
			if (!$include) $url='resumen/index.php'; 
		return $url;

	}

	private function extraerClases()
	{
			$clasesprincipal= array();
			$query="CALL SP_SL_CLASESPRINCIPAL(1);";
			$sqlquery= executeQuery($query);
			while($fila=mysqli_fetch_object($sqlquery))
			{
		           $id=$fila->id; 
				   $get=$fila->get2; 
				   $url=$fila->url; 
				  
				   $item=new ArrayObject();
				   $item-> id=$id;
				   $item-> get=$get;
				   $item-> url= $url;

				   array_push($clasesprincipal ,$item);
			}
			$_SESSION['clasesprincipal']= $clasesprincipal;
	}
}

class datosgenerales
{
	public $titulo;
	public $version;

	public function getDatos()
	{
		if (!isset($_SESSION['titulo'])){ $this->extraerDatos();  }
		return $_SESSION['titulo'];
	}

	private function extraerDatos()
	{
		$query= "CALL DATOS_CONFIG_SEL();";
	    $res=executeQuery($query);
	    $Datos=mysqli_fetch_object($res);
	    $_SESSION['titulo']=$Datos->titulo;
	}
}

	/*	$menusup= new menulateral();
		$menusup= $menusup->getMenuHtml('admin');
		//var_dump($_SESSION['menusuperior']);
		echo $menusup;*/

		?>