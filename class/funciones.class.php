<?php

class funcionesPost
{
	public function getEstudiantes($estudiante=null)
	{
		if ($estudiante)
		{
			$estudiantes=$this->extraerEstuadiantesBD($estudiante);
		}else{
			$estudiantes=null;
		}
		return $estudiantes;
	}

	public function getRubros($rubro=null)
	{
		if ($rubro)
		{
			$rubro=$this->extraerRubrosBD($rubro);
		}else{
			$rubro=null;
		}
		return $rubro;
	}

	private function extraerRubrosBD($nivelacion=null,$campo=null)
	{
		@$query= "CALL SP_SL_RUBROSNIV('".$nivelacion."',1);";
        @$res=executeQuery($query,'');
        if (@$res)
        {
        	$valcampo=array();
	        while($rubro = $res->fetch_object()){
	        	array_push($valcampo, array('id'=>$rubro->id,'descripcion'=>$rubro->descripcion));
				$valor=$valcampo;
			}

          return @$valor;
        }else{
          return 0;
        }
	}


	private function extraerEstuadiantesBD($estudiante=null,$campo=null)
	{
		@$query= "CALL SP_SL_ESTUDIANTE_CED('".$estudiante."',1);";
        @$res=executeQuery($query,'');
        if (@$res)
        {
	        while($estudiante = $res->fetch_object()){
	        	if ($campo)
	        	{
					$valcampo=$estudiante->apellidos.' '.$estudiante->$campo;
				}else{
					$valcampo=$estudiante->apellidos.' '.$estudiante->nombres;
				}
				$valor=$valcampo;
			}

          return @$valor;
        }else{
          return 0;
        }
	}
}

?>