<?php
@session_start('authcontrol');
class funciones
{


    public function __CONSTRUCT()
    {
      //$this->$pais_combo = new PaisCombo; 
    }
    public function formatoFecha($fecha)
    {
      /*$fecha="02/12/2015";*/
      $dia=substr($fecha, 0,2);
      $mes=substr($fecha, 3,2);
      $anio=substr($fecha, 6,4);
      $fechanueva=$anio.'-'.$mes.'-'.$dia;
      return $fechanueva;

    }
 
    public function formatoFechaDMY($fecha,$sep)
    {
      /*$fecha="02/12/2015";*/
      $dia=substr($fecha, 8,2);
      $mes=substr($fecha, 5,2);
      $anio=substr($fecha, 0,4);
      $fechanueva=$dia.$sep.$mes.$sep.$anio;
      return $fechanueva;

    }

    public function formatoFechaYMD($fecha)
    {
      /*$fecha="02/12/2015";*/
      $dia=substr($fecha, 0,2);
      $mes=substr($fecha, 3,2);
      $anio=substr($fecha, 6,4);
      $fechanueva=$anio.'-'.$mes.'-'.$dia;
      return $fechanueva;

    }

    public function formatoFechaPer($fecha,$sep)
    {
      /*$fecha="02/12/2015";*/
      $dia=substr($fecha, 8,2);
      $mes=substr($fecha, 5,2);
      $anio=substr($fecha, 0,4);
      $fechanueva=$dia.$sep.$mes.$sep.$anio;
      return $fechanueva;

    }

    public function formatoHora($hora)
    {
      $horatmp=substr($hora, 0,2);
      $minutostmp=substr($hora, 3,2);/*
      $dia=substr($fecha, 0,2);
      */
      if ($hora<10){ $hora= '0'.$hora.':'.$minutostmp; }
      $horanueva=$hora.':00';
      return $horanueva;

    }

    public function formatoMesNombre($fecha)
    {
      $mesfecha= substr($fecha, 3,2);
      switch($mesfecha){
          case "1": $mes_texto = "Enero"; break;
          case "2": $mes_texto = "Febrero"; break;
          case "3": $mes_texto = "Marzo"; break;
          case "4": $mes_texto = "Abril"; break;
          case "5": $mes_texto = "Mayo"; break;
          case "6": $mes_texto = "Junio"; break;
          case "7": $mes_texto = "Julio"; break;
          case "8": $mes_texto = "Agosto"; break;
          case "9": $mes_texto = "Septiembre"; break;
          case "10": $mes_texto = "Octubre"; break;
          case "11": $mes_texto = "Noviembre"; break;
          case "12": $mes_texto = "Diciembre"; break;
          default: $mes_texto = "";    
      }
      return $mes_texto;

    }

    public function formatoMesNombreComp($fecha,$sep)
    {
      $dia=substr($fecha, 8,2);
      $mesfecha=substr($fecha, 5,2);
      $anio=substr($fecha, 0,4);
      switch($mesfecha){
          case "1": $mes_texto = "Enero"; break;
          case "2": $mes_texto = "Febrero"; break;
          case "3": $mes_texto = "Marzo"; break;
          case "4": $mes_texto = "Abril"; break;
          case "5": $mes_texto = "Mayo"; break;
          case "6": $mes_texto = "Junio"; break;
          case "7": $mes_texto = "Julio"; break;
          case "8": $mes_texto = "Agosto"; break;
          case "9": $mes_texto = "Septiembre"; break;
          case "10": $mes_texto = "Octubre"; break;
          case "11": $mes_texto = "Noviembre"; break;
          case "12": $mes_texto = "Diciembre"; break;
          default: $mes_texto = "";    
      }
      $fechanueva=$dia.$sep.$mes_texto.$sep.$anio;
      return $fechanueva;

    }
  

}
                    //print_r($_SESSION['tipoparentezco']);
                  /*  $tipopar= new propiedades();
                    $nompar=$tipopar->formatoFecha('4');
                    echo $nompar;
*/

?>
