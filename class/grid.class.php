<?php
@session_start('imag');
class grid
{
	public function get_Grid()
	{

	}
	public function get_Panel_Html($id=Null,$titulo=Null,$style=Null,$options=Null,$handler=Null,$content=Null)
	{
		$panelHtml="<div id=\"".$id."\" class=\"easyui-panel\" title=\"".$titulo."\" style=\"".$style."\"
                data-options=\"
                    tools:[{
                        ".$options."
                        handler:function(){
                          ".$handler."  
                        }
                    }]
                \">".$content."
        </div>";

        return $panelHtml;
	}
    public function get_Grid_Html($id=Null,$title=Null,$style=Null,$sort=Null,$sortname=Null,$dataoptions=Null,$campos=Null)
    {
        $GridHtml=$this->table_Html($id,$title,$style,$sort,$sortname,$dataoptions,$campos);
        return $GridHtml;
    }
    private function table_Html($id=Null,$title=Null,$style=Null,$sort=Null,$sortname=Null,$dataoptions=Null,$campos=Null)
    {
        $cont=0;
        $th=null;
        if ($campos)
        {
            foreach ($campos as $key => $value) {
                $campo=@$value['campo'];
                $titulo=@$value['titulo'];
                $sortable=@$value['sortable'];
                $width=@$value['width'];
                $align=@$value['align'];
                $cont++;
                $th.="<th field=\"".$campo."\" align=\"".$align."\" width=\"".$width."\"  sortable=\"".$sortable."\">".$titulo."</th>";
            }
        }


        $tableHtml="<table id=\"".$id."\" class=\"easyui-datagrid\" style=\"".$style."\" sortOrder=\"".$sort."\" sortName=\"".$sortname."\"
            title=\"".$title."\" data-options=\"".$dataoptions."\"  method=\"post\" >
        <thead>
            <tr>
                ".$th."
            </tr>
        </thead>
        </table>";
        if ($cont>0)
        {
            return $tableHtml;
        }else{
            return 'No hay datos';
        }
        
    }
}
/*$grid= new grid();
echo $grid->get_Panel_Html('','','','','');*/
?>