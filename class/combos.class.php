<?php
@session_start('authcontrol');
class combos
{
    private $pais_combo;

    public function __CONSTRUCT()
    {
      //$this->$pais_combo = new PaisCombo; 
    }
    public function ListarPais()
    {
      if (!isset($_SESSION['paises'])){ $this->extraer_paises_db();  }
      $listapais=$_SESSION['paises'];
      $html = "";
            for ($i = 0; $i < count($listapais); $i++)
            {
              for($j=0;$j<count($listapais[$i]);$j++) 
              {  
                if ($j%2!==0)
                {
                  if ($listapais[$i][$j+1]==0)
                  {
                    $html .= '<option value="' . $listapais[$i][$j-1] . '">' . $listapais[$i][$j] . '</option>' . "\n";
                  //echo $listapais[$i][$j];
                  }
                }
              }
            }  
      /*$html .= '</select>' . "\n";*/
      return $html;
      /*return $_SESSION['paises'];*/
    }

    public function Listar_provincia($pais)
    {
      if (!isset($_SESSION['paises'])){ $this->extraer_paises_db();  }
      $listapais=$_SESSION['paises'];
      $html = "";
      $a=0;
            for ($i = 0; $i < count($listapais); $i++)
            {
              for($j=0;$j<count($listapais[$i]);$j++) 
              {  
                if ($j%2!==0)
                {
                  if ($listapais[$i][$j+1]==$pais)
                  {
                      $a++;
                    //$html .= '<option value="' . $listapais[$i][$j-1] . '">' . $listapais[$i][$j] . '</option>' . "\n";
                      @$listaprovincia[$a][0]=$listapais[$i][$j-1];
                      @$listaprovincia[$a][1]=$listapais[$i][$j];
                      @$listaprovincia[$a][2]=$listapais[$i][$j+1];
                      
                  }
                }
              }
            }  
      
      return @$listaprovincia;
      
    }

    public function Listar_ciudad($provincia)
    {
      if (!isset($_SESSION['paises'])){ $this->extraer_paises_db();  }
      $listapais=$_SESSION['paises'];
      $html = "";
      $a=0;
            for ($i = 0; $i < count($listapais); $i++)
            {
              for($j=0;$j<count($listapais[$i]);$j++) 
              {  
                if ($j%2!==0)
                {
                  if ($listapais[$i][$j+1]==$provincia)
                  {
                      $a++;
                    //$html .= '<option value="' . $listapais[$i][$j-1] . '">' . $listapais[$i][$j] . '</option>' . "\n";
                      @$listaciudad[$a][0]=$listapais[$i][$j-1];
                      @$listaciudad[$a][1]=$listapais[$i][$j];
                      @$listaciudad[$a][2]=$listapais[$i][$j+1];
                      
                  }
                }
              }
            }  
      
      return @$listaciudad;
      
    }

    public function Listarpacientes()
    {
      if (!isset($_SESSION['tipopaciente'])){ $this->extraer_tipopacientes_db();  }
      $listatipopaciente=$_SESSION['tipopaciente'];
      $html = "";
            foreach ($listatipopaciente as $key => $value) {
              $html .= '<option value="' . $value[0] . '">' . $value[1] . '</option>' . "\n";
            }
      return $html;
      /*return $_SESSION['paises'];*/
    }

    public function Listarparentezco()
    {
      //if (!isset($_SESSION['tipopaciente'])){ $this->extraer_tipopacientes_db();  }
      $listaparentezco=array();
      array_push($listaparentezco,array('id'=>0,'parentezco'=>'Papá'));
      array_push($listaparentezco,array('id'=>1,'parentezco'=>'Mamá'));
      array_push($listaparentezco,array('id'=>2,'parentezco'=>'Esposa'));
      array_push($listaparentezco,array('id'=>3,'parentezco'=>'Esposo'));
      array_push($listaparentezco,array('id'=>4,'parentezco'=>'Hijo'));
      array_push($listaparentezco,array('id'=>5,'parentezco'=>'Hija'));
      array_push($listaparentezco,array('id'=>6,'parentezco'=>'Hermano'));
      array_push($listaparentezco,array('id'=>7,'parentezco'=>'Hermana'));
      array_push($listaparentezco,array('id'=>8,'parentezco'=>'Amigo'));
      array_push($listaparentezco,array('id'=>9,'parentezco'=>'Amiga'));
      $_SESSION['tipoparentezco']= $listaparentezco;
      $html = "";
            foreach ($listaparentezco as $key => $value) {
              $html .= '<option value="' . $value['id'] . '">' . $value['parentezco'] . '</option>' . "\n";
            }
      return $html;
      /*return $_SESSION['paises'];*/
    }

    private function extraer_paises_db()
    {
        @$query= "select id,nombre,id_padre from fn_paises(0);";
            $res=ejecute_query($query);
            $cont=0;
            $_SESSION['paises']=array();
            while($Datos = pg_fetch_object($res)){
              $cont++;
              $_SESSION['paises'][$cont][0]=$Datos->id;
              $_SESSION['paises'][$cont][1]=$Datos->nombre;
              $_SESSION['paises'][$cont][2]=$Datos->id_padre;
              /*$idpais=$Datos->id;
              $nombrep=$Datos->nombre;
              $id_padrep=$Datos->id_padre;*/

            }

    }

    public function Extraerparentezco( $id )
    {
      $listaparentezco = $_SESSION['tipoparentezco'];
      foreach ($listaparentezco as $key => $value) {

        if ($value['id']==$id)
        {
            $nomparentezco=$value['parentezco'];
            
        }       
      }
      return $nomparentezco;
    }

    private function extraer_tipopacientes_db()
    {
            @$query= "select tip_pac_codi,tip_pac_nombre,tip_pac_descripcion,tip_pac_estado from fn_tipopaciente(1);";
            $res=ejecute_query($query);
            $cont=0;
            $_SESSION['tipopaciente']=array();
            while($Datos = pg_fetch_object($res)){
              $cont++;
              $_SESSION['tipopaciente'][$cont][0]=$Datos->tip_pac_codi;
              $_SESSION['tipopaciente'][$cont][1]=$Datos->tip_pac_nombre;
              $_SESSION['tipopaciente'][$cont][2]=$Datos->tip_pac_descripcion;
              $_SESSION['tipopaciente'][$cont][3]=$Datos->tip_pac_estado;
              /*$idpais=$Datos->id;
              $nombrep=$Datos->nombre;
              $id_padrep=$Datos->id_padre;*/

            }

    }

    

}
                    //print_r($_SESSION['tipoparentezco']);
                   /* $tipopar= new combos();
                    $nompar=$tipopar->Extraerparentezco('4');
                    echo $nompar;*/


?>
