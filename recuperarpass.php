<script type="text/javascript">
    function generarpass(){ 
	       var usuariorec=$('#txtsuariorec').val();
	       var dataString = 'usuariorec='+ usuariorec;
	       $("#error2").show();
	       $("#error2").fadeIn(400).html('<img src="images/loading.gif" />');
	       $.ajax({
	             type: "POST",
	             url: "procesarrecuperar.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=trim(result);
	             $("#error2").hide();
	             if(result==1){
	                  $("#txtsuariorec").attr("disabled", "disabled");
	                  $("#btenviar").attr("disabled", "disabled");
	                  $("#btenviar").css({ background: "#666" });
	                  $("#errorMessage2").css({ color: "#1461C6" });
	                  $("#errorMessage2").html("Se ha enviado un correo al usuario "+usuariorec);
	             }else{
	             	  $("#errorMessage2").css({ color: "red" });
	                  $("#errorMessage2").html(result);
	             }
	        }
	  });
	}

		function trim(str){
		var str=str.replace(/^\s+|\s+$/,'');
		return str;
		}
</script> 

<div style="line-height:20px;">
	<span>Estimado usuario, ingrese su nombre de usuario.</span>
	<span>Le llegará un mensaje a su correo registrado con una contraseña temporal.</span>
	<br/><br/>
	<span style="font-weight:bold;">Usuario: &nbsp;</span>
	<input type="text" id="txtsuariorec" name="txtusuariorec"  size="16" />
	<br/>
	<div id="errorMessage2" class="errorMessage" style="height: auto;float:left;    width: 276px;">
		<div id="error2" class="error" style="display: none;">
		
		</div>
	</div>
	<div style="float:right;">
		<input type="button" id="btenviar" name="submit" style="border: 1px solid white; " value="Enviar" class="boton" onclick="generarpass()">
	</div>
</div>