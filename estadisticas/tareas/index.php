<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',
       'version':'1','packages':['timeline']}]}"></script>

<?php
$query="CALL SP_SL_TAREA(0,1,1,1);";
    $sqlquery= executeQuery($query,"");
    $cont=0;
      while($fila=mysqli_fetch_object($sqlquery)){
           $cont=$cont+1;
           $idproyecto=$fila->idproyecto; 
           $nombre[]=$fila->nombre;
           $idusuarioc=$fila->idusuarioc;
           $fechacreacion=$fila->fechacreacion;
           $descripcion=$fila->descripcion;
           $fechainicial[]=$fila->fechainicio;
           $fechafinal[]=$fila->fechafinal;
           $horainicial=$fila->horainicial;
           $horafinal=$fila->horafinal;
           $tiempo=$fila->tiempo;
           $tiempocat=$fila->tiempocat;
          
           
           //$fechanac=str_replace("-", "/", $fechanac);
          
        }
?>


<script type="text/javascript">
      google.load("visualization", "1.1", {packages:["timeline"]});
      google.setOnLoadCallback(drawChart);

      function drawChart() {

  var container = document.getElementById('timeline-tooltip');
  var chart = new google.visualization.Timeline(container);
  var dataTable = new google.visualization.DataTable();
  dataTable.addColumn({ type: 'string', id: 'Room' });
  dataTable.addColumn({ type: 'string', id: 'Name' });
  dataTable.addColumn({ type: 'date', id: 'Start' });
  dataTable.addColumn({ type: 'date', id: 'End' });


  //alert(<?=$nombre[0]?>);
  var nombre = <?=json_encode($nombre)?>;
  var fechainicial = <?=json_encode($fechainicial)?>;
  var fechafinal = <?=json_encode($fechafinal)?>;

  // alert(dato);
  nombre=  nombre.toString().split(","); 
  fechainicial=  fechainicial.toString().split(","); 
  fechafinal=  fechafinal.toString().split(","); 


   for(var i=0;i<nombre.length;i++)
    {
    
    fechainicialus=fechainicial[i].toString().split("-"); 
    fechafinalus=fechafinal[i].toString().split("-"); 
    //alert(fechainicial[i]+','+fechafinal[i]);

    dataTable.addRows([
    [ nombre[i], '',       new Date(fechainicialus[0],fechainicialus[1]-1,fechainicialus[2],00,00,00),  new Date(fechafinalus[0],fechafinalus[1]-1,fechafinalus[2],23,59,59) ]//,
    //[ 'Magnolia Room', 'Intermediate JavaScript',     new Date(2015,10,13,12,0,0),  new Date(2015,10,13,13,30,0) ],
    //[ 'Magnolia Room', 'Advanced JavaScript',         new Date(2015,10,14,12,0,0),  new Date(2015,10,14,13,30,0) ],
    //[ 'Willow Room',   'Beginning Google Charts',    new Date(2015,10,12,12,0,0),  new Date(2015,10,12,13,30,0) ],
    //[ 'Willow Room',   'Intermediate Google Charts',  new Date(2015,10,13,12,0,0),  new Date(2015,10,13,13,30,0) ],
    /*[ 'Willow Room',   'Advanced Google Charts',      new Date(2015,10,14,12,0,0),  new Date(2015,10,14,13,30,0) ]*/]);

    }

    var options = {
    timeline: { colorByRowLabel: true }
  };

    chart.draw(dataTable);
      }
    </script>
  </head>
  <body>
    <div id="timeline-tooltip" style="height: 400px;"></div>
  </body>