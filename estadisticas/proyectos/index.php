<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',
       'version':'1','packages':['timeline']}]}"></script>

<?php
$id=$_GET['id'];
$avancep=$_GET['av'];

$query="CALL SP_SL_PROYECTOS(".$id.",1);";
    $sqlquery= executeQuery($query,"");
    $cont=0;
      while($fila=mysqli_fetch_object($sqlquery)){
           $nombreproy=$fila->nombre;
           $idusuariocproy=$fila->idusuarioc;
           $fechacreacionproy=$fila->fechacreacion;
           $descripcionproy=$fila->descripcion;
           $fechainicialproy=$fila->fechainicio;
           $fechafinalproy=$fila->fechafinal;
           $horainicialproy=$fila->horainicial;
           $horafinalproy=$fila->horafinal;
           $tiempoproy=$fila->tiempo;
           $tiempocatproy=$fila->tiempocat;
           $estatusproy=$fila->nomestatus;
      }
?>
<div style="line-height:22px;">
<span style="font-weight:bold;color:#1461C6;">Proyecto: </span>
<span><?=$nombreproy?></span>
<br>
<span style="font-weight:bold;color:#1461C6;">Descripcion: </span>
<span><?=$descripcionproy?></span>
<br>
<span style="font-weight:bold;color:#1461C6;">Avance: </span>
          <?php
            //  $avancep=0;
            if ($avancep == null || $avancep==""){ $avancep=0; }
            $posicion="-118";
            if ($avancep==0) { $posicion="-118"; }
            if ($avancep==100) { $posicion="0"; }
            if ($avancep>1 and $avancep<100) { $posicion=$posicion+($avancep*1.18); }  
  
            if ($avancep>=0 and $avancep<=25){ $barra='progressbg_red.gif'; }
            if ($avancep>=26 and $avancep<=50){ $barra='progressbg_orange.gif'; }
            if ($avancep>=51 and $avancep<=80){ $barra='progressbg_yellow.gif'; }
            if ($avancep>=81 and $avancep<=100){ $barra='progressbg_green.gif'; }         
            
          ?>  
            <img src="images/progressbar.gif" width="120" height="12" style="background-image: url(images/<?=$barra?>);   background-position: <?=$posicion?>px 50%;" title="" />
            <span style="font-weight:;">
              <?=$avancep?>% 
            </span>

<br>
<span style="font-weight:bold;color:#666;">Gráfico de Gantt del Proyecto: </span>
<br>
</div>
<?php
$query="CALL SP_SL_TAREA(0,".$id.",0,1);";
    $sqlquery= executeQuery($query,"");
    $cont=0;
      while($fila=mysqli_fetch_object($sqlquery)){
           $cont=$cont+1;
           $idproyecto=$fila->idproyecto; 
           $nombre[]=$fila->nombre;
           $idusuarioc=$fila->idusuarioc;
           $fechacreacion=$fila->fechacreacion;
           $descripcion=$fila->descripcion;
           $fechainicial[]=$fila->fechainicio;
           $fechafinal[]=$fila->fechafinal;
           $horainicial=$fila->horainicial;
           $horafinal=$fila->horafinal;
           $tiempo=$fila->tiempo;
           $tiempocat=$fila->tiempocat;
           $valavance=$fila->avance.'%';
           if ($valavance=="%"){ $valavance="0%";}
           $avance[]=$valavance;
           
           //$fechanac=str_replace("-", "/", $fechanac);
          
        }
?>


<script type="text/javascript">
      google.load("visualization", "1.1", {packages:["timeline"]});
      google.setOnLoadCallback(drawChart);

      function drawChart() {

  var container = document.getElementById('timeline-tooltip');
  var chart = new google.visualization.Timeline(container);
  var dataTable = new google.visualization.DataTable();
  dataTable.addColumn({ type: 'string', id: 'Room' });
  dataTable.addColumn({ type: 'string', id: 'Name' });
  dataTable.addColumn({ type: 'date', id: 'Start' });
  dataTable.addColumn({ type: 'date', id: 'End' });


  //alert(<?=$nombre[0]?>);
  var nombre = <?=json_encode($nombre)?>;
  var avance = <?=json_encode($avance)?>;
  var fechainicial = <?=json_encode($fechainicial)?>;
  var fechafinal = <?=json_encode($fechafinal)?>;

  // alert(dato);
  nombre=  nombre.toString().split(","); 
  avance=  avance.toString().split(","); 
  fechainicial=  fechainicial.toString().split(","); 
  fechafinal=  fechafinal.toString().split(","); 


   for(var i=0;i<nombre.length;i++)
    {
    
    fechainicialus=fechainicial[i].toString().split("-"); 
    fechafinalus=fechafinal[i].toString().split("-"); 
    //alert(fechainicial[i]+','+fechafinal[i]);

    dataTable.addRows([
    [ nombre[i], avance[i],       new Date(fechainicialus[0],fechainicialus[1]-1,fechainicialus[2],00,00,00),  new Date(fechafinalus[0],fechafinalus[1]-1,fechafinalus[2],23,59,59) ]//,
    //[ 'Magnolia Room', 'Intermediate JavaScript',     new Date(2015,10,13,12,0,0),  new Date(2015,10,13,13,30,0) ],
    //[ 'Magnolia Room', 'Advanced JavaScript',         new Date(2015,10,14,12,0,0),  new Date(2015,10,14,13,30,0) ],
    //[ 'Willow Room',   'Beginning Google Charts',    new Date(2015,10,12,12,0,0),  new Date(2015,10,12,13,30,0) ],
    //[ 'Willow Room',   'Intermediate Google Charts',  new Date(2015,10,13,12,0,0),  new Date(2015,10,13,13,30,0) ],
    /*[ 'Willow Room',   'Advanced Google Charts',      new Date(2015,10,14,12,0,0),  new Date(2015,10,14,13,30,0) ]*/]);

    }

    var options = {
    timeline: { singleColor: '#1461C6' }
  };

    chart.draw(dataTable, options);
      }
    </script>
  </head>
  <body>
    <div id="timeline-tooltip" style="height: 400px;"></div>

  </body>