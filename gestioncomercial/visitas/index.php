<div style="border: 1px solid #ccc; padding-top: 5px;padding-right: 8px;padding-left: 8px;padding-bottom: 5px;">
	<?php include ('botones.php'); ?>
</div>
<br>
<div>
	<span class="titulo">Mis Visitas</span>

	<div id="errorMessage2" class="errorMessage"   style=" line-height: 15px;"></div>
	<div id="error2" class="error" style="display:none;    line-height: 15px;"></div>
	<div id="registrado2" style="display:none ;    line-height: 15px; margin-bottom: 10px; padding: 5px 5px 3px; width: 240px;    margin-top: 6px;   border: 1px solid#ccc; background: -webkit-gradient(linear,left top,left bottom,from(#FAFAFA),to(#eaeaea));" >
	</div>
	<span style="color:#666;" >Este módulo esta diseñado solo para el Depatamento Comercial de la compañía.</span><br/>
	<table style="margin-top:10px;">
				<tr>
					<td>
						<span style="font-weight:bold;">Cliente</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Título V.</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Fecha Visita</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Hora Visitas</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Estatus</span>
					</td>
					<td>
						<span style="font-weight: bold; color:#666;">Acciones </span>
					</td>
				</tr>	
		<?php
		$query="CALL SP_SL_VISITAS(0,".$_SESSION['codusuario'].",0);";
		$sqlquery= executeQuery($query,"");
		$cont=0;
			while($fila=mysqli_fetch_object($sqlquery)){
		           $cont=$cont+1;
		           $idvisita=$fila->idvisita; 
				   $idusuario=$fila->idusuario;
				   $nombrer=$fila->nombre;
				   $motivor=$fila->motivo;
				   $idcliente=$fila->idcliente;
				   $idcliente=$fila->idcliente;
				   $fechacreacion=$fila->fechacreacion;
				   $ultimaact=$fila->ultimaact;
				   $ultimaact=$fila->ultimaact;
				   $horainicio=$fila->horainicio;
				   $horafin=$fila->horafin;
				   $idestatus=$fila->idestatus;
				   $nomestatus=$fila->nomestatus;
				   $nomcliente=$fila->nomcliente;
				   $fechainicio=$fila->fechainicio;
				   //$fechanac=str_replace("-", "/", $fechanac);
				 //  $cliente="Mario Aguilar";
				  // $estatus="POR VISITAR";
				  // if ($idestatus==4 || $idestatus==5) { $estatus="CANCELADA"; }
			?>
			<tr>
				<td><?=$nomcliente?></td>
				<td><?=$nombrer?></td>
				<td><?=$fechainicio?></td>
				<td><?=substr($horainicio, 0,5).' '?>-<?=' '.substr($horafin,0,5) ?></td>
				<td>
					<span style="color:<?php if ($idestatus==4) { echo 'red'; }else{ if ($idestatus==5 ) { echo 'gray'; }else { echo 'green'; }  } ?>;font-weight:bold;font-weight:bold;">
						<?=$nomestatus?>
					</span>
				</td>
				<td>
			
					<a href="#amodals<?=$idvisita?>">
						<img src="images/editar.png" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Editar Visitas"/>
					</a>
					
					<a href="#emodal<?=$idvisita?>">
						<img src="images/ver.png" style="float:;margin-right:5px; " width="16" height="16" title="Ver Visitas" onclick="" />
					</a>
					
					<img src="images/elim.png" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Eliminar Visitas" onclick="javascript:eliminar_visitas(<?=$idvisita?>);" />
					<img src="images/autorizado.png" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Confirmar Asistencia" onclick="javascript:confirmar_visita(<?=$idvisita?>);" />
					<img src="images/negar.jpg" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Cancelar Asistencia" onclick="javascript:cancelar_visita(<?=$idvisita?>);" />
					<a href="#cmodal<?=$idvisita?>">
						<img src="images/observacion.png" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Agregar Observación" onclick="" />
					</a>
				</td>
				<div id="emodal<?=$idvisita?>" class="modalmask2">
					<div class="modalbox2 movedown">
					<div style="position: absolute;width: 100%;padding: 0px; */" class="barracentro verdeamarela">
							<div style="float:left; padding:5px; "> Ver Visitas</div>
							<a href="#close" style="  margin-right: 2px;margin-top:2px;" onclick="javascript:;" title="Close" class="close">X</a>
						</div>
						<div style="margin-top:23px;">
						<?php
							require('gestioncomercial/visitas/consultar/index.php');
						?>
						</div>
					</div>
				</div>
				<div id="cmodal<?=$idvisita?>" class="modalmask2">
					<div class="modalbox2 movedown">
					<div style="position: absolute;width: 100%;padding: 0px; */" class="barracentro verdeamarela">
							<div style="float:left; padding:5px; "> Agregar comentario</div>
							<a href="#close" style="  margin-right: 2px;margin-top:2px;"  onclick="javascript:actualizar_vvisitas();" title="Close" class="close">X</a>
						</div>
						<div style="margin-top:23px;">
						<?php
							require('gestioncomercial/visitas/comentarios/index.php');
						?>
						</div>
					</div>
				</div>
				<?php
					if ($idestatus!=9)
					{
				?>
				<div id="amodals<?=$idvisita?>" class="modalmask2">
					<div class="modalbox2 movedown">
					<div style="position: absolute;width: 100%;padding: 0px; */" class="barracentro verdeamarela">
							<div style="float:left; padding:5px; ">Editar Visitas</div>
							<a href="#close" style="  margin-right: 2px;margin-top:2px;" onclick="javascript:actualizar_vvisitas();" title="Close" class="close">X</a>
						</div>
						<div style="margin-top:23px;">
						<?php
							require('gestioncomercial/visitas/actualizar/index.php');
						?>
						</div>
					</div>
				</div>
				<?php
					}
					?>
			</tr>
			<?php
				}
			?>
			</table><br/>

<?php
			if ($cont==0){
			?>
	<div style="line-height:20px;">
				
		<span>No tienes permisos ingresados.</span>
		<br>
	</div>
	<?php
				}
	?>		
<br/>


	<div id="modal1" class="modalmask2">
		<div class="modalbox2 movedown">
		<div style="position: absolute;width: 100%;padding: 0px; */" class="barracentro verdeamarela">
				<div style="float:left; padding:5px; "> Agregar Visitas</div>
				<a href="#close" style="  margin-right: 2px;margin-top:2px;" onclick="javascript:actualizar_vvisitas();" title="Close" class="close">X</a>
			</div>
			<div style="margin-top:23px;">
			<?php
				include('gestioncomercial/visitas/crear/index.php');
			?>
			</div>
		</div>
	</div>

</div>