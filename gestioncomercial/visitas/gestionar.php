
<div>

<?php
	if ($_SESSION['idrol']==2 || $_SESSION['idrol']==1)
	{
?>

	<span class="titulo">Gestión de Permisos</span>
	
	<table style="margin-top:10px;">
				<tr>
					<td>
						<span style="font-weight:bold;">Permiso</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Fecha Permiso</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Usuario</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Estatus</span>
					</td>
					<td>
						<span style="font-weight: bold; color:#666;">Acciones </span>
					</td>
				</tr>	
		<?php
		$query="CALL SP_SL_PERMISOS(0,".$_SESSION['codusuario'].",0,0,0);";
		$sqlquery= executeQuery($query,"");
		$cont=0;
			while($fila=mysqli_fetch_object($sqlquery)){
		           $cont=$cont+1;
		           $idpermiso=$fila->idpermiso; 
				   $idusuario=$fila->idusuario;
				   $nomauth=$fila->nomauth;
				   $idusuarioaut=$fila->idusuarioaut;
				   $idusuariorrhhaut=$fila->idusuariorrhhaut;
				   $nombrep=$fila->nombrep;
				   $descripcionp=$fila->descripcionp;
				   $fechacreacion=$fila->fechacreacion;
				   $fechainicio=$fila->fechainicio;
				   $fechafin=$fila->fechafin;
				   $horainicio=$fila->horainicio;
				   $horafin=$fila->horafin;
				   $idestatus=$fila->idestatus;
				   $estatus=$fila->nomestatus;
				   $todoeldia=$fila->todoeldia;
				   //$fechanac=str_replace("-", "/", $fechanac);

			?>
			<tr>
				<td><?=$nombrep?></td>
				<td><?=$fechainicio?></td>
				<td><?=$nomauth?> </td>
				<td>
					<span style="color:<?php if ($idestatus==4 || $idestatus==5) { echo 'red'; }else{ if ($idestatus==2 || $idestatus==3 ) { echo 'gray'; }else { echo 'green'; }  } ?>;font-weight:bold;font-weight:bold;">
						<?=$estatus?>
					</span>
				</td>
				<td>				
					<a href="#modals<?=$idpermiso?>">
						<img src="images/ver.png" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Ver Permiso" onclick="javascript:ver_permiso(<?=$idpermiso?>);" />
					</a>
					<?php
					if ($idestatus==1)
					{
					?>
					<img src="images/autorizado.png" style="float:;margin-right:5px; " width="16" height="16" title="Autorizar Permiso" onclick="javascript:autorizar_permiso(<?=$idpermiso?>);" />
					<img src="images/negar.jpg" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Negar Permiso" onclick="javascript:denegar_permiso(<?=$idpermiso?>);" />
					<?php
					}
					?>
					<?php
					if ($idestatus==4)
					{
					?>
								<img src="images/autorizado.png" style="float:;margin-right:5px; " width="16" height="16" title="Autorizar Permiso" onclick="javascript:autorizar_permiso(<?=$idpermiso?>);" />

					<?php
					}
					

					if ($idestatus==6)
					{
					?>
								<img src="images/negar.jpg" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Negar Permiso" onclick="javascript:denegar_permiso(<?=$idpermiso?>);" />
					<?php
					}
					?>
				</td>

			</tr>
			<?php
				}
			?>
			</table>


			<?php
			if ($cont==0){
			?>
	<div style="line-height:20px;">
				
		<span>No tienes permisos por autorizar</span>
		<br>
	</div>
	<?php
				}
	?>		

<?php
	}
?>

<?php
//echo $_SESSION['idrol'];
	if ($_SESSION['idrol']==6 || $_SESSION['idrol']==1)
	{
?>
<br>
	<span class="titulo">Gestión de Permisos</span>
	
	<table style="margin-top:10px;">
				<tr>
					<td>
						<span style="font-weight:bold;">Permiso</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Fecha Permiso</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Solicitado por</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Aprobado por</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Estatus</span>
					</td>
					<td>
						<span style="font-weight: bold; color:#666;">Acciones </span>
					</td>
				</tr>	
		<?php

	 $query="CALL SP_SL_PERMISOS(0,0,".$_SESSION['codusuario'].",0,0);";
		$sqlquery= executeQuery($query,"");
		$cont=0;
			while($fila=mysqli_fetch_object($sqlquery)){
		           $cont=$cont+1;
		           $idpermiso=$fila->idpermiso; 
				   $idusuario=$fila->idusuario;
				   $nomsol=$fila->nomsol;
				   $nomauth=$fila->nomauth;
				   $idusuarioaut=$fila->idusuarioaut;
				   $idusuariorrhhaut=$fila->idusuariorrhhaut;
				   $nombrep=$fila->nombrep;
				   $descripcionp=$fila->descripcionp;
				   $fechacreacion=$fila->fechacreacion;
				   $fechainicio=$fila->fechainicio;
				   $fechafin=$fila->fechafin;
				   $horainicio=$fila->horainicio;
				   $horafin=$fila->horafin;
				   $idestatus=$fila->idestatus;
				   $estatus=$fila->nomestatus;
				   $todoeldia=$fila->todoeldia;
				   //$fechanac=str_replace("-", "/", $fechanac);
			 if ($idestatus==6 || $idestatus==7 || $idestatus==5  || $idestatus==1) {
			 	 if ($idestatus==6) {$estatus="NUEVO"; }
			?>
			<tr>
				<td><?=$nombrep?></td>
				<td><?=$fechainicio?></td>
				<td><?=$nomsol?> </td>
				<td><?=$nomauth?> </td>
				<td>
					<span style="color:<?php if ($idestatus==4 || $idestatus==5) { echo 'red'; }else{ if ($idestatus==2 || $idestatus==3 ) { echo 'gray'; }else { echo 'green'; }  } ?>;font-weight:bold;font-weight:bold;">
						<?=$estatus?>
					</span>
				</td>
				<td>				
					<a href="#modals<?=$idpermiso?>">
						<img src="images/ver.png" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Ver Permiso" onclick="javascript:ver_permiso(<?=$idpermiso?>);" />
					</a>
					<?php
					if ($idestatus==6 || $idestatus==1)
					{
					?>
					<img src="images/autorizado.png" style="float:;margin-right:5px; " width="16" height="16" title="Autorizar Permiso" onclick="javascript:autorizar_permisorrhh(<?=$idpermiso?>);" />
					<img src="images/negar.jpg" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Negar Permiso" onclick="javascript:denegar_permisorrhh(<?=$idpermiso?>);" />
					<?php
					}
					?>
					<?php
					if ($idestatus==5)
					{
					?>
								<img src="images/autorizado.png" style="float:;margin-right:5px; " width="16" height="16" title="Autorizar Permiso" onclick="javascript:autorizar_permisorrhh(<?=$idpermiso?>);" />

					<?php
					}
					

					if ($idestatus==7)
					{
					?>
								<img src="images/negar.jpg" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Negar Permiso" onclick="javascript:denegar_permisorrhh(<?=$idpermiso?>);" />
					<?php
					}
					?>
				</td>

			</tr>
			<?php
				}
				}
			?>
			</table>


			<?php
			if ($cont==0){
			?>
	<div style="line-height:20px;">
				
		<span>No tienes permisos por autorizar</span>
		<br>
	</div>
	<?php
				}
	?>		

<?php
	}
?>


	<div id="modal1" class="modalmask2">
		<div class="modalbox2 movedown">
		<div style="position: absolute;width: 100%;padding: 0px; */" class="barracentro verdeamarela">
				<div style="float:left; padding:5px; "> Agregar Permisos</div>
				<a href="#close" style="  margin-right: 2px;margin-top:2px;" onclick="javascript:actualizar_vpermiso();" title="Close" class="close">X</a>
			</div>
			<div style="margin-top:23px;">
			<?php
				include('rrhh/permisos/crear/index.php');
			?>
			</div>
		</div>
	</div>

</div>