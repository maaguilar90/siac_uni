<?php
session_start();
include_once('../../conexion.php');


error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Guayaquil');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Sistema de Notas")
							 ->setLastModifiedBy("Sistema de Notas")
							 ->setTitle("Estudiantes")
							 ->setSubject("PHPExcel")
							 ->setDescription("PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Estudiantes");


// Add some data
echo date('H:i:s') , " Add some data" , EOL;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'ID')
            ->setCellValue('B1', 'Estudiante')
            ->setCellValue('C1', 'Cédula')
            ->setCellValue('D1', 'Curso')
            ->setCellValue('E1', 'Fecha Nacimiento')
            ->setCellValue('F1', 'Dirección')
            ->setCellValue('G1', 'Teléfono')
            ->setCellValue('H1', 'Celular')
            ->setCellValue('I1', 'Mail')
            ->setCellValue('J1', 'Nombre Padre')
            ->setCellValue('K1', 'Tel. Padre')            
            ->setCellValue('L1', 'Cédula Padre')
            ->setCellValue('M1', 'Dirección Padre')
            ->setCellValue('N1', 'Teléfono2 Padre')
            ->setCellValue('O1', 'Ocupación')
            ->setCellValue('P1', 'Nombre Madre')
            ->setCellValue('Q1', 'Dirección Madre')
            ->setCellValue('R1', 'Cédula Madre')
            ->setCellValue('S1', 'Teléfono Madre')
            ->setCellValue('T1', 'Ocupación Madre');

//$query= "CALL SP_SL_ESTUDIANTES(0,'1');";
$idcursoest=@$_POST['idcursoniv'];
$query= "CALL SP_SL_CURSONIVEST(".$idcursoest.");";



$res=executeQuery($query,'');
$cont=1;

while($fila=mysqli_fetch_object($res))
{
      $cont=$cont+1;
       $idalumno=$fila->id_alumno; 
	   $descripcion=$fila->apellidos." ".$fila->nombres;
	   $nomcurso=$fila->nomcurso;
         $direccion=$fila->direccion;
         $telefono=$fila->telefono;
         $celular=$fila->celular;
         $email=$fila->email;
         $nompadre=$fila->nompadre;
         $telrepresentante=$fila->tel_representante;
         $cedula=$fila->cedula;
         $fecnacest=$fila->fec_nac_est;
         $direcpadre=$fila->direc_padre;
         $cedpadre=$fila->ced_padre;
         $fono2padre=$fila->fono2_padre;
         $ocupacionpadre=$fila->ocupacion_padre;
         $trabapadre=$fila->traba_padre;
         $nommadre=$fila->nom_madre;
         $direcmadre=$fila->direc_madre;
         $cedmad=$fila->ced_mad;
         $fonmad=$fila->fon_mad;
         $ocupmadre=$fila->ocup_madre;


	   $estatus=$fila->estatus;
	  // $rol=$fila->id_rol;
	   $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$cont, $idalumno)
            ->setCellValue('B'.$cont, $descripcion)
            ->setCellValue('C'.$cont, $cedula)
            ->setCellValue('D'.$cont, $nomcurso)
            ->setCellValue('E'.$cont, $fecnacest)
            ->setCellValue('F'.$cont, $direccion)
            ->setCellValue('G'.$cont, $telefono)
            ->setCellValue('H'.$cont, $celular)
            ->setCellValue('I'.$cont, $email)
            ->setCellValue('J'.$cont, $nompadre)
            ->setCellValue('K'.$cont, $telrepresentante)            
            ->setCellValue('L'.$cont, $cedpadre)
            ->setCellValue('M'.$cont, $direcpadre)
            ->setCellValue('N'.$cont, $fono2padre)
            ->setCellValue('O'.$cont, $ocupacionpadre)
            ->setCellValue('P'.$cont, $nommadre)
            ->setCellValue('Q'.$cont, $direcmadre)
            ->setCellValue('R'.$cont, $cedmad)
            ->setCellValue('S'.$cont, $fonmad)
            ->setCellValue('T'.$cont, $ocupmadre);

       $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
       $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
       $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
}



//$objPHPExcel->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
//$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
//$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);


// Rename worksheet
echo date('H:i:s') , " Rename worksheet" , EOL;
$objPHPExcel->getActiveSheet()->setTitle('Estudiantes');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Save Excel 2007 file
//echo date('H:i:s') , " Write to Excel2007 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

//$objWriter->save("materia".date("Ymd").".xlsx");
$objWriter->save("../../archivosexp/estudiantes".$_SESSION['codusuario'].".xlsx");

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Save Excel 95 file
echo date('H:i:s') , " Write to Excel5 format" , EOL;
$callStartTime = microtime(true);

/*$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(str_replace('.php', '.xls', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
*/
echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Echo memory peak usage
echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

// Echo done
echo date('H:i:s') , " Done writing files" , EOL;
echo 'Files have been created in ' , getcwd() , EOL;

//echo @res




?>

