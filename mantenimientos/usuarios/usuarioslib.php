<?php include ('../../conexion.php'); ?>
<?php
	session_start();
	@$idestatus=$_GET['ide'];

	$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id_usuario';
	$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';

	if (!$sort)
	{
		$sort='id_usuario';
	}

	if (!$order)
	{
		$order='ASC';
	}

	$query="CALL SL_USUARIO('".$idestatus."','".$sort."','".$order."');";


	$i=0;
	$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;

	$items = array();
	date_default_timezone_set('UTC');

	$sqlquery= executeQuery($query,"");
	$cont=0;
	while($fila=mysqli_fetch_object($sqlquery)){
		$cont=$cont++;
		$i++;
		$idusuario=$fila->id_usuario; 
		$usuario=$fila->usuario; 
		$nombres=$fila->nombres;
		$apellidos=$fila->apellidos;
		@$fechacreacion=$fila->fecha_creacion;
		$correo=$fila->correo;
		$idrol=$fila->id_rol;
		$descripcion=$fila->descripcion;
		@$nombrec=$fila->nombrec;
		$avatar=$fila->avatar;

		if ($avatar)
		{
			$avatar='<img style="height:16px; width=16px; " src="'.$avatar.'">';
		}else{
			$avatar='<img style="height:16px; width=16px; " src="images/user.png">';
		}

		$diafechaf= substr($fechacreacion, 8,2);
		$mesfechaf= substr($fechacreacion, 5,2);
		$aniofechaf= substr($fechacreacion, 0,4);

		switch($mesfechaf){
		    case "01": $dia_texto = "Ene"; break;
		    case "02": $dia_texto = "Feb"; break;
		    case "03": $dia_texto = "Mar"; break;
		    case "04": $dia_texto = "Abr"; break;
		    case "05": $dia_texto = "May"; break;
		    case "06": $dia_texto = "Jun"; break;
		    case "07": $dia_texto = "Jul"; break;
		    case "08": $dia_texto = "Ago"; break;
		    case "09": $dia_texto = "Sep"; break;
		    case "10": $dia_texto = "Oct"; break;
		    case "11": $dia_texto = "Nov"; break;
		    case "12": $dia_texto = "Dic"; break;
		    default: $dia_texto = "-";    
		}	
		
		$nombremesfechaf= $dia_texto;
		$fechafinalm=$diafechaf." ".$nombremesfechaf." ".$aniofechaf;
	
		$botones="";
		//$estatus='<span style="color:'.$colorestatus.';font-weight:bold;">'.$estatus.'</span>';

		if (@$_SESSION['gusuarios']==1)
		{
			//$botones=$botones.'<form id="ff'.$idusuario.'" name="ff'.$idusuario.'" style="    margin-bottom: 0px;" action="mantenimiento/usuarios/.php" method="post" enctype="multipart/form-data">';	
			//$botones=$botones.'<input id="btnsubmit'.$idusuario.'" type="button" onclick="$(\'#txtidaccion'.$idusuario.'\').val(\''.$idusuario.'\');" style="    background-image: url(\'images/editar.png\');background-size: 15px;vertical-align: ;height: 21px;width: 19px;background-repeat: no-repeat;background-color: transparent;background-position-x: 1px;background-position-y: 2px;"/>';	
			//$botones=$botones.'<img src="images/ver.png" style="float:;margin-right:5px; " width="16" height="16" title="Ver tareas del Proyecto" onclick="javascript:ir_agregar_tarea('.$idusuario.');" />';
			//$botones=$botones.'<input type="hidden" id="txtidaccion'.$idusuario.'" name="txtidaccion" class="">';
			
			if ($idestatus!=3) 
			{	
				if ($idrol!=1) 
				{	
					//$botones=$botones.'<img src="images/ver.png" style="float:;margin-right:10px; " width="16" height="16" title="Ver Usuario" onclick="javascript:;" />';
					$botones=$botones.'<img src="images/elim.png" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Eliminar Usuario" onclick="javascript:eliminar_usuario('.$idusuario.');" />';
				}else
				{

				}
			}else
			{	
				$botones=$botones.'<img src="images/restaurar.png" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Restaurar Usuario" onclick="javascript:restaurar_usuario('.$idusuario.');"  />';
			}
			//$botones=$botones.' <script type="text/javascript"> ';
			//$botones=$botones.'$(function(){ $(\'#ff'.$idusuario.'\').form({ success:function(data){ var m = \'\'; $.messager.alert(\'Información del Proyecto\', m+data).window({ width:820,left:\'25%\' }); } }); }); $( "#btnsubmit'.$idusuario.'" ).click(function() { $( "#ff'.$idusuario.'" ).submit(); }); </script>';
			//$botones=$botones.'</form>';					
		}else{

			$botones=$botones.'<img src="images/ver.png" style="float:;margin-right:5px; " width="16" height="16" title="Ver Usuario" onclick="javascript:;" />';
		
		}

		//$tarproy=$nombres;
		//$nombres='<div id="titproy'.$idusuario.'" title="'.$nombres.'" class="easyui-tooltip">'.$nombres.'</div>';
		//$nombres=$nombres.' <script type="text/javascript"> $(function(){ $(\'#titproy'.$idusuario.'\').tooltip({ position: \'bottom\', content: \'<span style="padding:5px;color:#000">'.$tarproy.'</span>\',  }) }); </script>';

		$index = $i+($page-1)*$rows;
		$amount = rand(50,100);
		$price = rand(10000,20000)/100;
		$items[] = array(
			'id_usuario' => $idusuario,
			'avatar' => $avatar,
			'usuario' => $usuario,
			'apellidos' => $apellidos,
			'nombres' => $nombres,
			'correo' => $correo,
			'descripcion' => $descripcion,
			'fecha_creacion' => $fechafinalm,
			'nombrec' => $nombrec,
			'acciones' => $botones
		);
	
	}

	$result = array();
	$result['total'] = $i;
	$result['rows'] = $items;
	//$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'itemid';
	//$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
	echo json_encode($result);



?>