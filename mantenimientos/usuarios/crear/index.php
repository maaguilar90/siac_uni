<?php include ('../../../conexion.php'); ?>

<div style="border: 1px solid #ccc; padding-top: 5px;padding-right: 8px;padding-left: 8px;padding-bottom: 5px; margin-top: 8px;">
	<img src="images/guardar.png" title="Guardar Usuario" onclick="javascript: $('#formusuario').submit();" />
</div>

<div>
	
	<div>
		<div style="line-height: 25px;  float: lxeft; ">
			<form id="formusuario" method="post"  enctype="multipart/form-data">
            <div style="border:1px dotted #ccc; margin-top:0px; padding:10px;">
               
                <div style="padding-top: 0px;">
                <span>Avatar: </span><br>
                         <img id="image1" style="height:64px; width=64px;  float: left; padding-right: 20px;" src="images/user.png" />
                        <br><input id="f1" value="" class="f1 easyui-filebox" name="file" style="width:280px;" data-options="
                            prompt:'Seleccione la imagen...',buttonText:'Seleccionar',required:true, missingMessage:'Campo Obligatorio',
                            onChange: function(value){
                                var f = $(this).next().find('input[type=file]')[0];
                                if (f.files && f.files[0]){
                                    var reader = new FileReader();
                                    reader.onload = function(e){
                                        $('#image1').attr('src', e.target.result);
                                    }
                                    reader.readAsDataURL(f.files[0]);
                                }
                            }">
                </div>
                <div style="padding-top: 35px;">
                    <span>Usuario</span>
                    <input id="txtusuario" name="txtusuario" class="easyui-textbox"  type="text" size="25" data-options="required:true, missingMessage:'Campo Obligatorio',validType:'Length[3]'"/>
                    <br>
                </div>
                <div style="padding-top: 5px;">
                    <span>Contraseña</span>
                    <input id="txtpass" name="txtpass" class="easyui-validatebox"  type="password" size="25" data-options="required:true, missingMessage:'Campo Obligatorio',validType:'Length[3]'"/>
                    <br>
                </div>
                <div style="padding-top: 5px;">
                    <span>Confirmar Contraseña</span>
                    <input id="txtconfpass" name="txtconfpass" class="easyui-validatebox"  type="password" size="25" data-options="required:true, missingMessage:'Campo Obligatorio',validType:'Length[3]'"/>
                    <br>
                </div>
                <div style="padding-top: 5px;">
                    <span>Cédula</span>
                    <input id="txtcedula" name="txtcedula"  class="easyui-textbox" type="text" size="25" maxlength="10"  value=""  data-options="required:true, missingMessage:'Campo Obligatorio',validType:'CedulaLength[10]'"/>
                    <br>
                </div>
                <div style="padding-top: 5px;">
                    <span>Nombres</span>
                    <input id="txtnombres" name="txtnombres" class="easyui-textbox"  type="text" size="25" value="" data-options="required:true, missingMessage:'Campo Obligatorio',validType:'Length[3]'"/>
                    <br>
                </div>
                <div style="padding-top: 5px;">
                    <span>Apellidos</span>
                    <input id="txtapellidos" name="txtapellidos" class="easyui-textbox"  type="text" size="25" value="" data-options="required:true, missingMessage:'Campo Obligatorio',validType:'Length[3]'"/>
                    <br>
                </div>
                 <div style="padding-top: 5px;">
				<span>Empresa</span>
				<select id="sltempresa" name="sltempresa" class="easyui-combobox" data-options="editable:false,required:true">
					<?php 
						$query="CALL SP_SL_EMPRESA(1);";
						$sqlquery= executeQuery($query,"");
						while($fila=mysqli_fetch_object($sqlquery)){
					           $idempresa=$fila->idempresa; 
							   $desempresa=$fila->descripcion;
							   ?>
							   	<option value="<?=$idempresa?>" <?php if(@$empresa==$desempresa){ echo 'selected="selected"'; } ?> ><?=$desempresa?></option>
							   <?php
						} 
					?>
				</select>
				<br>
				</div>
                <div style="padding-top: 5px;">
				<span>Rol</span>
				<select id="sltroles" name="sltroles"  class="easyui-combobox" data-options="editable:false,required:true">
					<?php 
						$query="CALL SL_ROLES(0,1);";
						$sqlquery= executeQuery($query,"");
						while($fila=mysqli_fetch_object($sqlquery)){
					           $idrolg=$fila->id_rol; 
							   $descripcion=$fila->descripcion;
							   ?>
							   	<option value="<?=$idrolg?>" <?php if($idrolg==@$idrol){ echo 'selected="selected"'; } ?> ><?=$descripcion?></option>
							   <?php
						} 
					?>
				</select>
              
				<br>
				</div>
                <div style="padding-top: 5px;">
                    <span>Correo</span>
                    <input id="txtcorreo" name="txtcorreo" class="easyui-textbox" type="email" size="25" value=""  validType="email" data-options="required:true, missingMessage:'Campo Obligatorio',invalidMessage:'Ingrese un correo válido.'"/>
                    <br>
                </div>
                <div style="padding-top: 5px;">          
                            <span>Fecha de Nacimiento: </span>

                            <input id="txtfechanac" name="txtfechanac" class="easyui-datebox" data-options="required:true,formatter:myformatter,parser:myparser" style="height:26px" >
                           
                </div>
            </div>
        </form>		
   		</div>

	</div>	
	 
</div> 
<script type="text/javascript">
    $.extend($.fn.validatebox.defaults.rules, {
    CedulaLength: {
        validator: function(value, param){
            //alert(param[0]);
            //console.log(value.length);
            if (value.length == param[0])
            {
                //console.log(1);
                return true;
            }else{
                //console.log(2);
                return false;

            }
            
        },
        message: 'Número de Cédula Incorrecto.'
    }, Length: {
        validator: function(value, param){
            //alert(param[0]);
            //console.log(value.length);
            if (value.length >= param[0])
            {
                //console.log(1);
                return true;
            }else{
                //console.log(2);
                return false;

            }
            
        },
        message: 'Valor Incorrecto.'
    }
});

    $(function(){

$('#formusuario').form({
            url:'mantenimientos/usuarios/insertarusuario.php',
            onSubmit:function(){
                var espere=mensaje('Espere...','procesar','100px','');
                //console.log('Inicio Form')
                return $(this).form('validate');
            },
            success:function(data){
                console.log(data)
                if (data==1)
                {
                    console.log(data)
                    mensaje('Usuario ingresado con éxito.','ok','','');
                }else{
                    mensaje('Error al ingresar al usuario','error','','');
                }
                //$.messager.alert('Información', data, 'info',function()
                  //  {

                       // $('#dlg').dialog('close');
                    //});

            },
            error:function(data){
                $.messager.alert('Información', 'Error al actualizar.', 'info');
            }
        });

    });
 
</script>
