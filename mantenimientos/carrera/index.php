<?php $titulo="Carreras";   ?>
<div style="border: 1px solid #ccc;padding-right: 8px;padding-left: 8px;padding-bottom: 5px;padding-top: 5px;">
	<div>
	  <a onclick="javascript:$('#w').window('open');" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'">Agregar</a>
	  <a onclick="javscript:exportar_excel_estudiantes(<?=$_SESSION['codusuario']?>);" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-export-excel'">Exportar</a>	 
	</div> 
</div>
<br>
<div>
 <span class="titulo">Mantenimientos de Carreras</span>
 
<div style="margin-top:10px; heigth:10px;"></div>
		<?php
			$style="width:100% !important;height:68%;min-height:68%;";
			$dataoptions="view:scrollview,rownumbers:true,method:'post',queryParams:{editar:'editarCarrera(id)',eliminar:'eliminarCarrera(id)'},singleSelect:true,
				url:'mantenimientos/carrera/carrera.query.php?idc=1',autoRowHeight:false,pageSize:50,loadMsg:'Procesando su requerimiento, espere...'";
			$handler="";
			$sort="";
			$sortname="";

			$campos=array(
				array('campo'=> 'carrera' ,'titulo'=>$titulo,'width'=>'190','align'=>'left'),
				array('campo'=>'fechacreacion','titulo'=>'Fecha Creación','width'=>'150','align'=>'left'),
				array('campo'=>'acciones','titulo'=>'Acciones','width'=>'80','align'=>'center')
				);

			$gridcarrera= new grid();
			$gridcarrera= $gridcarrera->get_Grid_Html(strtolower($titulo),$titulo,$style,$sort,$sortname,$dataoptions,$campos);
			echo $gridcarrera;
		?>
		
	<div id="w" class="easyui-window" title="Agregar Carreras" data-options="modal:true,closed:true,closable:true,iconCls:'icon-save',onBeforeClose:function(){  location.reload();}" style="width:400px;height:250px;padding:10px;">
	        <?php
					include('mantenimientos/carrera/crear/index.php');
			?>
	</div>
</div>

<script type="text/javascript">

</script>