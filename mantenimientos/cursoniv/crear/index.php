<?php include_once('../../../conexion.php'); ?>
<?php include_once('../../../class/clasesgenerales.class.php'); ?>

<div style="border: 1px solid #ccc; padding-top: 5px;padding-right: 8px;padding-left: 8px;padding-bottom: 5px; margin-top: 3px;">
	<div>
		<img src="images/guardar.png" title="Guardar Curso Nivelación" onclick="javascript:agregarCursoNivelacion();" />
	</div> 		
</div>
<div>
<form action="post" id="frmcursoniv">
	<div>
		<div style="line-height: 25px;   padding-right: 30px;">
			<div style="margin-top:10px">
				<span class="titulo">Agregar Curso Nivelación</span>
			</div>

		<table border="0" style="border:none;">
			<tr style="border:none;">
				<td style="border:none;"><span>Curso Nombre</span></td>
				<td style="border:none;"><input id="txtnombres" type="text" size="21" value=""/></td>
			</tr>
			<tr>
				<td style="border:none;"><span>Año Lectivo&nbsp;&nbsp;</span></td>
				<td style="border:none;">
					<select id="slt_periodo" name="slt_periodo" class="easyui-combobox" style="width:80%;" data-options="editable:false" >
						<?php
						    $query="CALL SP_SL_ANIO_LECTIVO(1);";
							$sqlquery= executeQuery($query,"");
							$cont=0;
									while($fila=mysqli_fetch_object($sqlquery))
									{
								           $cont=$cont+1;
								           $id_lectivo=$fila->id_lectivo; 
										   $descripcion=$fila->anio_desde.' - '.$fila->anio_hasta;
						?>
					    	<option value="<?=$id_lectivo?>"><?=$descripcion?></option>
					    <?php
					    	}
					    ?>
					</select>
				</td>
			
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Fecha Inicio: </span></td>
				<td style="border:none;"><input id="txtfechainicio" name="txtfechainicio" style="width:120px;"  class="easyui-datebox" value="<?= date('d/m/Y') ?>"></td>
			</tr>
			<tr>
				<td style="border:none;"><span>Fecha Fin: </span></td>
				<td style="border:none;"><input id="txtfechafin" name="txtfechafin" style="width:120px;"  class="easyui-datebox" value="<?= date('d/m/Y') ?>"></td>

			</tr>
 			
	</table>

			
		</div>
		
	</div>	
</form>	 


</div>
<style type="text/css">
	tr
	{
		border: none;
	}
</style>