
<div style="border: 1px solid #ccc;padding-right: 8px;padding-left: 8px;padding-bottom: 5px;padding-top: 5px;">
	<div>
	  <a onclick="javascript:openagregarCursoNivelacion()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'">Agregar</a>
	  <a onclick="javscript:exportar_excel_estudiantes(<?=$_SESSION['codusuario']?>);" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-export-excel'">Exportar</a>	 
	</div> 
</div>
<br>
<div>
 <span class="titulo">Mantenimientos de Curso - Nivelación</span>
 	<div style="margin-top:10px;">
 	<span>Seleccione el curso: &nbsp;</span>
		<select id="slt_anio" name="slt_anio"class="easyui-combobox" style="width:20%;" data-options="editable:false" >
			<option value="0">TODOS</option>
			<?php
			    $query="CALL SP_SL_ANIO_LECTIVO(1);";
				$sqlquery= executeQuery($query,"");
				$cont=0;
						while($fila=mysqli_fetch_object($sqlquery))
						{
					           $cont=$cont+1;
					           $id_lectivo=$fila->id_lectivo; 
							   $descripcion=$fila->anio_desde.' - '.$fila->anio_hasta;
			?>
		    	<option value="<?=$id_lectivo?>"><?=$descripcion?></option>
		    <?php
		    	}
		    ?>
		</select>
	</div>
	<!--<div style="margin-top:8px;">
		<?php
			$style="";
			$options="";
			$handler="";

			$panelestudiantes= new grid();
			$panelestudiantes= $panelestudiantes->get_Panel_Html('estudiantes','Estudiantes',$style,$options,$handler,'');
			//echo $panelestudiantes;
		?>
	</div>-->
	<div style="margin-top:10px; heigth:10px;"></div>
		<?php
			$style="width:100% !important;height:96%;min-height:96%;";
			$dataoptions="view:scrollview,rownumbers:true,singleSelect:true,
				queryParams:{editar:'editarCursoNivelacion(id)',eliminar:'eliminarCursoNivelacion(id)'},
				url:'mantenimientos/cursoniv/cursoniv.query.php',autoRowHeight:false,pageSize:50,loadMsg:'Procesando su requerimiento, espere...'";
			$handler="";
			$sort="";
			$sortname="";

			$campos=array(
				array('campo'=>'descripcion','titulo'=>'Curso - Niv','width'=>'180','align'=>'left'),
				array('campo'=>'periodolectivo','titulo'=>'Periodo Lectivo','width'=>'150','align'=>'left'),
				array('campo'=>'fechainicio','titulo'=>'Fecha Inicio','width'=>'150','align'=>'left'),
				array('campo'=>'fechafin','titulo'=>'Fecha Fin','width'=>'150','align'=>'left') ,
				array('campo'=>'acciones','titulo'=>'Acciones','width'=>'80','align'=>'Center') ,
				);

			$gridestudiantes= new grid();
			$gridestudiantes= $gridestudiantes->get_Grid_Html('cursonive','Cursos - Nivelación',$style,$sort,$sortname,$dataoptions,$campos);
			echo $gridestudiantes;
		?>
		
	<div id="agregarCursoNivelacion" class="easyui-window" title="Agregar Curso - Nivelación" data-options="modal:true,closed:true,closable:true,iconCls:'icon-save',onBeforeClose:function(){  actualizar_grid(); }" style="width:360px;height:350px;padding:10px;">
	        
	</div>
</div>

<script type="text/javascript">
 $('#slt_anio').combobox({
        onChange: function(value){ 
          	console.log(value);     
          	$('#cursonive').datagrid('loadData', {"total":0,"rows":[]});
          	$('#cursonive').datagrid('load', 'mantenimientos/cursoniv/cursoniv.query.php?idlectivo='+value);

          	//$('#estudiantes').datagrid({ height: '500px' });
          	//$('#estudiantes').datagrid('reload');
			//$('#grid').panel('refresh', 'mantenimientos/estudiantes/estudiantecurso.query.php?idc='+value);        
}});
 function actualizar_grid()
 {
 	var value=$('#slt_anio').combobox('getValue');
 		$('#cursonive').datagrid('loadData', {"total":0,"rows":[]});
        $('#cursonive').datagrid('load', 'mantenimientos/cursoniv/cursoniv.query.php?idlectivo='+value);
 }
</script>