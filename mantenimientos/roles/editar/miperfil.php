<head>

</head>
<?php
	$query="CALL SL_USUARIO_ID('".$_SESSION['codusuario']."');";
	$sqlquery= executeQuery($query,"");
	while($fila=mysqli_fetch_object($sqlquery)){
           $idusuario=$fila->id_usuario; 
		   $usuario=$fila->usuario;
		   $nombres=$fila->nombres;
		   $apellidos=$fila->apellidos;
		   $correo=$fila->correo;
		   $fechanac=$fila->fechanacimiento;
		   $idrol=$fila->id_rol;
		   $empresa=$fila->empresa;
		   $cedula=$fila->cedula;

		   $fechanac=str_replace("-", "/", $fechanac);
	} 
?>
<div style="border: 1px solid #ccc; padding-top: 5px;padding-right: 8px;padding-left: 8px;padding-bottom: 5px;">
	<?php include ('botones.php'); ?>
</div>
<br>
<div>
	
	<div>
		<div style="line-height: 25px;  float: lxeft; padding-right: 30px;">
			<div>
				<span class="titulo">Mi perfil</span>
			</div>
			<div style="padding-top: 5px;">
				<span>Usuario</span>
				<input id="txtusuario" type="text" size="25" value="<?=$usuario?>" />
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Contraseña</span>
				<input id="txtpass" type="password" size="25" />
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Confirmar Contraseña</span>
				<input id="txtconfpass" type="password" size="25" />
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Rol</span>
				<select id="sltroles">
					<?php 
						$query="CALL SL_ROLES(1);";
						$sqlquery= executeQuery($query,"");
						while($fila=mysqli_fetch_object($sqlquery)){
					           $idrolg=$fila->id_rol; 
							   $descripcion=$fila->descripcion;
							   ?>
							   	<option id="<?=$idrolg?>" <?php if($idrolg==$idrol){ echo 'selected="selected"'; } ?> ><?=$descripcion?></option>
							   <?php
						} 
					?>
				</select>
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Cédula</span>
				<input id="txtcedula" type="text" size="25" maxlength="10"  value="<?=$cedula?>" />
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Nombres</span>
				<input id="txtnombres" type="text" size="25" value="<?=$nombres?>"/>
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Apellidos</span>
				<input id="txtapellidos" type="text" size="25" value="<?=$apellidos?>" />
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Correo</span>
				<input id="txtcorreo" type="email" size="25" value="<?=$correo?>" />
				<br>
			</div>
			<div style="padding-top: 5px;">			 
						<span>Fecha de Nacimiento: </span>
						<input id="date1" name="date1" type="text" readonly="" class="calendar" value="<?=($fechanac)?>">				
						<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Empresa</span>
				<select id="sltempresa">
					<?php 
						$query="CALL SP_SL_EMPRESA(1);";
						$sqlquery= executeQuery($query,"");
						while($fila=mysqli_fetch_object($sqlquery)){
					           $idempresa=$fila->idempresa; 
							   $desempresa=$fila->descripcion;
							   ?>
							   	<option id="<?=$idempresa?>" <?php if($empresa==$desempresa){ echo 'selected="selected"'; } ?> ><?=$desempresa?></option>
							   <?php
						} 
					?>
				</select>
				<br>
			</div>
			
				<div class="calendar" style="left: -1000px; visibility: hidden; zoom: 1; opacity: 0; position: absolute; top: -1000px; z-index: 1000;"></div>
				<div class="calendar2" style="left: -1000px; visibility: hidden; zoom: 1; opacity: 0; position: absolute; top: -1000px; z-index: 1000;"></div>
		
		
			<script type="text/javascript">
		   		
			//<![CDATA[
				window.addEvent('domready', function() { 
				  myCal1 = new Calendar({ date1: 'Y/m/d' }, { blocked: ['calendar'], direction: 1, tweak: { x: 6, y: 0 } });
				  myCal2 = new Calendar({ date2: 'Y/m/d' }, { classes: ['calendar2'], direction: 1, tweak: {x: 6, y: 0} });
			
				});
			//]]>
			  	</script>
		
		</div>
		<div id="dvbusqueda" style="float: left; padding-left: 20px; border-left: 1px dashed #ccc; height: 80%; display: none; line-height: 25px;">
			<div>
				<span class="titulo">Búsqueda</span>
			</div>
			<div style="padding-top: 5px;">
				<span>Nombre del Proyecto</span>
				<input type="text" size="25" />
				
			</div>
			<div id="dvbusquedacont" style="padding-top: 5px;">
				<span class="titulo" style="color: #666;">Resultado de búsqueda</span>
				<div id="dvresultadobus">
					
				</div>
			</div>
		</div>
	</div>	
</div>