<?php include_once('../../../conexion.php'); ?>
<?php include_once('../../../class/clasesgenerales.class.php'); ?>
<div style="border: 1px solid #ccc; padding-top: 5px;padding-right: 8px;padding-left: 8px;padding-bottom: 5px; margin-top: 3px;">
	<?php include ('botones.php'); ?>
</div>
<div>
	<div id="errorMessageag" class="errorMessage"></div>
	<div id="errorag" class="error" style="display:none;"></div>
	<div id="registradoag" style="display:none ; margin-bottom: 10px; padding: 5px; width: 210px; padding-top: 10px;   border: 1px solid#ccc; background: -webkit-gradient(linear,left top,left bottom,from(#FAFAFA),to(#eaeaea));" >
	</div>
	<div>
		<div style="line-height: 25px;   padding-right: 30px;">
			<div>
				<span class="titulo">Agregar Estudiante</span>
			</div>

		<table border="0" style="border:none;">
			<tr style="border:none;">
				<td style="border:none;"><span>Apellidos&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtapellidos" type="text" size="21" value="" /></td>
				<td style="border:none;"><span>Nombres&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtnombres" type="text" size="21" value=""/></td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Cédula&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtcedula"  size="21" maxlength="10"  value="" /></td>
				<td style="border:none;display: none">
					<span>Curso</span>&nbsp;&nbsp;</td>
				<td style="border:none;display: none">
					<select id="sltcurso">
						<?php 
							$query="CALL SP_SL_CURSOS(1);";
							$sqlquery= executeQuery($query,"");
							while($fila=mysqli_fetch_object($sqlquery)){
						           $idcurso=$fila->id_curso; 
								   $descripcion=$fila->descripcion;
								  ?>
								   	<option value="<?=$idcurso?>" ><?=$descripcion?></option>
								   <?php
							} 
						?>
					</select>
				</td>
			
			<td style="border:none;"><span>Fecha de Nac.:&nbsp; </span></td>
				<td style="border:none;"><input id="txtfecha" name="txtfecha" style="width:120px;"  class="easyui-datebox" value=""></td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Correo&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtcorreo" type="email" size="21" value="" /></td>
				<td style="border:none;"><span>Dirección&nbsp;&nbsp;</span></td>
						<td style="border:none;"><textarea id="txtdireccion" style="    width: 182px;"></textarea></td>		
			</tr>
			<tr style="border:none;">
						
						<td style="border:none;"><span>Teléfono&nbsp;&nbsp;</span></td>
						<td style="border:none;"><input id="txttelefono" size="21" value="" /></td>
						<td style="border:none;"><span>Celular&nbsp;&nbsp;</span></td>
						<td style="border:none;"><input id="txtcelular" id="txtcelular" size="21" value="" /></td>
			</tr>
			<tr style="border:none;">
						
						<td style="border:none;"><span>Sexo&nbsp;&nbsp;</span></td>
						<td style="border:none;"><select id="sltsexo"><option value="1">Masculino</option><option value="2">Femenino</option></select></td>
						 <td style="border:none;"><span>Inst. Educativa</span></td>
						<td style="border:none;"><input id="txtcolact" id="txtcolact" size="21" value="" /></td>
						
			</tr>
			<tr style="border:none;">
						<td style="border:none;"><span>Curso</span></td>
						<td style="border:none;"><input id="txtcuract" id="txtcuract" size="21" value="" /></td>
						<td style="border:none;"><span>Paralelo&nbsp;</span></td>
						<td style="border:none;"><input id="txtparal" id="txtparal" size="21" value="" /></td>
			</tr>
			<tr style="border:none; ">
						
						<td style="border:none;"><span>Especialización&nbsp;</span></td>
						<td style="border:none;"><input id="txtespe" id="txtespe" size="21" value="" /></td>
			</tr>
	</table>
	<td style="border:none;"><input type="hidden" id="txtniv" name="txtniv"  /></td>

	<span style="color:#666; font-weight:bold;">Datos del Representante</span>
	<table border="0" style="border:none;">
			<tr style="border:none;">
				<td style="border:none;"><span>Nombres&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtnombrepa" type="text" size="21" value="" /></td>
				<td style="border:none;display: none;"><span>Cédula&nbsp;&nbsp;</span></td>
				<td style="border:none;display: none;"><input id="txtcedulapa"  size="21" maxlength="10" /></td>
				<td style="border:none;"><span>Teléfono&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txttelefonopa" size="21" value="" /></td>
				
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Celular:&nbsp; </span></td>
				<td style="border:none;"><input id="txtcelularpa" size="21" style="width:120px;" value=""></td>
				<td style="border:0px;"><span>Fecha de Ingreso</span></td>
		<td style="border:0px;"><input id="txtfechaingr" name="txtfechaingr" style="width:120px;"  class="easyui-datebox" value=""></td>
				<td style="border:none;display: none;"><span>Dirección&nbsp;&nbsp;</span></td>
				<td style="border:none;display: none;"><textarea id="txtdireccionpa" style="    width: 182px;"></textarea></td>
				<td style="border:none;display: none;"><span>Ocupación&nbsp;&nbsp;</span></td>
				<td style="border:none;display: none;"><input id="txtocupacionpa" type="text" size="21" value=""/></td>
			</tr>
			<tr style="border:none;">
				
			</tr>
	</table>

	<!--<span style="color:#666; font-weight:bold;">Datos de la Madre</span>-->
	<table border="0" style="border:none;display: none;">
			<tr style="border:none;">
				<td style="border:none;"><span>Nombres&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtnombrema" type="text" size="21" value="" /></td>
				<td style="border:none;"><span>Cédula&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtcedulama"  size="21" maxlength="10" /></td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Dirección&nbsp;&nbsp;</span></td>
				<td style="border:none;"><textarea id="txtdireccionma" style="    width: 182px;"></textarea></td>
				<td style="border:none;"><span>Ocupación&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtocupacionma" type="text" size="21" value=""/></td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Teléfono&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txttelefonoma" size="21" value="" /></td>
				<td style="border:none;"><span>Celular:&nbsp; </span></td>
				<td style="border:none;"><input id="txtcelularma" style="width:120px;" size="21" value=""></td>
				
			</tr>
	</table>	

  <div style="margin:20px 0;"></div>
    <div id="source" style="border:1px solid #ccc;width:200px;height:140px;float:left;margin:5px;  overflow-y: auto;">
   	<div style="text-align:center; color:white; background:#388FCB; ">Cursos Disponibles</div>
   	<?php
   		$query="CALL SP_SL_CURSO_NIVELACION(3,1);";
   		$sqlquery= executeQuery($query,"");
				$cont=0;
				$idJQ="";
						while($fila=mysqli_fetch_object($sqlquery))
						{
					           $cont=$cont+1;
					           $idcursoniv=$fila->id;  
					           $descripcion=$fila->descripcion;
					           if ($cont>1){ $idJQ=$idJQ.','; }
					           $idJQ=$idJQ.'#'.$idcursoniv;  

							  /* $descripcion=$fila->anio_desde.' - '.$fila->anio_hasta;*/
			?>
    	        <div id="<?=$idcursoniv?>" class="drag"><?=$descripcion?></div>
		    <?php
		    	}
		    ?>

    </div>
    <div id="target" style="border:1px solid #ccc;width:200px;height:140px;float:left;margin:5px;    overflow-y: auto;">
     <div style="text-align:center; color:white; background:#388FCB; ">Cursos Actuales</div>  
    </div>


		</div>
	
	</div>	
	 
</div>

 <style type="text/css">
        .drag{
            width:160px;
            height:10px;
            padding:5px;
            margin:5px;
            border:1px solid #388FCB;

            
            line-height: 10px;
        }
        .dp{
            opacity:0.5;
            filter:alpha(opacity=50);
        }
        .over{
            background:#FBEC88;
        }
    </style>
    <script>
        $(function(){
            $('.drag').draggable({
                proxy:'clone',
                revert:true,
                cursor:'auto',
                onStartDrag:function(){
                    $(this).draggable('options').cursor='pointer';
                    $(this).draggable('proxy').addClass('dp');
                },
                onStopDrag:function(){
                    $(this).draggable('options').cursor='auto';
                }
            });
       
      
            $('#target').droppable({
                accept:'<?=$idJQ; ?>',
                onDragEnter:function(e,source){
                    $(source).draggable('options').cursor='auto';
                    $(source).draggable('proxy').css('border','1px solid red');
                    $(this).addClass('over');
                    console.log($(source).attr("id"));
                    var id=$(source).attr("id");
                    $('#txtniv').val(id)
                },
                onDragLeave:function(e,source){
                    $(source).draggable('options').cursor='pointer';
                    $(source).draggable('proxy').css('border','1px solid #ccc');
                    $(this).removeClass('over');
                },
                onDrop:function(e,source){
                    $(this).append(source)
                    $(this).removeClass('over');
                }
            });
             $('#source').droppable({
                 accept: '<?=$idJQ; ?>',
                onDragEnter:function(e,source){
                    $(source).draggable('options').cursor='auto';
                    $(source).draggable('proxy').css('border','1px solid red');
                    $(this).addClass('over');
                },
                onDragLeave:function(e,source){
                    $(source).draggable('options').cursor='not-allowed';
                    $(source).draggable('proxy').css('border','1px solid #ccc');
                    $(this).removeClass('over');
                },
                onDrop:function(e,source){
                    $(this).append(source)
                    $(this).removeClass('over');
                }
            });
            
        });
    </script>