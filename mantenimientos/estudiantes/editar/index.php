<?php include_once('../../../conexion.php'); ?>
<?php include_once('../../../class/clasesgenerales.class.php'); ?>
<?php
	$idestudiante=$_GET['id'];
	$query="CALL SP_SL_ESTUDIANTES(".$idestudiante.",1);";
	$sqlquery= executeQuery($query,"");

	while($fila=mysqli_fetch_object($sqlquery)){
		           $idestudiante=$fila->id_alumno; 
				   $nombres=$fila->nombres;
				   $apellidos=$fila->apellidos;
				   $cedula=$fila->cedula;
				   $idcursoest=$fila->id_curso;
				   $nomcurso=$fila->nomcurso;
				   $correo=$fila->email;
				   $direccion=$fila->direccion;
				   $telefono=$fila->telefono;
				   $celular=$fila->celular;
				   $fecnac=$fila->fec_nac_est;
				   $sexo=$fila->sexo;
				   $colegio=$fila->colegio;
				   $paralelo=$fila->paralelo;
				   $cursoact=$fila->curso;
				   $especializacion=$fila->especializacion;
				   $nompadre=$fila->nompadre;
				   $telrepresentante=$fila->tel_representante;
				   $direcpadre=$fila->direc_padre;
				   $cedpadre=$fila->ced_padre;
				   $fonopadre=$fila->tel_representante;
				   $celpadre=$fila->fono2_padre;
				   $nommadre=$fila->nom_madre;
				   $direcmadre=$fila->direc_madre;
				   $cedmad=$fila->ced_mad;
				   $fonmad=$fila->fon_mad;
				   $fon2mad=$fila->fon2_mad;
				   $ocupmadre=$fila->ocup_madre;
				   $ocuppadre=$fila->ocupacion_padre;
				   
				   $fecnew= new funciones();
				   $fecnac=$fecnew->formatoFechaDMY($fecnac,'/');
//echo var_dump($fila);
	}

?>
<input type="hidden" id="idestudiante" name="idestudiante" value="<?=$idestudiante ?>">
<div style="border: 1px solid #ccc; padding-top: 5px;padding-right: 8px;padding-left: 8px;padding-bottom: 5px; margin-top: 3px;">
	<div>
	  <a onclick="javascript:actualizarEstudiante(<?=@$idestudiante ?>);" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-update'">Actualizar</a> 
	</div> 
</div>
<div>
		<div>
		<div style="line-height: 25px;   padding-right: 30px;">
			<div>
				<span class="titulo">Editar Estudiante</span>
			</div>

		<table border="0" style="border:none;">
			<tr style="border:none;">
				<td style="border:none;"><span>Apellidos&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtapellidos" type="text" size="21" value="<?=$apellidos ?>" /></td>
				<td style="border:none;"><span>Nombres&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtnombres" type="text" size="21" value="<?=$nombres ?>"/></td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Cédula&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtcedula"  size="21" maxlength="10"  value="<?=$cedula ?>" /></td>
				<td style="border:none;">
					<span>Curso</span>&nbsp;&nbsp;</td>
				<td style="border:none;">
					<select id="sltcurso">
						<?php 
							$query="CALL SP_SL_CURSOS(1);";
							$sqlquery= executeQuery($query,"");
							while($fila=mysqli_fetch_object($sqlquery)){
						           $idcurso=$fila->id_curso; 
								   $descripcion=$fila->descripcion;
								  ?>
								   	<option value="<?=$idcurso?>" <?php if ($idcursoest==$idcurso) { echo 'selected="selected"'; } ?> ><?=$descripcion?></option>
								   <?php
							} 
						?>
					</select>
				</td>
			</tr>
			<tr style="border:none;">
			<td style="border:none;"><span>Fecha de Nac.:&nbsp; </span></td>
				<td style="border:none;"><input id="txtfecha" name="txtfecha" style="width:120px;"  class="easyui-datebox" value="<?=$fecnac ?>"></td>
				<td style="border:none;"><span>Correo&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtcorreo" type="email" size="21" value="<?=$correo ?>" /></td>
			</tr>
			<tr style="border:none;">
						<td style="border:none;"><span>Dirección&nbsp;&nbsp;</span></td>
						<td style="border:none;"><textarea id="txtdireccion" style="    width: 182px;"><?=$direccion ?></textarea></td>		
						<td style="border:none;"><span>Teléfono&nbsp;&nbsp;</span></td>
						<td style="border:none;"><input id="txttelefono" size="21" value="<?=$telefono ?>" /></td>
			</tr>
			<tr style="border:none;">
						<td style="border:none;"><span>Celular&nbsp;&nbsp;</span></td>
						<td style="border:none;"><input id="txtcelular" id="txtcelular" size="21" value="<?=$celular ?>" /></td>
						<td style="border:none;"><span>Sexo&nbsp;&nbsp;</span></td>
						<td style="border:none;">
							<select id="sltsexo">
								<option value="1" <?php if ($sexo==1) { echo 'selected="selected"'; } ?>>Masculino</option>
								<option value="2" <?php if ($sexo==2) { echo 'selected="selected"'; } ?>>Femenino</option>
							</select>
						</td>
			</tr>
			<tr style="border:none;">
						<td style="border:none;"><span>Colegio Ant.</span></td>
						<td style="border:none;"><input id="txtcolact" id="txtcolact" size="21" value="<?=$colegio ?>" /></td>
						<td style="border:none;"><span>Curso Act.</span></td>
						<td style="border:none;"><input id="txtcuract" id="txtcuract" size="21" value="<?=$cursoact ?>" /></td>
			</tr>
			<tr style="border:none;">
						<td style="border:none;"><span>Paralelo&nbsp;</span></td>
						<td style="border:none;"><input id="txtparal" id="txtparal" size="21" value="<?=$paralelo ?>" /></td>
						<td style="border:none;"><span>Especialización&nbsp;</span></td>
						<td style="border:none;"><input id="txtespe" id="txtespe" size="21" value="<?=$especializacion ?>" /></td>
			</tr>
	</table>
	<td style="border:none;"><input type="hidden" id="txtniv" name="txtniv"  /></td>

	<span style="color:#666; font-weight:bold;">Datos del Padre</span>
	<table border="0" style="border:none;">
			<tr style="border:none;">
				<td style="border:none;"><span>Nombres&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtnombrepa" type="text" size="21" value="<?=$nompadre ?>" /></td>
				<td style="border:none;"><span>Cédula&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtcedulapa"  size="21" maxlength="10"  value="<?=$cedpadre ?>" /></td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Dirección&nbsp;&nbsp;</span></td>
				<td style="border:none;"><textarea id="txtdireccionpa" style="    width: 182px;"><?=$direcpadre ?></textarea></td>
				<td style="border:none;"><span>Ocupación&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtocupacionpa" type="text" size="21" value="<?=$ocuppadre ?>"/></td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Teléfono&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txttelefonopa" size="21" value="<?=$fonopadre ?>" /></td>
				<td style="border:none;"><span>Celular:&nbsp; </span></td>
				<td style="border:none;"><input id="txtcelularpa" size="21" style="width:120px;" value="<?=$celpadre ?>"></td>
			</tr>
	</table>

	<span style="color:#666; font-weight:bold;">Datos de la Madre</span>
	<table border="0" style="border:none;">
			<tr style="border:none;">
				<td style="border:none;"><span>Nombres&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtnombrema" type="text" size="21" value="<?=$nommadre ?>" /></td>
				<td style="border:none;"><span>Cédula&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtcedulama"  size="21" maxlength="10" value="<?=$cedmad ?>" /></td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Dirección&nbsp;&nbsp;</span></td>
				<td style="border:none;"><textarea id="txtdireccionma" style="    width: 182px;"><?=$direcmadre ?></textarea></td>
				<td style="border:none;"><span>Ocupación&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txtocupacionma" type="text" size="21" value="<?=$ocupmadre ?>"/></td>
			</tr>
			<tr style="border:none;">
				<td style="border:none;"><span>Teléfono&nbsp;&nbsp;</span></td>
				<td style="border:none;"><input id="txttelefonoma" size="21" value="<?=$fonmad ?>" /></td>
				<td style="border:none;"><span>Celular:&nbsp; </span></td>
				<td style="border:none;"><input id="txtcelularma" style="width:120px;" size="21" value="<?=$fon2mad ?>"></td>
			</tr>
	</table>	

  <div style="margin:20px 0;"></div>
   <!-- <div id="source" style="border:1px solid #ccc;width:200px;height:140px;float:left;margin:5px;  overflow-y: auto;">
   	<div style="text-align:center; color:white; background:#388FCB; ">Cursos Disponibles</div>
   	<?php
   		$query="CALL SP_SL_CURSO_NIVELACION(3,1);";
   		$sqlquery= executeQuery($query,"");
				$cont=0;
						while($fila=mysqli_fetch_object($sqlquery))
						{
					           $cont=$cont+1;
					           $idcursoniv=$fila->id;  
					           $descripcion=$fila->descripcion;  

							  /* $descripcion=$fila->anio_desde.' - '.$fila->anio_hasta;*/
			?>
    	        <div id="<?=$idcursoniv?>" class="drag"><?=$descripcion?></div>
		    <?php
		    	}
		    ?>

    </div>
    <div id="target" style="border:1px solid #ccc;width:200px;height:140px;float:left;margin:5px;    overflow-y: auto;">
     <div style="text-align:center; color:white; background:#388FCB; ">Cursos Actuales</div>  
    </div>-->
<!--
<table  style="border:0px;" >
	<tr  style="border:0px;">
		<td style="border:0px;"><span>Fecha de Ingreso</span></td>
		<td style="border:0px;"><input id="txtfechaingr" name="txtfechaingr" style="width:120px;"  class="easyui-datebox" value=""></td>
	</tr>
</table>-->
		</div>
	
	</div>	
	 
</div>

 <style type="text/css">
        .drag{
            width:160px;
            height:10px;
            padding:5px;
            margin:5px;
            border:1px solid #388FCB;

            
            line-height: 10px;
        }
        .dp{
            opacity:0.5;
            filter:alpha(opacity=50);
        }
        .over{
            background:#FBEC88;
        }
    </style>
    <script>
        $(function(){
            $('.drag').draggable({
                proxy:'clone',
                revert:true,
                cursor:'auto',
                onStartDrag:function(){
                    $(this).draggable('options').cursor='pointer';
                    $(this).draggable('proxy').addClass('dp');
                },
                onStopDrag:function(){
                    $(this).draggable('options').cursor='auto';
                }
            });
       
      
            $('#target').droppable({
                accept:'#1,#2,#3,#4,#5,#6',
                onDragEnter:function(e,source){
                    $(source).draggable('options').cursor='auto';
                    $(source).draggable('proxy').css('border','1px solid red');
                    $(this).addClass('over');
                    console.log($(source).attr("id"));
                    var id=$(source).attr("id");
                    $('#txtniv').val(id)
                },
                onDragLeave:function(e,source){
                    $(source).draggable('options').cursor='pointer';
                    $(source).draggable('proxy').css('border','1px solid #ccc');
                    $(this).removeClass('over');
                },
                onDrop:function(e,source){
                    $(this).append(source)
                    $(this).removeClass('over');
                }
            });
             $('#source').droppable({
                 accept:'#1,#2,#3,#4,#5,#6',
                onDragEnter:function(e,source){
                    $(source).draggable('options').cursor='auto';
                    $(source).draggable('proxy').css('border','1px solid red');
                    $(this).addClass('over');
                },
                onDragLeave:function(e,source){
                    $(source).draggable('options').cursor='not-allowed';
                    $(source).draggable('proxy').css('border','1px solid #ccc');
                    $(this).removeClass('over');
                },
                onDrop:function(e,source){
                    $(this).append(source)
                    $(this).removeClass('over');
                }
            });
            
        });
    </script>