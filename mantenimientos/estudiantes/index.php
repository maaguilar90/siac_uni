
<div style="border: 1px solid #ccc;padding-right: 8px;padding-left: 8px;padding-bottom: 5px;padding-top: 5px;">
	<div>
	  <a onclick="javascript:openagregarEstudiante()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'">Agregar</a>
	  <a onclick="javscript:exportar_excel_estudiantes(<?=$_SESSION['codusuario']?>);" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-export-excel'">Exportar</a>	 
	</div> 
</div>
<br>
<div>
 <span class="titulo">Mantenimientos de Estudiantes</span>
 	<div style="margin-top:10px;">
 	<span>Seleccione el curso: &nbsp;</span>
		<select id="slt_curso" name="slt_curso"class="easyui-combobox" style="width:20%;" data-options="editable:false" >
			<?php
			    $query="CALL SP_SL_CURSOS(1);";
				$sqlquery= executeQuery($query,"");
				$cont=0;
						while($fila=mysqli_fetch_object($sqlquery))
						{
					           $cont=$cont+1;
					           $idcurso=$fila->id_curso; 
							   $descripcion=$fila->descripcion;
			?>
		    	<option value="<?=$idcurso?>"><?=$descripcion?></option>
		    <?php
		    	}
		    ?>
		</select>
	</div>
	<!--<div style="margin-top:8px;">
		<?php
			$style="";
			$options="";
			$handler="";

			$panelestudiantes= new grid();
			$panelestudiantes= $panelestudiantes->get_Panel_Html('estudiantes','Estudiantes',$style,$options,$handler,'');
			//echo $panelestudiantes;
		?>
	</div>-->
	<div style="margin-top:10px; heigth:10px;"></div>
	<div style="height:80%;min-height:80%;">
		<?php
			$style="width:100% !important;height:100%;min-height:100%;";
			$dataoptions="view:scrollview,rownumbers:true,singleSelect:true,
				url:'mantenimientos/estudiantes/estudiantecurso.query.php?idc=1',
				queryParams:{editar:'editarEstudiante(id)',eliminar:'eliminarEstudiante(id)'},
				autoRowHeight:false,pageSize:50,loadMsg:'Procesando su requerimiento, espere...'";
			$handler="";
			$sort="";
			$sortname="";

			$campos=array(
				array('campo'=>'cedula','titulo'=>'Cedula','width'=>'90','align'=>'left'),
				array('campo'=>'apellidos','titulo'=>'Apellidos','width'=>'150','align'=>'left'),
				array('campo'=>'nombre','titulo'=>'Nombres','width'=>'150','align'=>'left') ,
				array('campo'=>'telefono','titulo'=>'Teléfono','width'=>'80','align'=>'left') ,
				array('campo'=>'celular','titulo'=>'Celular','width'=>'80','align'=>'left') ,
				array('campo'=>'email','titulo'=>'Correo','width'=>'200','align'=>'left') ,
				array('campo'=>'direccion','titulo'=>'Dirección','width'=>'200','align'=>'left') ,
				array('campo'=>'acciones','titulo'=>'Acciones','width'=>'80','align'=>'Center') ,
				);

			$gridestudiantes= new grid();
			$gridestudiantes= $gridestudiantes->get_Grid_Html('estudiantes','Estudiantes',$style,$sort,$sortname,$dataoptions,$campos);
			echo $gridestudiantes;
		?>
		
	</div>
</div>
<div id="agregarEstudiante" class="easyui-window" title="Agregar Estudiante" data-options="modal:true,closed:true,closable:true,iconCls:'icon-save',onBeforeClose:function(){  actualizarGrid('#estudiantes','mantenimientos/estudiantes/estudiantecurso.query.php?idc='+ $('#slt_curso').combobox('getValue') );$('#agregarEstudiante').html('');  }" style="width:600px;height:550px;padding:10px;">
        <?php
				//include('mantenimientos/estudiantes/crear/index.php');
		?>
</div>
<div id="editarEstudiante" class="easyui-window" title="Editar Estudiante" data-options="modal:true,closed:true,closable:true,iconCls:'icon-save',onBeforeClose:function(){  actualizarGrid('#estudiantes','mantenimientos/estudiantes/estudiantecurso.query.php?idc='+ $('#slt_curso').combobox('getValue') ); $('#editarEstudiante').html('');  }" style="width:600px;height:550px;padding:10px;">
        <?php
				//include('mantenimientos/estudiantes/editar/index.php');
		?>
</div>

<script type="text/javascript">
 $('#slt_curso').combobox({
        onChange: function(value){ 
          	//console.log(value);     
          	actualizarGrid('#estudiantes','mantenimientos/estudiantes/estudiantecurso.query.php?idc='+value)
          	
}});


</script>