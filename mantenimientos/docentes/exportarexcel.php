<?php
session_start();
include_once('../../Mysqllocal.php');


error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Guayaquil');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Sistema de Notas")
							 ->setLastModifiedBy("Sistema de Notas")
							 ->setTitle("Docentes")
							 ->setSubject("PHPExcel")
							 ->setDescription("PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Docentes");


// Add some data
echo date('H:i:s') , " Add some data" , EOL;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'ID')
            ->setCellValue('B1', 'NOMBRES')
            ->setCellValue('C1', 'TELEFONO')
            ->setCellValue('D1', 'CELULAR')
            ->setCellValue('E1', 'EMAIL')
            ->setCellValue('F1', 'DIRECCION');

@$query= "CALL SP_SL_USUARIO_DOCENTES(0,'1');";
$res=executeQuery($query,'');
$cont=1;

while($fila=mysqli_fetch_object($res))
{
         $cont=$cont+1;
         $idmateria=$fila->id_usuario; 
	   $descripcion=$fila->apellidos.' '.$fila->nombres;
	   $telefono=$fila->telefono;
	   $celular=$fila->celular;
         $email=$fila->email;
         $direccion=$fila->direccion;
	  // $rol=$fila->id_rol;
	   $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$cont, $idmateria)

            ->setCellValue('B'.$cont, $descripcion)
            ->setCellValue('C'.$cont, $telefono)
            ->setCellValue('D'.$cont, $celular)
            ->setCellValue('E'.$cont, $email)
            ->setCellValue('F'.$cont, $direccion);
       $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
       $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
}



//$objPHPExcel->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
//$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
//$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);


// Rename worksheet
echo date('H:i:s') , " Rename worksheet" , EOL;
$objPHPExcel->getActiveSheet()->setTitle('Docentes');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Save Excel 2007 file
//echo date('H:i:s') , " Write to Excel2007 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

//$objWriter->save("materia".date("Ymd").".xlsx");
$objWriter->save("../../archivosexp/docentes".$_SESSION['codusuario'].".xlsx");

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Save Excel 95 file
echo date('H:i:s') , " Write to Excel5 format" , EOL;
$callStartTime = microtime(true);

/*$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(str_replace('.php', '.xls', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
*/
echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Echo memory peak usage
echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

// Echo done
echo date('H:i:s') , " Done writing files" , EOL;
echo 'Files have been created in ' , getcwd() , EOL;

//echo @res




?>

