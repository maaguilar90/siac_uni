<head>

</head>

<div style="border: 1px solid #ccc; padding-top: 5px;padding-right: 8px;padding-left: 8px;padding-bottom: 5px; margin-top: 8px;">
	<?php include ('botones.php'); ?>
</div>
<br>
<div>
	<div id="errorMessage" class="errorMessage"></div>
	<div id="error" class="error" style="display:none;"></div>
	<div id="registrado" style="display:none ; margin-bottom: 10px; padding: 5px; width: 210px; padding-top: 10px;   border: 1px solid#ccc; background: -webkit-gradient(linear,left top,left bottom,from(#FAFAFA),to(#eaeaea));" >
	</div>
	<div>
		<div style="line-height: 25px;  float: lxeft; padding-right: 30px;">
			<div>
				<span class="titulo">Agregar Usuario</span>
			</div>
			<div style="padding-top: 5px;">
				<span>Usuario</span>
				<input id="txtusuario" type="text" size="25" value="" />
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Contraseña</span>
				<input id="txtpass" type="password" size="25" />
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Confirmar Contraseña</span>
				<input id="txtconfpass" type="password" size="25" />
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Rol</span>
				<select id="sltroles">
					<?php 
						$query="CALL SL_ROLES(1);";
						$sqlquery= executeQuery($query,"");
						while($fila=mysqli_fetch_object($sqlquery)){
					           $idrolg=$fila->id_rol; 
							   $descripcion=$fila->descripcion;
							   ?>
							   	<option value="<?=$idrolg?>" <?php if($idrolg==@$idrol){ echo 'selected="selected"'; } ?> ><?=$descripcion?></option>
							   <?php
						} 
					?>
				</select>
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Cédula</span>
				<input id="txtcedula" type="text" size="25" maxlength="10"  value="" />
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Nombres</span>
				<input id="txtnombres" type="text" size="25" value=""/>
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Apellidos</span>
				<input id="txtapellidos" type="text" size="25" value="" />
				<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Correo</span>
				<input id="txtcorreo" type="email" size="25" value="" />
				<br>
			</div>
			<div style="padding-top: 5px;">			 
						<span>Fecha de Nacimiento: </span>
						<input id="txtfecha" name="txtfecha" type="text"  class="easyui-datebox" value="">				
						<br>
			</div>
			<div style="padding-top: 5px;">
				<span>Empresa</span>
				<select id="sltempresa">
					<?php 
						$query="CALL SP_SL_EMPRESA(1);";
						$sqlquery= executeQuery($query,"");
						while($fila=mysqli_fetch_object($sqlquery)){
					           $idempresa=$fila->idempresa; 
							   $desempresa=$fila->descripcion;
							   ?>
							   	<option value="<?=$idempresa?>" <?php if(@$empresa==$desempresa){ echo 'selected="selected"'; } ?> ><?=$desempresa?></option>
							   <?php
						} 
					?>
				</select>
				<br>
			</div>
			
			
		
		
		</div>
		<div id="dvbusqueda" style="float: left; padding-left: 20px; border-left: 1px dashed #ccc; height: 80%; display: none; line-height: 25px;">
			<div>
				<span class="titulo">Búsqueda</span>
			</div>
			<div style="padding-top: 5px;">
				<span>Nombre del Proyecto</span>
				<input type="text" size="25" />
				
			</div>
			<div id="dvbusquedacont" style="padding-top: 5px;">
				<span class="titulo" style="color: #666;">Resultado de búsqueda</span>
				<div id="dvresultadobus">
					
				</div>
			</div>
		</div>
	</div>	
	 
</div>
