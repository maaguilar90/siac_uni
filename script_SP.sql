/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.9-MariaDB : Database - dbnotas
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/* Procedure structure for procedure `ACCESO_MENU` */

/*!50003 DROP PROCEDURE IF EXISTS  `ACCESO_MENU` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `ACCESO_MENU`(
  IN `@tipo_usuario` TEXT
    )
BEGIN
  select superadmin,admin,mantenimientos,usuarios,ajustes,reportes,system from tbl_rol where id_estatus='1';
    END */$$
DELIMITER ;

/* Procedure structure for procedure `DATOS_CONFIG_SEL` */

/*!50003 DROP PROCEDURE IF EXISTS  `DATOS_CONFIG_SEL` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `DATOS_CONFIG_SEL`()
BEGIN
  SELECT nombresoft, VERSION, subversion, compilacion, titulo, estilo FROM tblconfig LIMIT 1;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `LOGIN_USER` */

/*!50003 DROP PROCEDURE IF EXISTS  `LOGIN_USER` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `LOGIN_USER`(
  IN `@usuario` TEXT,
  IN `@password` TEXT)
BEGIN
    
     SELECT count(u.id_usuario) as registros,u.id_usuario,u.usuario,u.tipo,u.id_rol,
  em.descripcion as empresa, ud.avatar, ud.fechanacimiento,ud.fechaingreso,ud.cedula,ud.nombres,ud.apellidos,
  ud.correo
     FROM tblusuarios u,tblusuariodetalle ud, tblempresa em 
     WHERE usuario=`@usuario` AND PASSWORD=MD5(`@password`) AND estatus='1' 
     and  ud.id_usuario=u.id_usuario AND em.idempresa=ud.id_empresa;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `LOG_ACCESO_LOGIN` */

/*!50003 DROP PROCEDURE IF EXISTS  `LOG_ACCESO_LOGIN` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `LOG_ACCESO_LOGIN`(
    IN `@id_usuario` TEXT,
    IN `@usuario` TEXT,
    IN `@ip` TEXT,
    IN `@navegador` TEXT
    )
BEGIN
  
  insert into tblacceso (id_usuario,usuario,ip,navegador,fecha_acceso) values (`@id_usuario`,`@usuario`,
  `@ip`,`@navegador`,current_timestamp);
    END */$$
DELIMITER ;

/* Procedure structure for procedure `LOG_ERROR_MYSQL` */

/*!50003 DROP PROCEDURE IF EXISTS  `LOG_ERROR_MYSQL` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `LOG_ERROR_MYSQL`(
  IN `@error` TEXT,
  IN `@query` VARCHAR(900)
    )
BEGIN
  INSERT INTO tbllogmysql values (NULL,`@error`,`@query`,NULL ) ;   
END */$$
DELIMITER ;

/* Procedure structure for procedure `LOG_LOGIN` */

/*!50003 DROP PROCEDURE IF EXISTS  `LOG_LOGIN` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `LOG_LOGIN`(
    IN `@usuario` TEXT,
    IN `@password` TEXT,
    IN `@ip` TEXT,
    IN `@navegador` TEXT,
    IN `@acceso` TEXT
    )
BEGIN
  insert into tbllog_acceso (usuario,password,ip,navegador,acceso,fecha_acceso) values (`@usuario`,`@password`,
  `@ip`,`@navegador`,`@acceso`,current_timestamp);
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SL_ROLES` */

/*!50003 DROP PROCEDURE IF EXISTS  `SL_ROLES` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SL_ROLES`(
  IN `@idrol` INT,
  IN `@estatus` INT
    )
BEGIN
  if `@idrol`>0 then
    SELECT id_rol, descripcion,superadmin,admin,mantenimientos,proyectos,notificaciones,visitasclientes,
    recursosh,sistema,gempresas,grecursos,gusuarios,groles,gproyectos,gtareas,galarmas,gestadisticas,proyectosagr,
    tareasagr,actividadesagr,idestatus 
    FROM tbl_rol WHERE idestatus=`@estatus` and id_rol=`@idrol`;
  else
    select id_rol, descripcion,superadmin,admin,mantenimientos,proyectos,notificaciones,visitasclientes,
    recursosh,sistema,gempresas,grecursos,gusuarios,groles,gproyectos,gtareas,galarmas,gestadisticas,proyectosagr,
    tareasagr,actividadesagr,idestatus 
    from tbl_rol where idestatus=`@estatus` order by descripcion asc;
  end if;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SL_USUARIO_ID` */

/*!50003 DROP PROCEDURE IF EXISTS  `SL_USUARIO_ID` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SL_USUARIO_ID`(
  IN `@idusuario` INT    
    )
BEGIN
  SELECT u.id_usuario,u.usuario,ud.nombres, ud.apellidos, ud.correo,ud.fechanacimiento, u.id_rol, 
  (select descripcion from tblempresa where idempresa=id_empresa ) as empresa, ud.cedula as cedula 
  FROM tblusuarios u, tblusuariodetalle ud 
  WHERE u.id_usuario=`@idusuario` and ud.id_usuario=u.id_usuario;
END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_DL_CURSO` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_DL_CURSO` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_DL_CURSO`(
  IN `@idcurso` INT
)
BEGIN
  delete from cursos where id_curso=`@idcurso`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_DL_ESTUDIANTES` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_DL_ESTUDIANTES` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_DL_ESTUDIANTES`(
  IN `@idestudiante` int
    )
BEGIN
  update alumnos set estado=9 where id_alumno=`@idestudiante`;   
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_DL_MATERIAS` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_DL_MATERIAS` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_DL_MATERIAS`(
  IN `@idmateria` INT
    )
BEGIN
  UPDATE materias SET estatus=9 where id_materia=`@idmateria`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_DL_MATERIA_CURSO` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_DL_MATERIA_CURSO` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_DL_MATERIA_CURSO`(
  IN `@idcursomateria` INT
    )
BEGIN
  delete from materia_por_curso where id_materia_curso=`@idcursomateria`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_IN_CURSO` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_IN_CURSO` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_IN_CURSO`(
  IN `@curso` text,
  IN `@calificaciona` INT,
  IN `@idestatus` INT
    )
BEGIN
  insert into cursos
  values
  (
    null,
    `@curso`,
    0,
    `@calificaciona` ,
    current_timestamp(),
    null,
    1
  );
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_IN_ESTUDIANTES` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_IN_ESTUDIANTES` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_IN_ESTUDIANTES`(
  in `@nombres` text,
  IN `@apellidos` TEXT,
  IN `@idcurso` int(11),
  IN `@direccion` TEXT,
  IN `@telefono` varchar(30),
  IN `@celular` varchar(10),
  IN `@email` varchar(80) ,
  IN `@nompadre` text,
  IN `@telefonopa` varchar(30),
  IN `@cedula` int(10),
  IN `@fechanac` date,
  IN `@sexo` int(1),
  IN `@direccionpa` text,
  IN `@cedulapa` int(10),
  IN `@celularpa` varchar(10) ,
  IN `@ocupacionpa` text,
  IN `@nombrema` text ,
  IN `@direccionma` text,
  IN `@cedulama` int(10),
  IN `@telefonoma` varchar(30),
  IN `@celularma` int(10),
  IN `@ocupacionma` text
    )
BEGIN
  INSERT INTO alumnos
  VALUES
  (
  NULL,
  `@nombres`,
  `@apellidos`,
  `@idcurso`,
  `@direccion`,
  `@telefono`,
  `@celular`,
  `@email`,
  `@nompadre`,
  `@telefonopa`,
  `@cedula`,
  1,
  `@fechanac`,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  `@sexo`,
  `@direccionpa`,
  `@cedulapa`,
  `@celularpa`,
  `@ocupacionpa`,
  NULL,
  NULL,
  `@nombrema`,
  `@direccionma`,
  `@cedulama`,
  `@telefonoma`,
  `@celularma`,
  `@ocupacionma`,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL
  );
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SL_USUARIO` */

/*!50003 DROP PROCEDURE IF EXISTS  `SL_USUARIO` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SL_USUARIO`(
  IN `@estatus` INT,
  IN `@campo` VARCHAR(100),
  IN `@orden` VARCHAR(100)
    )
BEGIN
  IF `@estatus`>0 THEN
    SELECT u.id_usuario,u.usuario,ud.nombres, ud.apellidos, ud.correo, u.id_rol, r.descripcion,
    fecha_creacion,(SELECT usuario FROM tblusuarios tu WHERE tu.id_usuario=u.usuarioC) AS nombrec,
    ud.avatar
    FROM tblusuarios u, tblusuariodetalle ud, tbl_rol r 
    WHERE  ud.id_usuario=u.id_usuario AND u.id_rol=r.id_rol AND estatus=`@estatus`
    ORDER BY
    CASE `@orden` WHEN 'desc' THEN
      CASE `@campo` WHEN 'id_usuario' THEN u.id_usuario
        WHEN 'usuario' THEN usuario
        WHEN 'nombres' THEN nombres
        WHEN 'apellidos' THEN apellidos
        WHEN 'correo' THEN correo
        WHEN 'descripcion' THEN descripcion
        WHEN 'fechacreacion' THEN fecha_creacion
        WHEN 'usuarioc' THEN usuarioc
      END
    END DESC,
    CASE `@orden` WHEN 'asc' THEN
      CASE `@campo` WHEN 'id_usuario' THEN u.id_usuario
        WHEN 'usuario' THEN usuario
        WHEN 'nombres' THEN nombres
        WHEN 'apellidos' THEN apellidos
        WHEN 'correo' THEN correo
        WHEN 'descripcion' THEN descripcion
        WHEN 'fechacreacion' THEN fecha_creacion
        WHEN 'usuarioc' THEN usuarioc
      END
    END 
    ;
     
  ELSE
    SELECT u.id_usuario,u.usuario,ud.nombres, ud.apellidos, ud.correo, u.id_rol, r.descripcion, ud.avatar
    FROM tblusuarios u, tblusuariodetalle ud , tbl_rol r
    WHERE  ud.id_usuario=u.id_usuario  AND u.id_rol=r.id_rol;
  END IF;
END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_IN_MATERIAS` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_IN_MATERIAS` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_IN_MATERIAS`(
  IN `@materia` text,
  IN `@calificacionalfa` INT,
  IN `@estatus` INT
    )
BEGIN
  INSERT INTO materias
  values
  (
    null,
    `@materia`,
    current_timestamp(),
    null,
    `@calificacionalfa`,
    0,
    `@estatus`
  );
  
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_IN_MATERIA_CURSO` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_IN_MATERIA_CURSO` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_IN_MATERIA_CURSO`(
  IN `@idcurso` INT,
  IN `@idmateria` INT
    )
BEGIN
  insert into materia_por_curso 
  values 
  (
    null,
    `@idcurso`,
    `@idmateria`,
    null
  );
  
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_IN_NOTAS_ESTUDIANTE` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_IN_NOTAS_ESTUDIANTE` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_IN_NOTAS_ESTUDIANTE`(
  IN `@idalumno` INT,
  IN `@idmateria` INT,
  IN `@idprofesor` INT,
  IN `@nota` decimal(10,2),
  IN `@notaalfa` varchar(1),
  IN `@idanio` INT,
  IN `@idparametro` INT,
  IN `@idparcial` INT,
  IN `@idperiododesc` INT,
  IN `@idaniolectivo` INT,
  IN `@idcurso` INT,
  IN `@usuariomod` INT,
  IN `@idparcialpar` INT,
  IN `@usuario` INT
    )
BEGIN
  insert into notas
  values
  (
    `@idalumno` ,
    `@idmateria` ,
    `@idprofesor` ,
    `@nota`,
    `@notaalfa` ,
    `@idanio` ,
    `@idparametro` ,
    `@idparcial` ,
    `@idperiododesc` ,
    `@idaniolectivo` ,
    `@idcurso` ,
    null,
    null,
    `@usuariomod` ,
    `@idparcialpar` ,
    `@usuario` 
  );
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_IN_PAGOS` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_IN_PAGOS` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_IN_PAGOS`(
	in `@idusuarioc` int(11),
	IN `@tipodoc` INT(11),
	IN `@cedulaest` varchar(10),
	IN `@valorpago` decimal(10,2),
	IN `@tipopago` INT(11),
	IN `@idrubro` INT(11),
	IN `@idestatus` INT(11)
    )
BEGIN
	SET @nombres=(SELECT CONCAT(apellidos,' ',nombres)AS estudiantes FROM `alumnos` WHERE `cedula`= '0930178462' );
	
	insert into tblcomprobantes
	values
	(
		null,
		`@cedulaest`,
		@nombres,
		`@valorpago`,
		CURRENT_TIMESTAMP(),
		`@idusuarioc`,
		1
	);
	SET @idcomprobante=(SELECT LAST_INSERT_ID() FROM tblcomprobantes);
	
	insert into tblpagos
	values
	(
		null,
		`@idusuarioc`,
		`@tipodoc`,
		@idcomprobante,
		@idcomprobante,
		`@cedulaest`,
		`@valorpago`,
		`@tipopago`,
		1,
		`@idrubro`,
		current_timestamp(),
		0,
		`@idestatus`
	);
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_IN_USUARIOS` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_IN_USUARIOS` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_IN_USUARIOS`(IN `@usuario` TEXT,
  IN `@password` TEXT,
  IN `@idrol` INT,
  IN `@cedula` VARCHAR(10),
  IN `@nombres` TEXT,
  IN `@apellidos` TEXT,
  IN `@correo` VARCHAR(100),
  IN `@fechanac` DATE,
  IN `@idempresa` INT,
  IN `@idmateria` INT,
  IN `@idusuarioc` INT,
  IN `@idestatus` INT 
    )
BEGIN
  DECLARE IdUltima INT;
  SET IdUltima = -1;
  INSERT INTO tblusuarios
  VALUES
  (
    NULL,
    `@usuario`,
    MD5(`@password`),
    `@idrol`,
    
    CURRENT_TIMESTAMP,
    `@idrol`,
    `@idusuarioc`,
    `@idestatus`
  );
  SELECT LAST_INSERT_ID() INTO IdUltima;
  
  INSERT INTO tblusuariodetalle 
  VALUES 
  ( 
    NULL,
    IdUltima,
    `@cedula`,
    `@nombres`,
    `@apellidos`,
    `@correo`,
    `@fechanac`,
    `@idempresa`,
    CURRENT_TIMESTAMP
  );
  
  
  
  
  INSERT INTO contactos 
  VALUES 
  ( 
    NULL,
    IdUltima,
    `@nombres`,
    `@apellidos`,
    `@idmateria`,
    "",
    "",
    `@correo`,
    ""
  );
  
  
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_ANIO_LECTIVO` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_ANIO_LECTIVO` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_ANIO_LECTIVO`(
  IN `@idestatus` INT
)
BEGIN
  select id_lectivo,inicio_lectivo,fin_lectivo,anio_desde,anio_hasta,fechacreacion,idestatus
  from lectivo where idestatus=1 order by anio_desde desc;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_CLASESPRINCIPAL` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_CLASESPRINCIPAL` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_CLASESPRINCIPAL`(
  IN `@idestatus` INT
    )
BEGIN
  SELECT id,get2,url,idestatus
  FROM tblclasesprincipal
  WHERE idestatus=`@idestatus`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_CURSONIV` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_CURSONIV` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_CURSONIV`(
  IN `@idestatus` INT(2)
    )
BEGIN
  select `id`,`idperiodolectivo`,`descripcion`,`fechainicio`,`fechafin`,`fechacreacion`,`idusuarioc`,`idestatus`
  from `tblcursonivel` where `idestatus`=`@idestatus`;
  
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_CURSOS` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_CURSOS` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_CURSOS`(
  IN `@idestatus` INT
    )
BEGIN
  select descripcion,cal_alfa,fechacreacion,fechamod,dirigente,id_curso
  from cursos;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_CURSOS_IDNAME` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_CURSOS_IDNAME` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_CURSOS_IDNAME`(
  IN `@idcurso` INT,
  IN `@idestatus` INT
 )
BEGIN
  select descripcion,cal_alfa,fechacreacion,fechamod,dirigente,id_curso
  FROM cursos where id_curso=`@idcurso` and estatus=`@idestatus`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_EMPRESA` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_EMPRESA` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_EMPRESA`(
  IN `@estatus` INT
    )
BEGIN
  SELECT idempresa,descripcion,razonsocial,telefono,telefono2,direccion,representantel,fechacreacion,idestatus
  FROM tblempresa 
  WHERE idestatus=`@estatus`; 
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_MATERIAS` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_MATERIAS` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_MATERIAS`(
  IN `@estatus` INT
    )
BEGIN
  select id_materia, descripcion, fechacreacion, fechamod, calificacion_alfa,orden,estatus
  from materias where estatus=`@estatus`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_MATERIAS_CURSO` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_MATERIAS_CURSO` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_MATERIAS_CURSO`(
  IN `@idcurso` INT,
  IN `@idmateria` INT
    )
BEGIN
  if `@idmateria` > 0 then
    select mpc.id_materia_curso,m.descripcion,m.id_materia,m.calificacion_alfa,c.id_curso
    from materia_por_curso mpc, materias m, cursos c
    where mpc.id_curso=c.id_curso and m.id_materia=mpc.id_materias
    and c.id_curso=`@idcurso` and mpc.id_materias=`@idmateria`;
  else
    SELECT mpc.id_materia_curso,m.descripcion,m.id_materia,m.calificacion_alfa,c.id_curso
    FROM materia_por_curso mpc, materias m, cursos c
    WHERE mpc.id_curso=c.id_curso AND m.id_materia=mpc.id_materias
    AND c.id_curso=`@idcurso`;
  end if;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_CURSO_NIVELACION` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_CURSO_NIVELACION` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_CURSO_NIVELACION`(
	 IN `@idlectivo` INT(11),
	 IN `@idestatus` INT(11)
    )
BEGIN
	if (`@idlectivo` > 0) then
		select `id`,`idperiodolectivo`,`descripcion`,`fechainicio`,`fechafin`,`fechacreacion`,`idusuarioc`,`idestatus`,
		(select concat(`anio_desde`,' - ',`anio_hasta`) from `lectivo` where `id_lectivo`=`idperiodolectivo`) as periodolectivo
		from `tblcursonivel`
		where `idperiodolectivo`=`@idlectivo` and `idestatus`=`@idestatus`;
	else
		SELECT `id`,`idperiodolectivo`,`descripcion`,`fechainicio`,`fechafin`,`fechacreacion`,`idusuarioc`,`idestatus`,
		(SELECT CONCAT(`anio_desde`,' - ',`anio_hasta`) FROM `lectivo` WHERE `id_lectivo`=`idperiodolectivo`) AS periodolectivo
		FROM `tblcursonivel`
		where `idestatus`=`@idestatus`;
	end if;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_ESTUDIANTES` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_ESTUDIANTES` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_ESTUDIANTES`(
  IN `@idestudiante` INT,
  IN `@idestatus` INT
    )
BEGIN
  IF `@idestudiante` > 0 then
    SELECT id_alumno,apellidos,nombres,id_curso,(SELECT descripcion FROM cursos WHERE id_curso=a.id_curso LIMIT 1) AS nomcurso,
    direccion,telefono,celular,email,nompadre,tel_representante,
    cedula,estado,fec_nac_est,fec_matric,nacion,pais,id_provincia,id_canton,id_parroquia,sexo,direc_padre,
    direc_padre,ced_padre,fono2_padre,ocupacion_padre,traba_padre,nacionalidad_padre,nom_madre,direc_madre,
    ced_mad,fon_mad,fon2_mad,ocup_madre,traba_madre,nacionalidad_madre,tutor,direccion_tutor,ced_tutor,
    fon_tutor,fono2_tutor,ocup_tutor,trab_tutor,nacionalidad_tutor,proviene_tutor,foto
    FROM alumnos a  WHERE estado=`@idestatus` and id_alumno=`@idestudiante` ORDER BY id_curso ASC, apellidos ASC;
  else
    SELECT id_alumno,apellidos,nombres,id_curso,(SELECT descripcion FROM cursos WHERE id_curso=a.id_curso LIMIT 1) AS nomcurso,
    direccion,telefono,celular,email,nompadre,tel_representante,
    cedula,estado,fec_nac_est,fec_matric,nacion,pais,id_provincia,id_canton,id_parroquia,sexo,direc_padre,
    direc_padre,ced_padre,fono2_padre,ocupacion_padre,traba_padre,nacionalidad_padre,nom_madre,direc_madre,
    ced_mad,fon_mad,fon2_mad,ocup_madre,traba_madre,nacionalidad_madre,tutor,direccion_tutor,ced_tutor,
    fon_tutor,fono2_tutor,ocup_tutor,trab_tutor,nacionalidad_tutor,proviene_tutor,foto
    FROM alumnos a  WHERE estado=`@idestatus` order by id_curso asc, apellidos asc;
  end if;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_ESTUDIANTES_CURSO` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_ESTUDIANTES_CURSO` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_ESTUDIANTES_CURSO`(
  IN `@idcurso` INT,
  IN `@idestatus` INT
    )
BEGIN
  IF `@idcurso` > 0 THEN
    SELECT id_alumno,apellidos,nombres,id_curso,(SELECT descripcion FROM cursos WHERE id_curso=a.id_curso LIMIT 1) AS nomcurso,
    direccion,telefono,celular,email,nompadre,tel_representante,
    cedula,estado,fec_nac_est,fec_matric,nacion,pais,id_provincia,id_canton,id_parroquia,sexo,direc_padre,
    direc_padre,ced_padre,fono2_padre,ocupacion_padre,traba_padre,nacionalidad_padre,nom_madre,direc_madre,
    ced_mad,fon_mad,fon2_mad,ocup_madre,traba_madre,nacionalidad_madre,tutor,direccion_tutor,ced_tutor,
    fon_tutor,fono2_tutor,ocup_tutor,trab_tutor,nacionalidad_tutor,proviene_tutor,foto
    FROM alumnos a  WHERE estado=`@idestatus` AND a.id_curso=`@idcurso` ORDER BY apellidos ASC;
  END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_ESTUDIANTE_CED` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_ESTUDIANTE_CED` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_ESTUDIANTE_CED`(
  IN `@cedulaest` varchar(10),
  IN `@idestatus` INT
    )
BEGIN
  IF `@cedulaest` > 0 THEN
    SELECT id_alumno,apellidos,nombres,id_curso,(SELECT descripcion FROM cursos WHERE id_curso=a.id_curso LIMIT 1) AS nomcurso,
    direccion,telefono,celular,email,nompadre,tel_representante,
    cedula,estado,fec_nac_est,fec_matric,nacion,pais,id_provincia,id_canton,id_parroquia,sexo,direc_padre,
    direc_padre,ced_padre,fono2_padre,ocupacion_padre,traba_padre,nacionalidad_padre,nom_madre,direc_madre,
    ced_mad,fon_mad,fon2_mad,ocup_madre,traba_madre,nacionalidad_madre,tutor,direccion_tutor,ced_tutor,
    fon_tutor,fono2_tutor,ocup_tutor,trab_tutor,nacionalidad_tutor,proviene_tutor,foto
    FROM alumnos a  WHERE estado=`@idestatus` AND cedula=`@cedulaest` ORDER BY id_curso ASC, apellidos ASC;
  ELSE
    SELECT id_alumno,apellidos,nombres,id_curso,(SELECT descripcion FROM cursos WHERE id_curso=a.id_curso LIMIT 1) AS nomcurso,
    direccion,telefono,celular,email,nompadre,tel_representante,
    cedula,estado,fec_nac_est,fec_matric,nacion,pais,id_provincia,id_canton,id_parroquia,sexo,direc_padre,
    direc_padre,ced_padre,fono2_padre,ocupacion_padre,traba_padre,nacionalidad_padre,nom_madre,direc_madre,
    ced_mad,fon_mad,fon2_mad,ocup_madre,traba_madre,nacionalidad_madre,tutor,direccion_tutor,ced_tutor,
    fon_tutor,fono2_tutor,ocup_tutor,trab_tutor,nacionalidad_tutor,proviene_tutor,foto
    FROM alumnos a  WHERE estado=`@idestatus` ORDER BY id_curso ASC, apellidos ASC;
  END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_MATERIAS_CURSO_ID` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_MATERIAS_CURSO_ID` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_MATERIAS_CURSO_ID`(
  IN `@idmateriacurso` INT
    )
BEGIN
  SELECT mpc.id_materia_curso,m.descripcion,m.id_materia,m.calificacion_alfa,c.id_curso
  FROM materia_por_curso mpc, materias m, cursos c
  WHERE mpc.id_curso=c.id_curso AND m.id_materia=mpc.id_materias
  AND mpc.id_materia_curso=`@idmateriacurso`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_MATERIAS_DOCENTE` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_MATERIAS_DOCENTE` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_MATERIAS_DOCENTE`(
  IN `@idusuario` INT
    )
BEGIN
  select m.descripcion from materias m, materia_por_curso mpc
  where mpc.id_materias= m.id_materia and mpc.id_usuario=`@idusuario`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_MATERIA_CURSO_PROFESOR` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_MATERIA_CURSO_PROFESOR` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_MATERIA_CURSO_PROFESOR`(
  IN `@idprofesor` INT
  
    )
BEGIN
  SELECT m.descripcion AS materia,cu.descripcion AS curso,mpc.id_materias,mpc.id_curso,mpc.id_materia_curso
  FROM contactos c, materias m, materia_por_curso mpc, cursos cu
  WHERE c.id_materia=m.id_materia AND m.id_materia=mpc.id_materias 
  AND c.id_usuario=`@idprofesor`  AND cu.id_curso=mpc.id_curso;
   END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_MENUCLASE` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_MENUCLASE` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_MENUCLASE`(
  IN `@idmenu` INT,
  IN `@idestatus` INT
    )
BEGIN
  SELECT id,idmenu,clase
  FROM tblmenuclass
  WHERE idmenu=`@idmenu` and estatus=`@idestatus`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_MENULATERAL` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_MENULATERAL` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_MENULATERAL`(
  
  IN `@idestatus` INT
    )
BEGIN
  SELECT idmenulateral,idmenu,descripcion,enlace,especial,orden,imagen,get2,accion,estatus
  FROM tblmenulateral
  WHERE estatus=`@idestatus` order by orden;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_MENUSUPERIOR` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_MENUSUPERIOR` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_MENUSUPERIOR`(
  IN `@idestatus` int
    )
BEGIN
  select idmenu,descripcion,enlace,especial,orden,imagen,permiso,accion,estatus
  from tblmenu
  where estatus=`@idestatus`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_ESTUDIANTE_NOTA` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_ESTUDIANTE_NOTA` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_ESTUDIANTE_NOTA`(
  IN `@idestudiante` INT)
BEGIN
  SELECT (SELECT nota FROM notas n WHERE n.id_alumno=`@idestudiante` AND id_parametro=1 AND id_parcial=1 ) AS q1p1,
  (SELECT nota FROM notas n WHERE n.id_alumno=`@idestudiante` AND id_parametro=1 AND id_parcial=2 ) AS q1p2,
  (SELECT nota FROM notas n WHERE n.id_alumno=`@idestudiante` AND id_parametro=1 AND id_parcial=3 ) AS q1p3,
  (SELECT nota FROM notas n WHERE n.id_alumno=`@idestudiante` AND id_parametro=1 AND id_parcial=4 ) AS q1p4,
  (SELECT nota FROM notas n WHERE n.id_alumno=`@idestudiante` AND id_parametro=2 AND id_parcial=1 ) AS q2p1,
  (SELECT nota FROM notas n WHERE n.id_alumno=`@idestudiante` AND id_parametro=2 AND id_parcial=2 ) AS q2p2,
  (SELECT nota FROM notas n WHERE n.id_alumno=`@idestudiante` AND id_parametro=2 AND id_parcial=3 ) AS q2p3,
  (SELECT nota FROM notas n WHERE n.id_alumno=`@idestudiante` AND id_parametro=2 AND id_parcial=4 ) AS q2p4;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_NOTA_VENTA` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_NOTA_VENTA` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_NOTA_VENTA`(
  IN `@nnota` INT(11)
    )
BEGIN
    
  if (`@nnota` > 0) then
    select `id`,`nnotaventa`,`idcliente`,`cedulaest`,`razonsocial`,`telefono`,`direccion`,`iva`,`subtotal`,`valortotal`,
    `fechacreacion`,`idusuarioc`,`idestatus`,
    (select concat(apellidos,' ',nombres)as estudiantes from `alumnos` where `cedula`= `cedulaest` ) as estudiante,
    (SELECT `usuario` FROM `tblusuarios` WHERE `id_usuario`=`idusuarioc` ) AS nusuarioc
    from tblnotaventa
    where nnotaventa= `@nnota`; 
  else
    SELECT `id`,`nnotaventa`,`idcliente`,`cedulaest`,`razonsocial`,`telefono`,`direccion`,`iva`,`subtotal`,`valortotal`,
    `fechacreacion`,`idusuarioc`,`idestatus`,
    (SELECT CONCAT(apellidos,' ',nombres)AS estudiantes FROM `alumnos` WHERE `cedula`= `cedulaest` ) AS estudiante,
    (SELECT `usuario` FROM `tblusuarios` WHERE `id_usuario`=`idusuarioc` ) AS nusuarioc
    FROM tblnotaventa;
  end if;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_PAGOS` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_PAGOS` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_PAGOS`(
  IN `@npago` INT(11),
  IN `@cedulaest` VARCHAR(10),
  IN `@rubro` INT(11),
  IN `@idestatus` INT(11)
    )
BEGIN
  IF (`@npago` > 0) THEN
    SELECT `id`,`tipodoc`,`ndocumento`,`iddocumento`,`valorpago`,
    (SELECT `descripcion` FROM `tbltipopago` WHERE id=`tipopago` ) AS ntipopago,
    (SELECT `descripcion` FROM `tblformapago` WHERE id=`formapago` ) AS nformapago,
    (SELECT `descripcion` FROM `tblrubrosniv` WHERE id=`idrubro` ) AS nrubro,
    (SELECT CONCAT(apellidos,' ',nombres)AS estudiantes FROM `alumnos` WHERE `cedula`= `cedulaest` ) AS estudiante,
    `fechaingreso`,`contable`,`idestatus`,
    (SELECT `usuario` FROM `tblusuarios` WHERE `id_usuario`=`idusuarioc` ) AS nusuarioc
    FROM `tblpagos` order by fechaingreso desc;
  ELSE
    SELECT `id`,`tipodoc`,`ndocumento`,`iddocumento`,`valorpago`,
    (SELECT `descripcion` FROM `tbltipopago` WHERE id=`tipopago` ) AS ntipopago,
    (SELECT `descripcion` FROM `tblformapago` WHERE id=`formapago` ) AS nformapago,
    (SELECT `descripcion` FROM `tblrubrosniv` WHERE id=`idrubro` ) AS nrubro,
    (SELECT CONCAT(apellidos,' ',nombres)AS estudiantes FROM `alumnos` WHERE `cedula`= `cedulaest` ) AS estudiante,
    `fechaingreso`,`contable`,`idestatus`,
    (SELECT `usuario` FROM `tblusuarios` WHERE `id_usuario`=`idusuarioc` ) AS nusuarioc
    FROM `tblpagos` ORDER BY fechaingreso desc;
  END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_ROLES` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_ROLES` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_ROLES`(
  IN `@estatus` INT
)
BEGIN
    IF `@estatus`=0 THEN
      SELECT id_rol,descripcion,superadmin,admin,mantenimientos,usuarios,ajustes,reportes,system FROM tbl_rol;
    ELSE
      SELECT id_rol,descripcion,superadmin,admin,mantenimientos,usuarios,ajustes,reportes,system FROM tbl_rol WHERE id_estatus=`@estatus`;
    END IF;
  END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_RUBROSNIV` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_RUBROSNIV` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_RUBROSNIV`(
  IN `@idnivelacion` INT(11),
  IN `@idestatus` INT(2)
    )
BEGIN
  if (`@idnivelacion` > 0) then
    select `id`,`descripcion`,`idnivelacion`,`valor`,`idusuarioc`,`fechacreacion`,`tiporubro`,`fechamaxpago`,`idestatus`
    from `tblrubrosniv`
    where `idnivelacion`=`@idnivelacion` and `idestatus`=`@idestatus`;
  else
    SELECT `id`,`descripcion`,`idnivelacion`,`valor`,`idusuarioc`,`fechacreacion`,`tiporubro`,`fechamaxpago`,`idestatus`
    FROM `tblrubrosniv`
    WHERE `idestatus`=`@idestatus`;
  END IF;
  
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_SL_USUARIO_DOCENTES` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_SL_USUARIO_DOCENTES` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_SL_USUARIO_DOCENTES`(
  IN `@iddocente` INT,
  IN `@estatus` INT
    )
BEGIN
  IF `@estatus`=3 THEN
    SELECT u.id_usuario,u.usuario,ud.nombres, ud.apellidos, ud.correo, u.id_rol, r.descripcion
    FROM tblusuarios u, tblusuariodetalle ud , tbl_rol r
    WHERE  ud.id_usuario=u.id_usuario  AND u.id_rol=r.id_rol and u.id_rol=5;
  ELSE
    SELECT u.id_usuario,u.usuario,ud.nombres, ud.apellidos, ud.correo, u.id_rol, r.descripcion,c.telefono,c.celular,c.email,c.direccion 
    FROM tblusuarios u, tblusuariodetalle ud, tbl_rol r, contactos c 
    WHERE  ud.id_usuario=u.id_usuario AND u.id_rol=r.id_rol AND estatus=`@estatus`  AND u.id_rol=5 and c.id_usuario=u.id_usuario
    ORDER BY u.usuario ASC;
  END IF;
END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_UP_CURSOS` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_UP_CURSOS` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_UP_CURSOS`(
  IN `@idcurso` INT,
  IN `@ncurso` text,
  IN `@ncalificacion` INT
)
BEGIN
  update cursos 
  set descripcion=`@ncurso`,cal_alfa=`@ncalificacion`
  where id_curso=`@idcurso`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_UP_USDET` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_UP_USDET` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_UP_USDET`(
  IN `@id` INT,
  IN `@password` VARCHAR(35),
  IN `@cedula` VARCHAR(35), 
  IN `@nombres` TEXT,
  IN `@apellidos` TEXT,
  IN `@correo` VARCHAR(80),
  IN `@fechanac` DATE,    
  IN `@avatar` VARCHAR(300)
)
BEGIN
    IF `@password`<>"" THEN
      UPDATE tblusuarios SET PASSWORD=MD5(`@password`) WHERE id_usuario=`@id`;
      UPDATE tblusuariodetalle SET cedula=`@cedula`,nombres=`@nombres`,apellidos=`@apellidos`,
      correo=`@correo`,fechanacimiento=`@fechanac`, avatar=`@avatar`
      WHERE id_usuario=`@id`;
    ELSE
      UPDATE tblusuariodetalle SET cedula=`@cedula`,nombres=`@nombres`,apellidos=`@apellidos`,
      correo=`@correo`,fechanacimiento=`@fechanac`, avatar=`@avatar`
      WHERE id_usuario=`@id`;
    END IF;
  END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_UP_USUARIOS` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_UP_USUARIOS` */;

DELIMITER $$

/*!50003 CREATE  PROCEDURE `SP_UP_USUARIOS`(
  IN `@id` INT,
  IN `@usuario` VARCHAR(20),
  IN `@password` VARCHAR(35), 
  IN `@id_rol` INT,
  IN `@id_estatus` INT
)
BEGIN
    IF `@id_estatus`<>3 THEN
      UPDATE tblusuarios SET usuario=`@usuario`,PASSWORD=MD5(`@password`),id_rol=`@id_rol` WHERE id_usuario=`@id`;
    ELSE
      UPDATE tblusuarios SET estatus=`@id_estatus` WHERE id_usuario=`@id`;
    END IF;
  END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
