var url=window.location.protocol + "//" + window.location.host+"/";

function saltarusuario(e){
  tecla = (document.all) ? e.keyCode : e.which; 
  if (tecla==13) document.getElementById('password').focus();
}

function saltarpass(e){
  tecla = (document.all) ? e.keyCode : e.which; 
  if (tecla==13) validLogin();
}

function ir_agregar_empresa()
{
	window.location=url+'principal.php?admin=emp-agr';
}

function ir_agregar_usuario()
{
	window.location=url+'principal.php?admin=usu-agr';
}

function ir_agregar_recurso()
{
	window.location=url+'principal.php?admin=rec-agr';
}

function ir_agregar_proyecto()
{
	window.location=url+'principal.php?admin=proy-agr';
}

function ir_agregar_alarma()
{
	window.location=url+'principal.php?admin=alarm-agr';
}

function actualizar_vmateria()
{
	window.location=url+'principal.php?admin=gest-mate';
}

function actualizar_vestudiante()
{
	window.location=url+'principal.php?admin=gest-estu';
}

function actualizar_vcursos()
{
	window.location=url+'principal.php?admin=gest-curs';
}
function ir_editar_proyecto(id)
{
	window.location=url+'principal.php?admin=proy-edi&id='+id;
}

function ir_agregar_tarea(id)
{
	window.location=url+'principal.php?admin=tar-agr&id='+id;
}

function ir_inter_tarea(id)
{
	window.location=url+'principal.php?admin=tar-int&id='+id;
}

function ir_gantt_proyecto(id,av)
{
	window.location=url+'principal.php?admin=est-proy&id='+id+'&av='+av;
}

function buscar_proyecto()
{
	document.getElementById('dvbusqueda').style.display= '';
}

function buscar_tarea()
{
	document.getElementById('dvbusqueda').style.display= '';
}

function agregar_tarea()
{
	document.getElementById('dvagtarea').style.display= '';
}

function agregar_usuario()
{
	if (validaciones_usuario()==1)
	{
		guardar_usuario()
		//alert('Se ha ingresado el usuario con éxito');
		
	}
}

function guardar_usuario()
{
	 var usuario=$('#txtusuario').val();
	 var password=$('#txtpass').val();
	 var roles=$('#sltroles').val();
	 var cedula=$('#txtcedula').val();
	 var nombres=$('#txtnombres').val();
	 var apellidos=$('#txtapellidos').val();
	 var correo=$('#txtcorreo').val();
	 var fecha=$('#txtfecha').val();
	 var empresa=$('#sltempresa').val();
	       
	       var dataString = 'txtusuario='+ usuario+'&txtpass='+ password+ '&sltroles='+roles + '&txtcedula='+cedula+'&txtnombres='+nombres+'&txtapellidos='+apellidos+'&txtcorreo='+correo+'&date1='+fecha+'&sltempresa='+empresa;
	       $("#errorMessage").show();
	       $("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
	       $.ajax({
	             type: "POST",
	             url: "mantenimientos/usuarios/insertarusuario.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             $("#registrado").hide();
	             //$("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //$("#error").html('');
	                  $("#error").hide();
	                  $("#registrado").html('<div style="background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">El Usuario se ha ingresado con éxito</span>');
	                  $("#registrado").show();
	                  $("#registrado").fadeOut(3000);
	                  $("#errorMessage").hide();
	                  limpiar_campos_usuario();
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  $("#errorMessage").hide();
	             	  $("#error").show();
	                  $("#error").html('Hubo un error al insertar el usuario.');
	                  $("#error").html(result);
	             }
	        }
	  });
}

function agregar_materia()
{
	 var materia=$('#txtmateria').val();
	 var calificacionalfa=$('#txtcalialfa').val();
	
	       
	       var dataString = 'materia='+ materia+'&calificaciona='+ calificacionalfa;
	       $("#errorMessage").show();
	       $("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
	       $.ajax({
	             type: "POST",
	             url: "mantenimientos/materias/insertarmateria.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             $("#registrado").hide();
	             //$("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //$("#error").html('');
	                  $("#error").hide();
	                  $("#registrado").html('<div style="background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">La Materia se ha ingresado con éxito</span>');
	                  $("#registrado").show();
	                  $("#registrado").fadeOut(3000);
	                  $("#errorMessage").hide();
	                  //limpiar_campos_usuario();
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  $("#errorMessage").hide();
	             	  $("#error").show();
	                  $("#error").html('Hubo un error al insertar el usuario.');
	                  $("#error").html(result);
	             }
	        }
	  });
}


function eliminar_curso(idcurso)
{
	$.messager.confirm('Confirm','¿Desea eliminar el curso seleccionado?',function(r){
    	if (r){
	
			var dataString = 'idcurso='+ idcurso;
			var espere=mensaje('Espere...','procesar','100px','');
		 	var validacion=true;
	       jQuery.ajax({
		             type: "POST",
		             url: "mantenimientos/cursos/eliminarcurso.php",
		             data: dataString,
		             cache: false,
		             success: function(result){
		             var result=result;
		             espere.dialog('close');
		             if(result==1){
		                mensaje('Curso eliminado con éxito.','ok','','');
		                location.reload();
		                
		             }else{
						mensaje('Error al eliminar el curso.','error','','');
             			console.log(result);
		             }
		        }
		  });

		}

	});
}


function agregar_curso()
{
	 var curso=$('#txtcurso').val();
	 var calificacionalfa=$('#txtcalialfa').val();
	 var espere=mensaje('Espere...','procesar','100px','');
	 var validacion=true;

 	if (!curso){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtcurso').focus(); validacion=false;}

 	if (validacion)
	{
	       var dataString = 'curso='+ curso+'&calificaciona='+ calificacionalfa;
	       $.ajax({
	             type: "POST",
	             url: "mantenimientos/cursos/insertarcurso.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             espere.dialog('close');
	             if(result==1){
	             	
             		mensaje('Curso ingresado con éxito.','ok','','');
	             }else{
	             	mensaje('Error al ingresar el curso.','error','','');
             		console.log(result);
	             }
	        }
	  });
 	}
}


function validaciones_usuario()
{
	if (!document.getElementById('txtusuario').value=="")
	{
		if (!document.getElementById('txtpass').value=="")
		{
			if (!document.getElementById('txtconfpass').value=="")
			{
				if (document.getElementById('txtpass').value==document.getElementById('txtconfpass').value)
				{
					return 1;
				}else{
					alert('El Password no coincide');
					document.getElementById('txtpass').focus();
					return 0;
				}		
			}else{
				alert('Debe ingresar la confirmación del password');
				document.getElementById('txtconfpass').focus();
				return 0;
			}		
		}else{
			alert('Debe ingresar el password');
			document.getElementById('txtpass').focus();
			return 0;
		}	
	}else{
		alert('Debe ingresar el nombre de usuario');
		document.getElementById('txtusuario').focus();
		return 0;
	}
	return 0;
}

function limpiar_campos_usuario()
{
	document.getElementById('txtusuario').value="";
	document.getElementById('txtpass').value="";
	document.getElementById('txtconfpass').value="";
	document.getElementById('txtnombres').value="";
	document.getElementById('txtapellidos').value="";
	document.getElementById('txtcedula').value="";
	document.getElementById('txtcorreo').value="";
	document.getElementById('txtfecha').value="";
	//document.getElementById('sltempresa').="";


}

function eliminar_usuario(id)
{
		var usuario=id;
	       
		var dataString = 'idusuario='+ usuario;
		$("#errorMessage2").show();
		$("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		$.ajax({
	             type: "POST",
	             url: "mantenimientos/usuarios/eliminarusuario.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             $("#registrado2").hide();
	             //$("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //$("#error").html('');
	                  $("#error2").hide();
	                  $("#registrado2").html('<div style="background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">El Usuario ha sido eliminado con éxito</span>');
	                  $("#registrado2").show();
	                  $("#registrado2").fadeOut(3000);
	                  $("#errorMessage2").hide();

	                  $("#dvusuario"+id).fadeOut(500);
	                  limpiar_campos_usuario();
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  $("#errorMessage2").hide();
	             	  $("#error2").show();
	                  $("#error2").html('Hubo un error al eliminar el usuario.');
	                  $("#error2").html(result);
	             }
	        }
	  });
}

function actualizar_vusuario()
{
	window.location=url+'principal.php?admin=usu-gest';
}

/********************* Funciones Menu Superior ***************/

function ir_mantenimientos()
{
	window.location=url+'principal.php?admin=mantenimientos';
}

function ir_calificaciones()
{
	window.location=url+'principal.php?admin=calificaciones';
}

function ir_notificaciones()
{
	window.location=url+'principal.php?admin=notificaciones';
}

function ir_reportes()
{
	window.location=url+'principal.php?admin=reportes';
}

function ir_matriculacion()
{
	window.location=url+'principal.php?admin=matriculacion';
}

function ir_facturacion()
{
	window.location=url+'principal.php?admin=facturacion';
}

function ir_documentos()
{
	window.location=url+'principal.php?admin=documentos';
}


function ir_sistema()
{
	window.location=url+'principal.php?admin=sistema';
}

function cerrar_sesion()
{
	window.location=url+'cerrarsesion.php';
}

function ir_a_inicio()
{
	window.location=url+'principal.php';
}

/********************* Fin Funciones Menu Superior ***********/

/********************* Funciones generales *******************/

function isValidDate(day,month,year)
	{
		var dteDate;
		month=month-1;
		dteDate=new Date(year,month,day);
		return ((day==dteDate.getDate()) && (month==dteDate.getMonth()) && (year==dteDate.getFullYear()));
	}
 
	function validate_fecha(fecha)
	{
		//var patron=new RegExp("^([0-9]{1,2})([/])([0-9]{1,2})([/])(19|20)+([0-9]{2})$");
 		  var patron=new RegExp("^(19|20)+([0-9]{2})([/])([0-9]{1,2})([/])([0-9]{1,2})$");
		if(fecha.search(patron)==0)
		{
			var values=fecha.split("/");
			if(isValidDate(values[2],values[1],values[0]))
			{
				return true;
			}
		}
		return false;
	}
 
    function calcularDias(fechainicial,fechafinal)
    {
		var fechaInicial=fechainicial;
		var fechaFinal=fechafinal;
		var resultado="";
		if(validate_fecha(fechaInicial) && validate_fecha(fechaFinal))
		{
			inicial=fechaInicial.split("/");
			final=fechaFinal.split("/");
			// obtenemos las fechas en milisegundos
			var dateStart=new Date(inicial[0],(inicial[1]-1),inicial[2]);
            var dateEnd=new Date(final[0],(final[1]-1),final[2]);
            if(dateStart<dateEnd)
            {
				resultado=""+(((dateEnd-dateStart)/86400)/1000)+" días";
			}else{
				resultado="La fecha inicial es posterior a la fecha final";
			}
		}else{
			if(!validate_fecha(fechaInicial))
				resultado="La fecha inicial es incorrecta";
			if(!validate_fecha(fechaFinal))
				resultado="La fecha final es incorrecta";
		}
		//document.getElementById("resultado").innerHTML=resultado;
		//alert(resultado);
		document.getElementById("txttiempoasig").value=resultado;
    }

function diferenciafechas(fa,fb){  //fa y fb dos fechas
	fa=fa.replace("-","/");
	fa=fa.replace("-","/");
	fb=fb.replace("-","/");
	fb=fb.replace("-","/");

	fa=fa.substring(6,10)+'/'+fa.substring(3,5)+'/'+fa.substring(0,2);
	fb=fb.substring(6,10)+'/'+fb.substring(3,5)+'/'+fb.substring(0,2);

	fa= new Date(fa);
	fb = new Date(fb);
	//alert(fa);
	var totdias = fa-fb;
	//alert(fb);
	totdias /=3600000;  
	totdias /=24;	
	totdias= Math.floor(totdias);
	totdias= Math.abs(totdias);

	var ans, meses, dias, m2, m1, d3, d2, d1;
	var f2=new Date(fb);
	var f1=new Date(fa);

	

	if (fa > fb){f2=new Date(fa);f1=new Date(fb);}else{var f2=new Date(fb); f1=new Date(fa);}  //Siempre f2 > f1
	

	ans=f2.getFullYear()-f1.getFullYear(); // dif de años inicial
	m2=f2.getMonth();
	m1=f1.getMonth();
	meses=m2-m1;	if (meses<0){meses +=12; --ans; }

	d2=f2.getDate();
	d1=f1.getDate();
	dias=d2-d1;

	var f3=new Date(f2.getFullYear(),m2,1);
	f3.setDate(f3.getDate()-1);
	d3=f3.getDate();

	if (d1>d2) {
		dias +=d3; --meses; if (meses<0){meses +=12; --ans; }
		if (fa>fb){  //corrección por febrero y meses de 30 días
			f3=new Date(f1.getFullYear(),m1+1,1);
			f3.setDate(f3.getDate()-1);
			d3=f3.getDate();
			if (d3==30) dias -=1;
			if (d3==29) dias -=2;
			if (d3==28) dias -=3;
		}
	}

	document.getElementById("txttiempoasigc").value='Años: '+ans+' Meses: '+meses+' dias: '+dias;
	//alert(dias);
	//return {ans:ans,meses:meses,dias:dias,Tdias:totdias};
}

/******************* Fin Funciones generales *****************/

function eliminar_proyecto(idproyecto)
{
	if (confirm("¿Desea Eliminar este proyecto?"))
	{
	 	//var idproyecto=jQuery('#idproyecto').val();
		var dataString = 'idproyecto='+ idproyecto;
		

		jQuery("#errorMessage").show();
	       jQuery("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
	       jQuery.ajax({
	             type: "POST",
	             url: "proyectos/eliminarproyecto.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //$("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //$("#error").html('');
	                  //jQuery("#error").hide();
	                  //jQuery("#registrado").html('<div style="background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">El proyecto se ha eliminado con éxito</span>');
	                  //jQuery("#registrado").show();
	                  //jQuery("#registrado").fadeOut(3000);
	                  //jQuery("#errorMessage").hide();
	                  //limpiar_campos_actividad();
	                  location.reload();
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage").hide();
	             	  jQuery("#error").show();
	                  jQuery("#error").html('Hubo un error al Eliminar el proyecto.');
	                  jQuery("#error").html(result);
	             }
	        }
	  });
	}
}

function limpiar_campos_actividad()
{
	
	document.getElementById('chktterminada').checked=false;
	document.getElementById('txtnactividad').value="";
	document.getElementById('txtdescripcion').value="";
	document.getElementById('txtavance').value="";

}

function editar_actividad(idactividad)
{
	alert('Opción no disponible');
}

function eliminar_actividad(idactividad)
{
	if (confirm("¿Desea Eliminar esta actividad?"))
	{
		var idtarea=  $('#idtarea').val();
	 	var idproyecto=$('#idproyecto').val();
		var dataString = 'idtarea='+ idtarea+'&idproyecto='+ idproyecto;
		var dataString = dataString + '&idactividad='+idactividad;

		$("#errorMessage2").show();
	       $("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
	       $.ajax({
	             type: "POST",
	             url: "proyectos/tareas/interactuar/eliminaractividades.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             $("#registrado2").hide();
	             //$("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //$("#error").html('');
	                  $("#error2").hide();
	                  $("#registrado2").html('<div style="background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">La Actividad se ha eliminado con éxito</span>');
	                  $("#registrado2").show();
	                  $("#registrado2").fadeOut(3000);
	                  $("#errorMessage2").hide();
	                  limpiar_campos_actividad();
	                  location.reload();
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  $("#errorMessage2").hide();
	             	  $("#error2").show();
	                  $("#error2").html('Hubo un error al Eliminar la actividad.');
	                  $("#error2").html(result);
	             }
	        }
	  });
	}
}

function crear_actividad()
{
	 var nactividad=$('#txtnactividad').val();
	 var descripcion=$('#txtdescripcion').val();
	 var imagen=$('#txtarchivo').val();
	 var avance=$('#txtavance').val();
	 var tterminada=document.getElementById('chktterminada').checked;
	 var idtarea=  $('#idtarea').val();
	 var idproyecto=$('#idproyecto').val();
	       
	       var dataString = 'nactividad='+ nactividad+'&descripcion='+ descripcion;
	       var dataString = dataString + '&imagen='+imagen + '&tterminada='+tterminada;
	       var dataString = dataString +'&idtarea='+idtarea+'&idproyecto='+idproyecto+'&avance='+avance;
	       $("#errorMessage").show();
	       $("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
	       $.ajax({
	             type: "POST",
	             url: "proyectos/tareas/interactuar/insertaractividades.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             $("#registrado").hide();
	             //$("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //$("#error").html('');
	                  $("#error").hide();
	                  $("#registrado").html('<div style="background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">La Actividad se ha ingresado con éxito</span>');
	                  $("#registrado").show();
	                  $("#registrado").fadeOut(3000);
	                  $("#errorMessage").hide();
	                  limpiar_campos_actividad();
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  $("#errorMessage").hide();
	             	  $("#error").show();
	                  $("#error").html('Hubo un error al insertar la actividad.');
	                  $("#error").html(result);
	             }
	        }
	  });
}

function actualizar_actividad(idactividad)
{
	 var nactividad=$('#txtnactividad'+idactividad).val();
	 var descripcion=$('#txtdescripcion'+idactividad).val();
	 var imagen=$('#txtarchivo'+idactividad).val();
	 var avance=$('#txtavance'+idactividad).val();
	 var tterminada=document.getElementById('chktterminada'+idactividad).checked;
	 var idtarea=  $('#idtarea').val();
	 var idproyecto=$('#idproyecto').val();
	       
	       var dataString = 'nactividad='+ nactividad+'&descripcion='+ descripcion+'&idactividad='+ idactividad;
	       var dataString = dataString + '&imagen='+imagen + '&tterminada='+tterminada;
	       var dataString = dataString +'&idtarea='+idtarea+'&idproyecto='+idproyecto+'&avance='+avance;
	       $("#errorMessage"+idactividad).show();
	       $("#errorMessage"+idactividad).fadeIn(400).html('<img src="images/loading.gif" />');
	       $.ajax({
	             type: "POST",
	             url: "proyectos/tareas/interactuar/actualizaractividades.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             $("#registrado"+idactividad).hide();
	             //$("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //$("#error").html('');
	                  $("#error"+idactividad).hide();
	                  $("#registrado"+idactividad).html('<div style="background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">La Actividad se ha ingresado con éxito</span>');
	                  $("#registrado"+idactividad).show();
	                  $("#registrado"+idactividad).fadeOut(3000);
	                  $("#errorMessage"+idactividad).hide();

	                  //location.reload();
	                  //limpiar_campos_actividad();
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  $("#errorMessage"+idactividad).hide();
	             	  $("#error"+idactividad).show();
	                  $("#error"+idactividad).html('Hubo un error al insertar la actividad.');
	                  $("#error"+idactividad).html(result);
	             }
	        }
	  });
}

/******************* Fin Funciones generales *****************/

/******************* Fin Funciones generales *****************/

function cambio_avance()
{
	//alert(document.getElementById('chktterminada').checked);
	if (document.getElementById('chktterminada').checked==false)
	{
		document.getElementById('txtavance').value="50";
		document.getElementById('txtavance').disabled=false;
	}
	else
	{
		document.getElementById('txtavance').value="100";
		document.getElementById('txtavance').disabled=true;
	}
}

function actualizar_vproyecto()
{
	window.location=url+'principal.php?admin=proy-gest';
}

function actualizar_vtareas(id)
{
	window.location=url+'principal.php?admin=tar-int&id='+id;
}

function crear_proyecto()
{
	
	if (validar_campos_proyecto()==1)
	{
		//alert(jQuery('#sltasig1').val());
		if (crear_proyecto_guardar()==1)
		{
			limpiar_campos_proyecto();
			//alert('Proyecto y tareas Generadas con éxito');
		}
	}else{


	}
	actualizar_vtareas

}


function subir_actualizar(idactividad)
{
	  var formData = new FormData(jQuery(".formulario"+idactividad)[0]);
        var message = ""; 
        //hacemos la petición ajax
          var file = jQuery("#imagen"+idactividad)[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo
        showMessage2("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
  
        jQuery.ajax({
            url: 'proyectos/tareas/interactuar/upload.php',  
            type: 'POST',
            // Form data
            //datos del formulario
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,
            //mientras enviamos el archivo
            beforeSend: function(){
                message = jQuery("<span class='before'>Subiendo la imagen, por favor espere...</span>");
                showMessage2(message)        
            },
            //una vez finalizado correctamente
            success: function(data){
                message = jQuery("<span class='success'>La imagen ha subido correctamente.</span>");
                showMessage2(message);
                document.getElementById('txtarchivo'+idactividad).value=data;
                //if(isImage2(fileExtension))
                //{
                    //jQuery(".showImage").html("<img src='proyectos/tareas/interactuar/files/"+data+"' />");
                    // jQuery(location).attr('href',"proyectos/tareas/interactuar/files/"+data,'_blank');    

                    window.open("proyectos/tareas/interactuar/files/"+data,'_blank');
                //}
            },
            //si ha ocurrido un error
            error: function(){
                message = jQuery("<span class='error'>Ha ocurrido un error.</span>");
                showMessage2(message);
            }
        });
}
 function showMessage2(message){
    jQuery(".messages").html("").show();
    jQuery(".messages").html(message);
	}

function isImage2(extension)
{
    switch(extension.toLowerCase()) 
    {
        case 'jpg': case 'gif': case 'png': case 'jpeg': case 'pdf':
            return true;
        break;
        default:
            return false;
        break;
    }
}


jQuery(document).ready(function(){
 
    jQuery(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    jQuery('#imagen').change(function()
    {
        //obtenemos un array con los datos del archivo
        var file = jQuery("#imagen")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo
        showMessage("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
    });
 
    function showMessage(message){
    jQuery(".messages").html("").show();
    jQuery(".messages").html(message);
	}

	function isImage(extension)
{
    switch(extension.toLowerCase()) 
    {
        case 'jpg': case 'gif': case 'png': case 'jpeg': case 'pdf':
            return true;
        break;
        default:
            return false;
        break;
    }
}


    //al enviar el formulario
    jQuery('#subir').click(function(){
        //información del formulario
        var formData = new FormData(jQuery(".formulario")[0]);
        var message = ""; 
        //hacemos la petición ajax  
        jQuery.ajax({
            url: 'proyectos/tareas/interactuar/upload.php',  
            type: 'POST',
            // Form data
            //datos del formulario
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,
            //mientras enviamos el archivo
            beforeSend: function(){
                message = jQuery("<span class='before'>Subiendo la imagen, por favor espere...</span>");
                showMessage(message)        
            },
            //una vez finalizado correctamente
            success: function(data){
                message = jQuery("<span class='success'>La imagen ha subido correctamente.</span>");
                showMessage(message);
                document.getElementById('txtarchivo').value=data;
                //if(isImage(fileExtension))
                //{
                    //jQuery(".showImage").html("<img src='proyectos/tareas/interactuar/files/"+data+"' />");
                    // jQuery(location).attr('href',"proyectos/tareas/interactuar/files/"+data,'_blank');    

                    window.open("proyectos/tareas/interactuar/files/"+data,'_blank');
                //}
            },
            //si ha ocurrido un error
            error: function(){
                message = jQuery("<span class='error'>Ha ocurrido un error.</span>");
                showMessage(message);
            }
        });
    });
})



function crear_mensaje()
{
	alert('Mensaje Enviado');
}
function validar_campos_proyecto()
{
	if (!document.getElementById('txtproyecto').value=="")
	{
		if (!document.getElementById('txtdescproyecto').value=="")
		{
			return 1;
		}else{
			alert('Debe llenar la descripcion del Proyecto');
			document.getElementById('txtdescproyecto').focus();	
			return 0;
		}
	}else{
		alert('Debe llenar el nombre del Proyecto');		
		document.getElementById('txtproyecto').focus();
		return 0;
	}
}

function crear_proyecto_guardar()
{

		var valfila=parseInt(jQuery("#txtval").val());

		var proyecto=jQuery('#txtproyecto').val(); var descriproy=jQuery('#txtdescproyecto').val();
		var fechainiciop=document.getElementsByName('txtfechaini')[0].value;
		fechainiciop=fechainiciop.substring(6,10)+'-'+fechainiciop.substring(3,5)+'-'+fechainiciop.substring(0,2);
		var fechafinp=document.getElementsByName('txtfechafin')[0].value;
		fechafinp=fechafinp.substring(6,10)+'-'+fechafinp.substring(3,5)+'-'+fechafinp.substring(0,2);

		var dataString = 'proy='+ proyecto+'&descproy='+descriproy+'&feciniproy='+fechainiciop+'&fecfinproy='+fechafinp;

		for (i=1; i<=valfila; i++)
		{

			var fechai=document.getElementsByName('txtfechatarini'+i)[0].value;
			fechai=fechai.substring(6,10)+'-'+fechai.substring(3,5)+'-'+fechai.substring(0,2);
			var fechaf=document.getElementsByName('txtfechatarfin'+i)[0].value;
			fechaf=fechaf.substring(6,10)+'-'+fechaf.substring(3,5)+'-'+fechaf.substring(0,2);

			dataString = dataString + '&tar'+i+'='+ jQuery('#txttarea'+i).val() + '&desctar'+i+'=' + jQuery('#txtdesctarea'+i).val();
			dataString = dataString +'&asigtar'+i+'='+ jQuery('#sltasig'+i).val()+'&finitar'+i+'='+ fechai;
			dataString = dataString + '&ffintar'+i+'='+ fechaf;
			
		}


		//alert(dataString);
	
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "proyectos/crear/insertarproyectos.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado2").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	                  jQuery("#error2").hide();
	                  jQuery("#registrado2").html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">El Proyecto ha sido creado con éxito</span>');
	                  jQuery("#registrado2").show();
	                  jQuery("#registrado2").fadeOut(3000);
	                  jQuery("#errorMessage2").hide();

	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_usuario();
	                  limpiar_campos_proyecto();
	                  return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al eliminar el usuario.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
}

function limpiar_campos_proyecto()
{
	var valfila=parseInt(jQuery("#txtval").val());
	document.getElementById('txtproyecto').value="";
	document.getElementById('txtdescproyecto').value="";

	for (i=1; i<=valfila; i++)
	{

	document.getElementById('txttarea'+i).value="";
	document.getElementById('txtdesctarea'+i).value="";

	}

	for (i=valfila; i>=1; i--)
	{

	quitartareafila()

	}

	
}



///////////////// Fin Funciones Generales //////////////////////////

function actualizar_vpermiso()
{
	window.location=url+'principal.php?admin=perm-pers';
}

function crear_permiso()
{


		var npermiso=jQuery('#txtpermiso').val();
		var motivop=jQuery('#txtmotivo').val();
		//var fechainiciop=jQuery('#date1').val();
		fechainiciop=document.getElementsByName('txtfechaini')[0].value;
		var fechafinp=document.getElementsByName('txtfechafin')[0].value;
		var jinmediato=jQuery('#sltasig1').val();
		var rrhh=jQuery('#sltrrhh').val();
		var horainiciop=jQuery('#txthinicio').val();
		var horafinp=jQuery('#txthfin').val();

		dataString= "npermiso="+npermiso + '&motivop='+motivop+'&fechainiciop='+fechainiciop;
		dataString= dataString+ '&fechafinp='+fechafinp+'&jinmediato='+jinmediato;
		dataString= dataString+ '&rrhh='+rrhh+'&horainiciop='+horainiciop+'&horafinp='+horafinp;

		jQuery("#errorMessage").show();
		jQuery("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "rrhh/permisos/crear/insertarpermisos.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	                  jQuery("#error").hide();
	                  jQuery("#registrado").html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">El Permiso ha sido creado con éxito</span>');
	                  jQuery("#registrado").show();
	                  jQuery("#registrado").fadeOut(3000);
	                  jQuery("#errorMessage").hide();

	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  limpiar_campos_permiso();
	                  return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage").hide();
	             	  jQuery("#error").show();
	                  jQuery("#error").html('Hubo un error al eliminar el permiso.');
	                  jQuery("#error").html(result);

	                  return 0;
	             }
	        }
	  });
}

function limpiar_campos_permiso()
{
		document.getElementById('txtpermiso').value="";
		document.getElementById('txtmotivo').value="";

}

function autorizar_permiso(idpermiso)
{
	if (confirm("Está seguro que desea autorizar el permiso indicado?"))
	{
		dataString= "idpermiso="+idpermiso;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "rrhh/permisos/autorizarpermiso.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al autorizar el permiso.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function denegar_permiso(idpermiso)
{
	if (confirm("Está seguro que desea denegar el permiso indicado?"))
	{
		dataString= "idpermiso="+idpermiso;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "rrhh/permisos/denegarpermiso.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al negar el permiso.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function autorizar_permisorrhh(idpermiso)
{
	if (confirm("Está seguro que desea autorizar el permiso indicado?"))
	{
		dataString= "idpermiso="+idpermiso;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "rrhh/permisos/autorizarpermisorrhh.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al autorizar el permiso.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function denegar_permisorrhh(idpermiso)
{
	if (confirm("Está seguro que desea denegar el permiso indicado?"))
	{
		dataString= "idpermiso="+idpermiso;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "rrhh/permisos/denegarpermisorrhh.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al negar el permiso.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function eliminar_permiso(idpermiso)
{
	if (confirm("Está seguro que desea eliminar el permiso indicado?"))
	{
		dataString= "idpermiso="+idpermiso;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "rrhh/permisos/eliminarpermiso.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al negar el permiso.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}


function actualizar_permiso(idpermiso)
{

		//alert(jQuery('#txtpermisoa6').val());
		var npermiso=jQuery('#txtpermisoa'+idpermiso).val();
		var motivop=jQuery('#txtmotivoa'+idpermiso).val();
		var fechainiciop=jQuery('#edate'+(idpermiso*2)).val();
		var fechafinp=jQuery('#edate'+((idpermiso*2)+1)).val();
		var jinmediato=jQuery('#sltasig1a'+idpermiso).val();
		var rrhh=jQuery('#sltrrhha'+idpermiso).val();
		var horainiciop=jQuery('#txthinicioa'+idpermiso).val();
		var horafinp=jQuery('#txthfina'+idpermiso).val();

		dataString= "npermiso="+npermiso + '&idpermiso='+idpermiso + '&motivop='+motivop+'&fechainiciop='+fechainiciop;
		dataString= dataString+ '&fechafinp='+fechafinp+'&jinmediato='+jinmediato;
		dataString= dataString+ '&rrhh='+rrhh+'&horainiciop='+horainiciop+'&horafinp='+horafinp;

		jQuery("#errorMessagea"+idpermiso).show();
		jQuery("#errorMessagea"+idpermiso).fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "rrhh/permisos/actualizar/actualizarpermisos.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registradoa"+idpermiso).hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	                  jQuery("#errora"+idpermiso).hide();
	                  jQuery("#registradoa"+idpermiso).html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">El Permiso ha sido actualizado con éxito</span>');
	                  jQuery("#registradoa"+idpermiso).show();
	                  jQuery("#registradoa"+idpermiso).fadeOut(3000);
	                  jQuery("#errorMessagea"+idpermiso).hide();

	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessagea"+idpermiso).hide();
	             	  jQuery("#errora"+idpermiso).show();
	                  jQuery("#errora"+idpermiso).html('Hubo un error al actualizar el permiso.');
	                  jQuery("#errora"+idpermiso).html(result);

	                  return 0;
	             }
	        }
	  });
}

function actualizar_vreuniones()
{
	window.location=url+'principal.php?admin=reun-pers';
}

function eliminar_reunion(idreunion)
{
	if (confirm("Está seguro que desea eliminar la reunión indicada?"))
	{
		dataString= "idreunion="+idreunion;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/eliminarreunion.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al negar el permiso.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function confirmar_asistencia(idreunion)
{
	if (confirm("Está seguro que desea confirmar la reunión indicado?"))
	{
		dataString= "idreunion="+idreunion;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/confirmarreunion.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al autorizar el permiso.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function cancelar_asistencia(idreunion)
{
	if (confirm("Está seguro que desea cancelar la reunión indicado?"))
	{
		dataString= "idreunion="+idreunion;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/cancelarreunion.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al autorizar el permiso.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function crear_comentario(idreunion)
{
	if (!document.getElementById("txtcomentarioc"+idreunion).value=="")
	{
		var comentario=jQuery('#txtcomentarioc'+idreunion).val();
		dataString= "idreunion="+idreunion+"&comentario="+comentario;
	 
		jQuery("#cerrorMessage"+idreunion).show();
		jQuery("#cerrorMessage"+idreunion).fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/comentarios/insertarcomentario.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#cregistrado"+idreunion).hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	        		  jQuery("#cerror"+idreunion).hide();
	                  jQuery("#cregistrado"+idreunion).html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">El comentario ha sido creado con éxito</span>');
	                  jQuery("#cregistrado"+idreunion).show();
	                  jQuery("#cregistrado"+idreunion).fadeOut(3000);
	                  jQuery("#cerrorMessage"+idreunion).hide();
	                  document.getElementById("txtcomentarioc"+idreunion).value="";
	                  document.getElementById("txtcomentarioc"+idreunion).disabled="disabled";
	             }else{
	             	  jQuery("#cerrorMessage"+idreunion).hide();
	             	  jQuery("#cerror"+idreunion).show();
	                  jQuery("#cerror"+idreunion).html('Hubo un error al autorizar el permiso.');
	                  jQuery("#cerror"+idreunion).html(result);

	                  return 0;
	             }
	        }
	  });
	}else{
		alert("Debe ingresar el comentario");
		document.getElementById("txtcomentarioc"+idreunion).focus()
	}
}

function crear_reunion()
{


		var nreunion=jQuery('#txtreunion').val();
		var motivor=jQuery('#txtmotivo').val();
		var fechainicior=jQuery('#date1').val();
		var clienter=jQuery('#sltclient').val();
		var horainicior=jQuery('#txthinicio').val();
		var horafinr=jQuery('#txthfin').val();

		dataString= "nreunion="+nreunion + '&motivor='+motivor+'&fechainicior='+fechainicior;
		dataString= dataString+'&clienter='+clienter;
		dataString= dataString+'&horainicior='+horainicior+'&horafinr='+horafinr;

		jQuery("#errorMessage").show();
		jQuery("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/crear/insertarreunion.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	                  jQuery("#error").hide();
	                  jQuery("#registrado").html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">La reunión ha sido creado con éxito</span>');
	                  jQuery("#registrado").show();
	                  jQuery("#registrado").fadeOut(3000);
	                  jQuery("#errorMessage").hide();

	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  limpiar_campos_reunion();
	                  return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage").hide();
	             	  jQuery("#error").show();
	                  jQuery("#error").html('Hubo un error al eliminar el permiso.');
	                  jQuery("#error").html(result);

	                  return 0;
	             }
	        }
	  });
}

function actualizar_reunion(idreunion)
{

		//alert(jQuery('#txtpermisoa6').val());
		var nreunion=jQuery('#txtreuniona'+idreunion).val();
		var motivor=jQuery('#txtmotivora'+idreunion).val();
		var clientr=jQuery('#sltclienta'+idreunion).val();
		var fechainicior=jQuery('#edate'+(idreunion)).val();
		var horainicior=jQuery('#txthinicioa'+idreunion).val();
		var horafinr=jQuery('#txthfina'+idreunion).val();

		dataString= "nreunion="+nreunion + '&idreunion='+idreunion + '&motivor='+motivor+'&fechainicior='+fechainicior;
		dataString= dataString+ '&horainicior='+horainicior+'&horafinr='+horafinr+'&clientr='+clientr;

		jQuery("#errorMessagea"+idreunion).show();
		jQuery("#errorMessagea"+idreunion).fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/actualizar/actualizarreuniones.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registradoa"+idreunion).hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	                  jQuery("#errora"+idreunion).hide();
	                  jQuery("#registradoa"+idreunion).html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">La reunión ha sido actualizado con éxito</span>');
	                  jQuery("#registradoa"+idreunion).show();
	                  jQuery("#registradoa"+idreunion).fadeOut(3000);
	                  jQuery("#errorMessagea"+idreunion).hide();

	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessagea"+idreunion).hide();
	             	  jQuery("#errora"+idreunion).show();
	                  jQuery("#errora"+idreunion).html('Hubo un error al actualizar el permiso.');
	                  jQuery("#errora"+idreunion).html(result);

	                  return 0;
	             }
	        }
	  });
}

function limpiar_campos_reunion()
{
		document.getElementById('txtreunion').value="";
		document.getElementById('txtmotivo').value="";

}

function actualizar_vvisitas()
{
	window.location=url+'principal.php?admin=visi-pers';
}

function eliminar_visitas(idvisita)
{
	if (confirm("Está seguro que desea eliminar la Visita indicada?"))
	{
		dataString= "idvisita="+idvisita;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/visitas/eliminarvisita.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al eliminar la visita.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function actualizar_visita(idvisita)
{

		//alert(jQuery('#txtpermisoa6').val());
		var nvisita=jQuery('#txtvisitaa'+idvisita).val();
		var motivor=jQuery('#txtmotivora'+idvisita).val();
		var clientr=jQuery('#sltclienta'+idvisita).val();
		var fechainicior=jQuery('#edate'+(idvisita)).val();
		var horainicior=jQuery('#txthinicioa'+idvisita).val();
		var horafinr=jQuery('#txthfina'+idvisita).val();

		dataString= "nvisita="+nvisita + '&idvisita='+idvisita + '&motivor='+motivor+'&fechainicior='+fechainicior;
		dataString= dataString+ '&horainicior='+horainicior+'&horafinr='+horafinr+'&clientr='+clientr;

		jQuery("#errorMessagea"+idvisita).show();
		jQuery("#errorMessagea"+idvisita).fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/visitas/actualizar/actualizarvisitas.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registradoa"+idvisita).hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	                  jQuery("#errora"+idvisita).hide();
	                  jQuery("#registradoa"+idvisita).html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">La Visita ha sido actualizado con éxito</span>');
	                  jQuery("#registradoa"+idvisita).show();
	                  jQuery("#registradoa"+idvisita).fadeOut(3000);
	                  jQuery("#errorMessagea"+idvisita).hide();

	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessagea"+idvisita).hide();
	             	  jQuery("#errora"+idvisita).show();
	                  jQuery("#errora"+idvisita).html('Hubo un error al actualizar la visita.');
	                  jQuery("#errora"+idvisita).html(result);

	                  return 0;
	             }
	        }
	  });
}

function confirmar_visita(idvisita)
{
	if (confirm("Está seguro que desea confirmar la visita indicada?"))
	{
		dataString= "idvisita="+idvisita;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/visitas/confirmarvisita.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al autorizar el permiso.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function cancelar_visita(idvisita)
{
	if (confirm("Está seguro que desea cancelar la visita indicada?"))
	{
		dataString= "idvisita="+idvisita;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/visitas/cancelarvisita.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al autorizar el permiso.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function crear_visita()
{


		var nvisita=jQuery('#txtvisita').val();
		var motivor=jQuery('#txtmotivo').val();
		var fechainicior=jQuery('#date1').val();
		var clienter=jQuery('#sltclient').val();
		var horainicior=jQuery('#txthinicio').val();
		var horafinr=jQuery('#txthfin').val();

		dataString= "nvisita="+nvisita + '&motivor='+motivor+'&fechainicior='+fechainicior;
		dataString= dataString+'&clienter='+clienter;
		dataString= dataString+'&horainicior='+horainicior+'&horafinr='+horafinr;

		jQuery("#errorMessage").show();
		jQuery("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/visitas/crear/insertarvisita.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	                  jQuery("#error").hide();
	                  jQuery("#registrado").html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">La Visita ha sido creado con éxito</span>');
	                  jQuery("#registrado").show();
	                  jQuery("#registrado").fadeOut(3000);
	                  jQuery("#errorMessage").hide();

	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  limpiar_campos_visita();
	                  return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage").hide();
	             	  jQuery("#error").show();
	                  jQuery("#error").html('Hubo un error al eliminar la visita.');
	                  jQuery("#error").html(result);

	                  return 0;
	             }
	        }
	  });
}

function limpiar_campos_visita()
{
		document.getElementById('txtvisita').value="";
		document.getElementById('txtmotivo').value="";

}

function crear_comentariovisita(idvisita)
{
	if (!document.getElementById("txtcomentarioc"+idvisita).value=="")
	{
		var comentario=jQuery('#txtcomentarioc'+idvisita).val();
		dataString= "idvisita="+idvisita+"&comentario="+comentario;
	 
		jQuery("#cerrorMessage"+idvisita).show();
		jQuery("#cerrorMessage"+idvisita).fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/visitas/comentarios/insertarcomentario.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#cregistrado"+idvisita).hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	        		  jQuery("#cerror"+idvisita).hide();
	                  jQuery("#cregistrado"+idvisita).html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">El comentario ha sido creado con éxito</span>');
	                  jQuery("#cregistrado"+idvisita).show();
	                  jQuery("#cregistrado"+idvisita).fadeOut(3000);
	                  jQuery("#cerrorMessage"+idvisita).hide();
	                  document.getElementById("txtcomentarioc"+idvisita).value="";
	                  document.getElementById("txtcomentarioc"+idvisita).disabled="disabled";
	             }else{
	             	  jQuery("#cerrorMessage"+idvisita).hide();
	             	  jQuery("#cerror"+idvisita).show();
	                  jQuery("#cerror"+idvisita).html('Hubo un error al crear el comentario de la visita.');
	                  jQuery("#cerror"+idvisita).html(result);

	                  return 0;
	             }
	        }
	  });
	}else{
		alert("Debe ingresar el comentario");
		document.getElementById("txtcomentarioc"+idreunion).focus()
	}
}

function actualizar_vreunionesg()
{
	window.location=url+'principal.php?admin=reun-gest';
}

function actualizar_vvisitasg()
{
	window.location=url+'principal.php?admin=visi-gest';
}

function crear_cliente()
{
	if (!document.getElementById("txtnombrecli").value=="")
	{
		var ncliente=jQuery('#txtnombrecli').val();
		var observacioncli=jQuery('#txtobservacioncli').val();
		var ruc=jQuery('#txtruc').val();
		dataString= "ncliente="+ncliente+"&observacioncli="+observacioncli+"&ruc="+ruc;
	 
		jQuery("#errorMessage").show();
		jQuery("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/empresas/insertarcliente.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	        		  jQuery("#error").hide();
	                  jQuery("#registrado").html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">El Cliente ha sido creado con éxito</span>');
	                  jQuery("#registrado").show();
	                  jQuery("#registrado").fadeOut(3000);
	                  jQuery("#errorMessage").hide();
	                  document.getElementById("txtcomentarioc"+idvisita).value="";
	                  document.getElementById("txtcomentarioc"+idvisita).disabled="disabled";
	             }else{
	             	  jQuery("#errorMessage").hide();
	             	  jQuery("#error").show();
	                  jQuery("#error").html('Hubo un error al crear el comentario de la visita.');
	                  jQuery("#error").html(result);

	                  return 0;
	             }
	        }
	  });
	}else{
		alert("Debe ingresar el nombre del cliente");
		document.getElementById('txtnombrecli').focus()
	}
}

function actualizar_vcliente()
{
	window.location=url+'principal.php?admin=clie-gest';
}

function eliminar_cliente(idcliente)
{
	if (confirm("Está seguro que desea eliminar el indicado?"))
	{
		dataString= "idcliente="+idcliente;
	
		jQuery("#errorMessage2").show();
		jQuery("#errorMessage2").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "gestioncomercial/empresas/eliminarcliente.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage2").hide();
	             	  jQuery("#error2").show();
	                  jQuery("#error2").html('Hubo un error al eliminar el cliente.');
	                  jQuery("#error2").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

function actualizarnotificaciones()
{

	dataString= "idcliente=1";
	jQuery.ajax({
	             type: "POST",
	             url: "notificaciones/notificacionesc.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	            // jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	            // jQuery("#identnotificaciones").html("1");
	             jQuery("#identnotificaciones").show();
	             jQuery("#identnotificaciones").html('&nbsp;'+result+'&nbsp;');

	              //document.getElementById('identnotificaciones').style="display:;";
	             if(result>0){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	           //  location.reload();
	           jQuery("#identnotificaciones").html('&nbsp;'+result+'&nbsp;');
	              jQuery("#identnotificaciones").show();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	 // jQuery("#identnotificaciones").hide();
	             	  

	                  return 0;
	             }
	        }
	  });
}

function actualizar_notificacion(idnotificacion,urldest)
{
	dataString= "idnotificacion="+idnotificacion;
	jQuery.ajax({
	             type: "POST",
	             url: "notificaciones/actualizarnot.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             if(result>0){
	           		window.location=url+urldest;
	             }else{
	                  return 0;
	             }
	        }
	  });
	
}

function actualizar_perfil()
{

	password= jQuery("#txtpass").val();
	passwordc= jQuery("#txtconfpass").val();
	cedula= jQuery("#txtcedula").val();
	nombres= jQuery("#txtnombres").val();
    apellidos= jQuery("#txtapellidos").val();
    correo= jQuery("#txtcorreo").val();
	fechanac= jQuery("#date1").val();
	if (password!="")
	{
		if (password==passwordc)
		{
			if (correo!="")
			{
					dataString= 'password='+password+'&cedula='+cedula+'&nombres='+nombres;
					dataString=dataString+'&apellidos='+apellidos+'&correo='+correo+'&fechanac='+fechanac;
					jQuery.ajax({
					             type: "POST",
					             url: "usuarios/actualizarperfil.php",
					             data: dataString,
					             cache: false,
					             success: function(result){
					             var result=result;
					             if(result>0){
				 					  jQuery("#error").hide();
					                  jQuery("#registrado").html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">Perfil actualizado con éxito</span>');
					                  jQuery("#registrado").show();
					                  jQuery("#registrado").fadeOut(3000);
					                  jQuery("#errorMessage").hide();

					             }else{
					             	  jQuery("#errorMessage").hide();
					             	  jQuery("#error").show();
					                  jQuery("#error").html('Hubo un error al actualizar el perfil');
					                  jQuery("#error").html(result);

					                  return 0;
					             
					             }
					        }
					  });
	 		}else{
		 		alert('Debe ingresar un correo.')
		 		jQuery("#txtcorreo").focus()
		 	}
	 	}else{
	 		alert('Las Contraseñas no coinciden.')
	 		jQuery("#txtpass").focus()
	 	}
	 }else{
	 		alert('No puede estar vacio el password')
	 		jQuery("#txtpass").focus()
	 }
}

function crear_alarma(tipo)
{
	if (tipo==1)
	{
		idproyecto= jQuery("#sltproyecto").val();
		periodop= jQuery("#sltperiodop").val();
		pperiodop= jQuery("#txtporcentajep").val();
		usuariop= jQuery("#sltasig1").val();
		idtarea=0;
	}else{
		idproyecto=0;
		idtarea= jQuery("#slttarea").val();
		periodop= jQuery("#sltperiodot").val();
		pperiodop= jQuery("#txtporcentajet").val();
		usuariop= jQuery("#sltasig2").val();
	}

	dataString= 'idproyecto='+idproyecto+'&periodop='+periodop+'&pperiodop='+pperiodop;
	dataString=dataString+'&usuariop='+usuariop+'&idtarea='+idtarea;
					jQuery.ajax({
					             type: "POST",
					             url: "alarmas/crear/insertaralarma.php",
					             data: dataString,
					             cache: false,
					             success: function(result){
					             var result=result;
					             if(result>0){
				 					  jQuery("#error").hide();
					                  jQuery("#registrado").html('<div style="line-height: 15px; background: green; height: 13px; width: 3px; float: left;"></div>&nbsp;<span style="color: green;">Alarma Generada con éxito</span>');
					                  jQuery("#registrado").show();
					                  jQuery("#registrado").fadeOut(3000);
					                  jQuery("#errorMessage").hide();

					             }else{
					             	  jQuery("#errorMessage").hide();
					             	  jQuery("#error").show();
					                  jQuery("#error").html('Hubo un error al Generar la Alarma');
					                  jQuery("#error").html(result);

					                  return 0;
					             
					             }
					        }
	});


}

function eliminar_alarmap(idalarma)
{
	if (confirm("Está seguro que desea eliminar la alarma?"))
	{
		dataString= "idalarma="+idalarma;
	
		jQuery("#errorMessage").show();
		jQuery("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "alarmas/eliminaralarma.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             jQuery("#registrado").hide();
	             //jQuery("#errorMessage").hide();
	             if(result==1){
	                  //window.location='principal.php';
	                  //jQuery("#error").html('');
	             location.reload();
	                  //jQuery("#dvusuario"+id).fadeOut(500);
	                  //limpiar_campos_permiso();
	                  //return 1;
	                  //limpiar_factura();
	                  //alert('guardado');
	             }else{
	             	  jQuery("#errorMessage").hide();
	             	  jQuery("#error").show();
	                  jQuery("#error").html('Hubo un error al eliminar el cliente.');
	                  jQuery("#error").html(result);

	                  return 0;
	             }
	        }
	  });
	}
}

      function myformatter(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
        }
        function myparser(s){
            if (!s) return new Date();
            var ss = (s.split('-'));
            var y = parseInt(ss[0],10);
            var m = parseInt(ss[1],10);
            var d = parseInt(ss[2],10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                return new Date(d,m-1,y);
            } else {
                return new Date();
            }
        }

     
        function agregartareafila()
        {
        	var valfila=parseInt(jQuery("#txtval").val());
        	valfilanew=parseInt(valfila)+1;
        	//var obj = jQuery("sltasig1").clone();
        	jQuery("#btnag"+valfilanew).hide();
        	jQuery("#agcolumna"+valfilanew).hide();
        	jQuery("#columna"+valfilanew).fadeIn(400);
        	jQuery("#agcolumna"+(valfilanew+1)).fadeIn(400);
        	document.getElementById('txtval').value=valfilanew;

        	var sumf=sumaFecha(1,document.getElementsByName('txtfechatarfin'+valfila)[0].value);
        	sumf=sumf.replace("/","-");
        	sumf=sumf.replace("/","-");
        	//alert(document.getElementsByName('txtfechafin')[0].value);
        	$('#txtfechatarini'+valfilanew).datebox('setValue', sumf);	// set datebox value
        	$('#txtfechatarfin'+valfilanew).datebox('setValue', document.getElementsByName('txtfechafin')[0].value);	// set datebox value

        }

                function quitartareafila()
        {
        	var valfila=parseInt(jQuery("#txtval").val());
        	valfilanew=parseInt(valfila)+1;
        	//var obj = jQuery("sltasig1").clone();
        	jQuery("#columna"+valfila).hide();
        	jQuery("#agcolumna"+(valfilanew)).hide();
        	
        	jQuery("#agcolumna"+valfila).fadeIn();
        	jQuery("#btnag"+valfila).fadeIn();

        	document.getElementById('txtval').value=(parseInt(valfila)-1);

        }

function agregartareaedfila(tarea)
        {
        	//tarea='t'+tarea;
        	var valfila=parseInt(jQuery("#txtval"+tarea).val());
        	valfilanew=parseInt(valfila)+1;
        	//var obj = jQuery("sltasig1").clone();
        	jQuery("#edbtnag"+valfilanew+tarea).hide();
        	jQuery("#edagcolumna"+valfilanew+tarea).hide();
        	jQuery("#edcolumna"+valfilanew+tarea).fadeIn(400);
        	jQuery("#edagcolumna"+(valfilanew+1)+tarea).fadeIn(400);
        	document.getElementById('txtval'+tarea).value=valfilanew;

        	var sumf=sumaFecha(1,document.getElementsByName('txtfechatarfin'+valfila)[0].value);
        	sumf=sumf.replace("/","-");
        	sumf=sumf.replace("/","-");
        	//alert(document.getElementsByName('txtfechafin')[0].value);
        	$('#txtfechatarini'+valfilanew).datebox('setValue', sumf);	// set datebox value
        	$('#txtfechatarfin'+valfilanew).datebox('setValue', document.getElementsByName('txtfechafin')[0].value);	// set datebox value
        }

function quitartareaedfila(tarea)
        {
        	var valfila=parseInt(jQuery("#txtval"+tarea).val());
        	valfilanew=parseInt(valfila)+1;
        	//var obj = jQuery("sltasig1").clone();
        	jQuery("#edcolumna"+valfila+tarea).hide();
        	jQuery("#edagcolumna"+(valfilanew)+tarea).hide();
        	
        	jQuery("#edagcolumna"+valfila+tarea).fadeIn();
        	jQuery("#edbtnag"+valfila+tarea).fadeIn();

        	document.getElementById('txtval'+tarea).value=(parseInt(valfila)-1);

        }

  function sumaFecha(d, fecha)
		{
			fecha=fecha.replace("-","/");
			fecha=fecha.replace("-","/");
			fecha=fecha.substring(0,2)+'/'+fecha.substring(3,5)+'/'+fecha.substring(6,10);

			//fecha= new Date(fecha);

			 var Fecha = new Date();
			 var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
			 var sep = sFecha.toString().indexOf('/') != -1 ? '/' : '-'; 
			 var aFecha = sFecha.toString().split(sep);
			 var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
			 fecha= new Date(fecha);
			 fecha.setDate(fecha.getDate()+parseInt(d));
			 var anno=fecha.getFullYear();
			 var mes= fecha.getMonth()+1;
			 var dia= fecha.getDate();
			 mes = (mes < 10) ? ("0" + mes) : mes;
			 dia = (dia < 10) ? ("0" + dia) : dia;
			 var fechaFinal = dia+sep+mes+sep+anno;
			 return (fechaFinal);
		 }
function exportar_excel_materias(usuario)
{
	//if (confirm("Está seguro que desea eliminar la alarma?"))
	//{
		dataString= "";
	
		//jQuery("#errorMessage").show();
		//jQuery("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "mantenimientos/materias/exportarexcel.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             //if(result==1){
	      //       	jQuery("#errorMessage").hide();
	             	 $.messager.alert('Exportar Materias','Archivo Exportado Exitosamente. Revìselo <a href="archivosexp/materia'+usuario+'.xlsx" target="_blank">Aquí</a>','info');
	             //}else{

	             //}
	        }
	  });
	//}
}

function exportar_excel_docentes(usuario)
{
	//if (confirm("Está seguro que desea eliminar la alarma?"))
	//{
		dataString= "";
	
		//jQuery("#errorMessage").show();
		//jQuery("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "mantenimientos/docentes/exportarexcel.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             //if(result==1){
	      //       	jQuery("#errorMessage").hide();
	             	 $.messager.alert('Exportar Materias','Archivo Exportado Exitosamente. Revìselo <a href="archivosexp/docentes'+usuario+'.xlsx" target="_blank">Aquí</a>','info');
	             //}else{

	             //}
	        }
	  });
	//}
}

function exportar_excel_estudiantes(usuario)
{
	//if (confirm("Está seguro que desea eliminar la alarma?"))
	//{
		dataString= "";
	
		//jQuery("#errorMessage").show();
		//jQuery("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "mantenimientos/estudiantes/exportarexcel.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             //if(result==1){
	      //       	jQuery("#errorMessage").hide();
	             	 $.messager.alert('Exportar Materias','Archivo Exportado Exitosamente. Revìselo <a href="archivosexp/estudiantes'+usuario+'.xlsx" target="_blank">Aquí</a>','info');
	             //}else{

	             //}
	        }
	  });
	//}
}

function mensaje(texto,tipo,alto,ancho)
	{
		var icono="";
		if (!alto){ alto='200px'; }
		if (!ancho){ ancho='25px'; }
		var modalmode=false;
		//console.log($(".messager-body").css("height","25px;"));
		$(".messager-body").css("height",'25px !important;')
		//console.log($(".messager-body").css("height"));
		

		if (tipo=='procesar') { icono='<img src="images/loading.gif" style="height:15%;"/>'; modalmode=true; }
		if (tipo=='ok') { icono='<img src="images/visto-azul.png" style="height:15%;"/>'; modalmode=false;}
		if (tipo=='error') { icono='<img src="images/error_message.png" style="height:15%;"/>'; modalmode=false;}
		if (tipo=='advertencia') { icono='<img src="images/advertencia_message.png" style="height:15%;"/>'; modalmode=false;}
		if (tipo=='info') { icono='<img src="images/observacion.png" style="height:15%;"/>'; modalmode=false;}

	         result=   $.messager.show({
                msg:icono+' '+texto,
                showType:'fade',
                closable:false,
                timeout:2000,
                modal:modalmode,
                style:{
                    left:'',
                    right:'0.5%',
                    width:alto,
                    padding:'1px',
                    height:ancho,
                    top:document.body.scrollTop+document.documentElement.scrollTop+10,
                    bottom:''
                }})
          return result;
    }


function fecha_actual()
{
	var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var f=new Date();
	return (f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
}
	



function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

$.extend($.fn.datebox.defaults,{
	formatter:function(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
	},
	parser:function(s){
		if (!s) return new Date();
		var ss = s.split('/');
		var d = parseInt(ss[0],10);
		var m = parseInt(ss[1],10);
		var y = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d);
		} else {
			return new Date();
		}
	}
});

