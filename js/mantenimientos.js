
////****   Mantenimiento de Cursos ******///////
function eliminarCurso(id)
{
	console.log(id);
	$.messager.confirm('Confirm','¿Desea eliminar el curso seleccionado?',function(r){
    	if (r){
	
			var dataString = 'idcurso='+ id;
			var espere=mensaje('Espere...','procesar','100px','');
		 	var validacion=true;
	       jQuery.ajax({
		             type: "POST",
		             url: "mantenimientos/cursos/eliminarcurso.query.php",
		             data: dataString,
		             cache: false,
		             success: function(result){
		             var result=result;
		             espere.dialog('close');
		             if(result==1){
		                mensaje('Curso eliminado con éxito.','ok','','');
		                //location.reload();
		                $('#cursos').datagrid('loadData', {"total":0,"rows":[]});
          				$('#cursos').datagrid('load', 'mantenimientos/cursos/cursos.query.php?idc=1');
		             }else{
						mensaje('Error al eliminar el curso.','error','','');
             			console.log(result);
		             }
		        }
		  });

		}

	});	
}

function editarCurso(id)
{
	console.log(id);
	mensaje('Función no disponible.','info','','');
}

////****   Fin Mantenimiento de Cursos ******///////


////****   Mantenimiento de Cursos Nivelación ******///////

function openagregarCursoNivelacion ()
{
	//console.log(id);
	$("#agregarCursoNivelacion").empty();
    var win;
    $("#agregarCursoNivelacion").window({
        width: 400,
        height: 350,
        modal: true,
        href: 'mantenimientos/cursoniv/crear/index.php',
        title: 'Agregar Curso - Nivelación',
        collapsible: false,
        maximizable: true,
        minimizable: false,
        onResize: function () {
            $(this).window('center');
        }
    });
	$('#agregarCursoNivelacion').window('open');
	//mensaje('Función no disponible.','info','','');
}

function eliminarCursoNivelacion(id)
{
	console.log(id);
	$.messager.confirm('Confirm','¿Desea eliminar el curso nivelación seleccionado?',function(r){
    	if (r){
	
			var dataString = 'idcursoniv='+ id;
			var espere=mensaje('Espere...','procesar','100px','');
		 	var validacion=true;
	       jQuery.ajax({
		             type: "POST",
		             url: "mantenimientos/cursoniv/eliminar/eliminarcursoniv.query.php",
		             data: dataString,
		             cache: false,
		             success: function(result){
		             var result=result;
		             espere.dialog('close');
		             if(result==1){
		                mensaje('Curso eliminado con éxito.','ok','','');
		                //location.reload();
		                $('#cursonive').datagrid('loadData', {"total":0,"rows":[]});
          				$('#cursonive').datagrid('load', 'mantenimientos/cursoniv/cursoniv.query.php?idlectivo=0');
		             }else{
						mensaje('Error al eliminar el curso.','error','','');
             			console.log(result);
		             }
		        }
		  });

		}

	});	
}

function editarCursoNivelacion(id)
{
	console.log(id);
	mensaje('Función no disponible.','info','','');
}

function agregarCursoNivelacion()
{
	/*$('.window-proxy-mask, .window-mask').css({'z-index': '9998'});*/
	var espere=mensaje('Espere...','procesar','100px','');
	var validacion=true;
	
	var descripcion=$('#txtnombres').val(); 	
	var periodolec=$("#slt_periodo").combobox('getValue');
	var fechainicio=$('#txtfechainicio').datebox('getValue'); 	
	var fechafin=$('#txtfechafin').datebox('getValue');
		      
	var dataString = 'descripcion='+ descripcion+'&periodolec='+ periodolec;
	var dataString = dataString + '&fechainicio='+ fechainicio+'&fechafin='+ fechafin;


	console.log(dataString);

	if (!descripcion){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtnombres').focus(); validacion=false;}
	if (!fechainicio){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtfechainicio').focus(); validacion=false;}
	if (!fechafin){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtfechafin').focus(); validacion=false;}
	//if (!cedula){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtcedula').focus(); validacion=false;}

	if (validacion)
	{
		$.ajax({
             type: "POST",
             url: "mantenimientos/cursoniv/crear/insertarcursoniv.php",
             data: dataString,
             cache: false, 
             success: function(result){
             var result=result;
             espere.dialog('close');
             if(result==1){
             	
             	mensaje('Curso Niv. ingresado con éxito.','ok','215px','');

             }else{
             	mensaje('Error al ingresar el curso Niv.','error','215px','');
             	console.log(result);
             }
       	 }
 		});
	}

}

function exportarExcelEstudiantesCursoNivelacion(usuario,cursoniv)
{
	//if (confirm("Está seguro que desea eliminar la alarma?"))
	//{

		dataString= "idcursoniv="+cursoniv;
		console.log(dataString)
	
		//jQuery("#errorMessage").show();
		//jQuery("#errorMessage").fadeIn(400).html('<img src="images/loading.gif" />');
		jQuery.ajax({
	             type: "POST",
	             url: "mantenimientos/cursonivest/exportarexcel.php",
	             data: dataString,
	             cache: false,
	             success: function(result){
	             var result=result;
	             //if(result==1){
	      //       	jQuery("#errorMessage").hide();
	             	 $.messager.alert('Exportar Materias','Archivo Exportado Exitosamente. Revìselo <a href="archivosexp/estudiantes'+usuario+'.xlsx" target="_blank">Aquí</a>','info');
	             //}else{

	             //}
	        }
	  });
	//}
}

////****   Fin Mantenimiento de Cursos Nivelación ******///////


////****   Mantenimiento de Estudiantes ******///////

function openagregarEstudiante ()
{
	//console.log(id);
	$("#agregarEstudiante").empty();
    var win;
    $("#agregarEstudiante").window({
        width: 700,
        height: 500,
        modal: true,
        href: 'mantenimientos/estudiantes/crear/index.php',
        title: 'Editar Estudiante',
        collapsible: false,
        maximizable: true,
        minimizable: false,
        onResize: function () {
            $(this).window('center');
        }
    });
	$('#agregarEstudiante').window('open');
	//mensaje('Función no disponible.','info','','');
}

function editarEstudiante(id)
{
	console.log(id);
	$("#editarEstudiante").empty();
    var win;
    $("#editarEstudiante").window({
        width: 700,
        height: 500,
        modal: true,
        href: 'mantenimientos/estudiantes/editar/index.php?id='+id,
        title: 'Editar Estudiante',
        collapsible: false,
        maximizable: true,
        minimizable: false,
        onResize: function () {
            $(this).window('center');
        }
    });
	$('#editarEstudiante').window('open');
	//mensaje('Función no disponible.','info','','');
}


function eliminarEstudiante(id)
{
	console.log(id);
	$.messager.confirm('Confirm','¿Desea eliminar el estudiante seleccionado?',function(r){
    	if (r){
	
			var dataString = 'idestudiante='+ id;
			var espere=mensaje('Espere...','procesar','100px','');
		 	var validacion=true;
	       jQuery.ajax({
		             type: "POST",
		             url: "mantenimientos/estudiantes/eliminar/eliminarestudiante.query.php",
		             data: dataString,
		             cache: false,
		             success: function(result){
		             var result=result;
		             espere.dialog('close');
		             if(result==1){
		                mensaje('Estudiante eliminado con éxito.','ok','220px','');
		                //location.reload();
		                var value=$("#slt_curso").combobox('getValue');
		                actualizarGrid('#estudiantes','mantenimientos/estudiantes/estudiantecurso.query.php?idc='+value)
		               	
		             }else{
						mensaje('Error al eliminar el Estudiante.','error','','');
             			console.log(result);
		             }
		        }
		  });

		}

	});	
}

function agregar_estudiante()
{
	/*$('.window-proxy-mask, .window-mask').css({'z-index': '9998'});*/
	var espere=mensaje('Espere...','procesar','100px','');
	var validacion=true;
	
	var apellidos=$('#txtapellidos').val(); 	var nombres=$('#txtnombres').val();
	var cedula=$('#txtcedula').val(); 			var curso=$('#sltcurso').val();
	var fechanac=document.getElementsByName('txtfecha')[0].value; 
	var correo=$('#txtcorreo').val(); 			var direccion=$('#txtdireccion').val();
	var telefono=$('#txttelefono').val(); 		var celular=$('#txtcelular').val();
	var nombrepa=$('#txtnombrepa').val(); 		var cedulapa=$('#txtcedulapa').val();
	var direccionpa=$('#txtdireccionpa').val(); var ocupacionpa=$('#txtocupacionpa').val();
	var telefonopa=$('#txttelefonopa').val(); 	var nombrema=$('#txtnombrema').val();
	var celularpa=$('#txtcelularpa').val();	var celularma=$('#txtcelularma').val();
	var cedulama=$('#txtcedulama').val();		var direccionma=$('#txtdireccionma').val(); 
	var ocupacionma=$('#txtocupacionma').val();	var telefonoma=$('#txttelefonoma').val();
	var niv=$('#txtniv').val();					var fechaing=$('#txtfechaingr').datebox('getValue');
	var cursoact=$('#txtcuract').val();			var paral=$('#txtparal').val();
	var espe=$('#txtespe').val();				var colegio=$('#txtcolact').val();
	var sexo=$('#sltsexo').val();
//	var calificacionalfa=$('#txtcalialfa').val();
	
	      
	var dataString = 'apellidos='+ apellidos+'&nombres='+ nombres;
	var dataString = dataString + '&cedula='+ cedula+'&curso='+ curso;
	var dataString = dataString + '&fechanac='+ fechanac+'&correo='+ correo;
	var dataString = dataString + '&direccion='+ direccion+'&telefono='+ telefono;
	var dataString = dataString + '&celular='+ celular+'&nombrepa='+ nombrepa;
	var dataString = dataString + '&cedulapa='+ cedulapa+'&direccionpa='+ direccionpa;
	var dataString = dataString + '&ocupacionpa='+ ocupacionpa+'&telefonopa='+ telefonopa;
	var dataString = dataString + '&celularpa='+ celularpa+'&celularma='+ celularma;
	var dataString = dataString + '&nombrema='+ nombrema+'&cedulama='+ cedulama;
	var dataString = dataString + '&direccionma='+ direccionma+'&ocupacionma='+ ocupacionma;
	var dataString = dataString + '&telefonoma='+ telefonoma+'&sexo='+ sexo;
	var dataString = dataString + '&fechaing='+ fechaing+'&txtniv='+ niv;
	var dataString = dataString + '&espe='+ espe+'&colegio='+ colegio;
	var dataString = dataString + '&cursoact='+ cursoact+'&paral='+ paral;

	//console.log(dataString);

	if (!apellidos){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtapellidos').focus(); validacion=false;}
	if (!nombres){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtnombres').focus(); validacion=false;}
	if (!cedula){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtcedula').focus(); validacion=false;}
	//if (!cedula){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtcedula').focus(); validacion=false;}

	if (validacion)
	{
		$.ajax({
             type: "POST",
             url: "mantenimientos/estudiantes/crear/insertarestudiante.php",
             data: dataString,
             cache: false,
             success: function(result){
             var result=result;
             espere.dialog('close');
             if(result==1){
             	
             	mensaje('Estudiante ingresado con éxito.','ok','215px','');

             }else{
             	mensaje('Error al ingresar el estudiante.','error','215px','');
             	console.log(result);
             }
       	 }
 		});
	}

}


function actualizarEstudiante(id)
{
	/*$('.window-proxy-mask, .window-mask').css({'z-index': '9998'});*/
	var espere=mensaje('Espere...','procesar','100px','');
	var validacion=true;


	
	var idestudiante=$('#idestudiante').val();
	var apellidos=$('#txtapellidos').val(); 	var nombres=$('#txtnombres').val();
	var cedula=$('#txtcedula').val(); 			var curso=$('#sltcurso').val();
	var fechanac=document.getElementsByName('txtfecha')[0].value; 
	var correo=$('#txtcorreo').val(); 			var direccion=$('#txtdireccion').val();
	var telefono=$('#txttelefono').val(); 		var celular=$('#txtcelular').val();
	var nombrepa=$('#txtnombrepa').val(); 		var cedulapa=$('#txtcedulapa').val();
	var direccionpa=$('#txtdireccionpa').val(); var ocupacionpa=$('#txtocupacionpa').val();
	var telefonopa=$('#txttelefonopa').val(); 	var nombrema=$('#txtnombrema').val();
	var celularpa=$('#txtcelularpa').val();	var celularma=$('#txtcelularma').val();
	var cedulama=$('#txtcedulama').val();		var direccionma=$('#txtdireccionma').val(); 
	var ocupacionma=$('#txtocupacionma').val();	var telefonoma=$('#txttelefonoma').val();
	/*var niv=$('#txtniv').val();					var fechaing=$('#txtfechaingr').datebox('getValue');*/
	var cursoact=$('#txtcuract').val();			var paral=$('#txtparal').val();
	var espe=$('#txtespe').val();				var colegio=$('#txtcolact').val();
	var sexo=$('#sltsexo').val();
//	var calificacionalfa=$('#txtcalialfa').val();
	
	    
	var dataString = 'apellidos='+ apellidos+'&nombres='+ nombres;
	var dataString = dataString + '&cedula='+ cedula+'&curso='+ curso;
	var dataString = dataString + '&fechanac='+ fechanac+'&correo='+ correo;
	var dataString = dataString + '&direccion='+ direccion+'&telefono='+ telefono;
	var dataString = dataString + '&celular='+ celular+'&nombrepa='+ nombrepa;
	var dataString = dataString + '&cedulapa='+ cedulapa+'&direccionpa='+ direccionpa;
	var dataString = dataString + '&ocupacionpa='+ ocupacionpa+'&telefonopa='+ telefonopa;
	var dataString = dataString + '&celularpa='+ celularpa+'&celularma='+ celularma;
	var dataString = dataString + '&nombrema='+ nombrema+'&cedulama='+ cedulama;
	var dataString = dataString + '&direccionma='+ direccionma+'&ocupacionma='+ ocupacionma;
	var dataString = dataString + '&telefonoma='+ telefonoma+'&sexo='+ sexo;
	//var dataString = dataString + '&fechaing='+ fechaing+'&txtniv='+ niv;
	var dataString = dataString + '&espe='+ espe+'&colegio='+ colegio;
	var dataString = dataString + '&cursoact='+ cursoact+'&paral='+ paral;
	var dataString = dataString + '&idestudiante='+ idestudiante;


	console.log(dataString);

	if (!apellidos){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtapellidos').focus(); validacion=false;}
	if (!nombres){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtnombres').focus(); validacion=false;}
	if (!cedula){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtcedula').focus(); validacion=false;}
	//if (!cedula){ espere.dialog('close'); mensaje('Faltan campos de ingresar','advertencia','',''); $('#txtcedula').focus(); validacion=false;}

	if (validacion)
	{
		$.ajax({
             type: "POST",
             url: "mantenimientos/estudiantes/editar/actualizarestudiante.query.php",
             data: dataString,
             cache: false,
             success: function(result){
             var result=result;
             espere.dialog('close');
             if(result==1){
             	console.log(result);
             	mensaje('Estudiante actualizado con éxito.','ok','215px','');

             }else{
             	mensaje('Error al actualizar el estudiante.','error','215px','');
             	console.log(result);
             }
       	 }
 		});
	}

}

////****   Fin Mantenimiento de Estudiantes ******///////

function actualizarGrid(idcontrol,params)
{
	$(idcontrol).datagrid('loadData', {"total":0,"rows":[]});
    $(idcontrol).datagrid('load', params);
}