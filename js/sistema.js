function agregarMenuSuperior()
{
	//console.log(id);
	$("#agregarMenuSuperior").empty();
    var win;
    $("#agregarMenuSuperior").window({
        width: 320,
        height: 320,
        modal: true,
        href: 'sistema/menus/crear/index.php',
        title: ' Agregar Menú Superior',
        collapsible: false,
        maximizable: false,
        minimizable: false,
        onResize: function () {
            $(this).window('center');
        }
    });
	$('#agregarMenuSuperior').window('open');
	//mensaje('Función no disponible.','info','','');
}

/****     Menu Lateral  *****/

function eliminarMenuLateral(id)
{
    console.log(id);
    $.messager.confirm('Confirm','¿Desea eliminar el curso seleccionado?',function(r){
        if (r){
    
            var dataString = 'idcurso='+ id;
            var espere=mensaje('Espere...','procesar','100px','');
            var validacion=true;
           jQuery.ajax({
                     type: "POST",
                     url: "mantenimientos/cursos/eliminarcurso.query.php",
                     data: dataString,
                     cache: false,
                     success: function(result){
                     var result=result;
                     espere.dialog('close');
                     if(result==1){
                        mensaje('Curso eliminado con éxito.','ok','','');
                        //location.reload();
                        $('#cursos').datagrid('loadData', {"total":0,"rows":[]});
                        $('#cursos').datagrid('load', 'mantenimientos/cursos/cursos.query.php?idc=1');
                     }else{
                        mensaje('Error al eliminar el curso.','error','','');
                        console.log(result);
                     }
                }
          });

        }

    }); 
}

function editarMenuLateral(id)
{
    console.log(id);
    mensaje('Función no disponible.','info','','');
}





function eliminarMenuSuperior(id)
{
    console.log(id);
    $.messager.confirm('Confirm','¿Desea eliminar el menú seleccionado?',function(r){
        if (r){
    
            var dataString = 'idcurso='+ id;
            var espere=mensaje('Espere...','procesar','100px','');
            var validacion=true;
           jQuery.ajax({
                     type: "POST",
                     url: "mantenimientos/cursos/eliminarcurso.query.php",
                     data: dataString,
                     cache: false,
                     success: function(result){
                     var result=result;
                     espere.dialog('close');
                     if(result==1){
                        mensaje('Curso eliminado con éxito.','ok','','');
                        //location.reload();
                        $('#menusuperior').datagrid('loadData', {"total":0,"rows":[]});
                        $('#menusuperior').datagrid('load', 'sistema/menus/menusuperior.query.php');
                     }else{
                        mensaje('Error al eliminar el curso.','error','','');
                        console.log(result);
                     }
                }
          });

        }

    }); 
}

function editarMenuSuperior(id)
{
    console.log(id);
    mensaje('Función no disponible.','info','','');
}

function openagregarCursoNivelacion ()
{
    //console.log(id);
    $("#agregarCursoNivelacion").empty();
    var win;
    $("#agregarCursoNivelacion").window({
        width: 400,
        height: 350,
        modal: true,
        href: 'mantenimientos/cursoniv/crear/index.php',
        title: 'Agregar Curso - Nivelación',
        collapsible: false,
        maximizable: true,
        minimizable: false,
        onResize: function () {
            $(this).window('center');
        }
    });
    $('#agregarCursoNivelacion').window('open');
    //mensaje('Función no disponible.','info','','');
}

/****  Fin Menu Lateral  ****/