/*
SQLyog Ultimate v8.3 
MySQL - 5.5.16 : Database - dbautcontrol
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


/*Table structure for table `tblempresa` */

DROP TABLE IF EXISTS `tblempresa`;

CREATE TABLE `tblempresa` (
  `idempresa` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `razonsocial` text,
  `telefono` varchar(10) DEFAULT NULL,
  `telefono2` varchar(10) DEFAULT NULL,
  `direccion` varchar(400) DEFAULT NULL,
  `representantel` text,
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idestatus` int(11) NOT NULL,
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tblempresa` */

insert  into `tblempresa`(`idempresa`,`descripcion`,`razonsocial`,`telefono`,`telefono2`,`direccion`,`representantel`,`fechacreacion`,`idestatus`) values (1,'SETEC',NULL,NULL,NULL,NULL,NULL,'2015-07-10 10:38:26',1),(2,'GLOBAL REINSURANCE',NULL,NULL,NULL,NULL,NULL,'2015-07-10 10:38:38',1),(3,'NATIONAL PETROLEUM COMPANY',NULL,NULL,NULL,NULL,NULL,'2015-07-10 10:38:51',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
