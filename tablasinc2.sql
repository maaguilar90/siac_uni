/*
SQLyog Ultimate v8.3 
MySQL - 5.5.16 : Database - dbautcontrol
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


/*Table structure for table `tblconfig` */

DROP TABLE IF EXISTS `tblconfig`;

CREATE TABLE `tblconfig` (
  `id` int(2) NOT NULL,
  `nombresoft` text NOT NULL,
  `version` int(11) NOT NULL,
  `subversion` int(11) NOT NULL,
  `compilacion` text NOT NULL,
  `titulo` text NOT NULL,
  `estilo` enum('Azul','Verde','Rojo','Blanco') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tblconfig` */

insert  into `tblconfig`(`id`,`nombresoft`,`version`,`subversion`,`compilacion`,`titulo`,`estilo`) values (1,'COVE',1,1,'1','SISTEMA DE CONTROL EMPRESARIAL (AUTHCONTROL)','Azul');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
