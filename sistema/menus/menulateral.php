<div style="border: 1px solid #ccc;padding-right: 8px;padding-left: 8px;padding-bottom: 5px;padding-top: 5px;">
	<div>
	  <a onclick="javascript:openagregarSubmenu()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'">Agregar</a>
	</div> 
</div>
<br>
<div>
	<span class="titulo">Administrador de Menús</span>
</div>
<div>
 	<div style="margin-top:10px;">
 	<span>Seleccione el curso: &nbsp;</span>
		<select id="slt_menu" name="slt_menu"class="easyui-combobox" style="width:20%;" data-options="editable:false" >
			<?php
			    $query="CALL SP_SL_MENUSUPERIOR(1);";
				$sqlquery= executeQuery($query,"");
				$cont=0;
						while($fila=mysqli_fetch_object($sqlquery))
						{
					           $cont=$cont+1;
					           $idmenu=$fila->idmenu; 
							   $descripcion=$fila->descripcion;
			?>
		    	<option value="<?=$idmenu?>"><?=$descripcion?></option>
		    <?php
		    	}
		    ?>
		</select>
	</div>
	<!--<div style="margin-top:8px;">
		<?php
			$style="";
			$options="";
			$handler="";

			$panelestudiantes= new grid();
			$panelestudiantes= $panelestudiantes->get_Panel_Html('estudiantes','Estudiantes',$style,$options,$handler,'');
			//echo $panelestudiantes;
		?>
	</div>-->
	<div style="margin-top:10px; heigth:10px;"></div>
	<div style="height:80%;min-height:80%;">
		<?php
			$style="width:100% !important;height:100%;min-height:100%;";
			$dataoptions="view:scrollview,rownumbers:true,singleSelect:true,
				url:'sistema/menus/menu.query.php?idc=1',
				queryParams:{editar:'editarMenuLateral(id)',eliminar:'eliminarMenuLateral(id)'},
				autoRowHeight:false,pageSize:50,loadMsg:'Procesando su requerimiento, espere...'";
			$handler="";
			$sort="";
			$sortname="";

			$campos=array(
				array('campo'=>'descripcion','titulo'=>'Descripcion','width'=>'270','align'=>'left'),
				array('campo'=>'enlace','titulo'=>'Enlace','width'=>'100','align'=>'left'),
				array('campo'=>'url','titulo'=>'Url','width'=>'250','align'=>'left'),
				array('campo'=>'orden','titulo'=>'Orden','width'=>'50','align'=>'left') ,
				array('campo'=>'imagen','titulo'=>'Imagen','width'=>'80','align'=>'left') ,
				array('campo'=>'get2','titulo'=>'Script','width'=>'80','align'=>'left') ,
				array('campo'=>'accion','titulo'=>'Accion','width'=>'120','align'=>'left') ,
				array('campo'=>'acciones','titulo'=>'Acciones','width'=>'80','align'=>'Center') ,
				);

			$gridestudiantes= new grid();
			$gridestudiantes= $gridestudiantes->get_Grid_Html('submenu','SubMenu',$style,$sort,$sortname,$dataoptions,$campos);
			echo $gridestudiantes;
		?>
		
	</div>
</div>
<div id="agregarMenuLateral" class="easyui-window" title="Agregar Menu Lateral" data-options="modal:true,closed:true,closable:true,iconCls:'icon-save',onBeforeClose:function(){  actualizarGrid('#submenu','sistema/menus/menu.query.php?idc='+ $('#slt_menu').combobox('getValue') );$('#agregarSubmenu').html('');  }" style="width:600px;height:550px;padding:10px;">
        <?php
				//include('mantenimientos/estudiantes/crear/index.php');
		?>
</div>
<div id="editarMenuLateral" class="easyui-window" title="Editar Menu Lateral" data-options="modal:true,closed:true,closable:true,iconCls:'icon-save',onBeforeClose:function(){  actualizarGrid('#estudiantes','mantenimientos/estudiantes/estudiantecurso.query.php?idc='+ $('#slt_curso').combobox('getValue') ); $('#editarEstudiante').html('');  }" style="width:600px;height:550px;padding:10px;">
        <?php
				//include('mantenimientos/estudiantes/editar/index.php');
		?>
</div>

<script type="text/javascript">
 $('#slt_menu').combobox({
        onChange: function(value){ 
          	//console.log(value);     
          	actualizarGrid('#submenu','sistema/menus/menu.query.php?idc='+value)
          	
}});


</script>
</div>