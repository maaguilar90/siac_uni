<div style="border: 1px solid #ccc;padding-right: 8px;padding-left: 8px;padding-bottom: 5px;padding-top: 5px;">
	<div>
	  <a onclick="javascript:agregarMenuSuperior()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'">Agregar</a>
	</div> 
</div>
<br>
<div>
	<span class="titulo">Administrador de Menús Superior</span>
</div>
<div>
 
	<!--<div style="margin-top:8px;">
		<?php
			$style="";
			$options="";
			$handler="";

			$panelestudiantes= new grid();
			$panelestudiantes= $panelestudiantes->get_Panel_Html('estudiantes','Estudiantes',$style,$options,$handler,'');
			//echo $panelestudiantes;
		?>
	</div>-->
	<div style="margin-top:10px; heigth:10px;"></div>
	<div style="height:80%;min-height:80%;">
		<?php
			$style="width:100% !important;height:100%;min-height:100%;";
			$dataoptions="view:scrollview,rownumbers:true,singleSelect:true,
				url:'sistema/menus/menusuperior.query.php?idc=1',
				queryParams:{editar:'editarMenuSuperior(id)',eliminar:'eliminarMenuSuperior(id)'},
				autoRowHeight:false,pageSize:50,loadMsg:'Procesando su requerimiento, espere...'";
			$handler="";
			$sort="";
			$sortname="";

			$campos=array(
				array('campo'=>'descripcion','titulo'=>'Descripcion','width'=>'270','align'=>'left'),
				array('campo'=>'enlace','titulo'=>'Enlace','width'=>'100','align'=>'left'),
				array('campo'=>'especial','titulo'=>'Url','width'=>'250','align'=>'left'),
				array('campo'=>'orden','titulo'=>'Orden','width'=>'50','align'=>'left') ,
				array('campo'=>'imagen','titulo'=>'Imagen','width'=>'80','align'=>'left') ,
				array('campo'=>'permiso','titulo'=>'Script','width'=>'80','align'=>'left') ,
				array('campo'=>'accion','titulo'=>'Accion','width'=>'120','align'=>'left') ,
				array('campo'=>'acciones','titulo'=>'Acciones','width'=>'80','align'=>'Center') ,
				);

			$gridestudiantes= new grid();
			$gridestudiantes= $gridestudiantes->get_Grid_Html('menusuperior','Menu Superior',$style,$sort,$sortname,$dataoptions,$campos);
			echo $gridestudiantes;
		?>
		
	</div>
</div>
<?php 
	$nameDiv='';
	$param='';
	$url='sistema/menus/menusuperior.query.php';
	$dataoptions="modal:true,closed:true,closable:true,iconCls:'icon-save',onBeforeClose:function(){  actualizarGrid('#menusuperior',''); }";


?>


<div id="editarMenuSuperior" class="easyui-window" title="Editar Menu Superior" data-options="modal:true,closed:true,closable:true,iconCls:'icon-save',onBeforeClose:function(){  actualizarGrid('#menusuperior','sistema/menus/menusuperior.query.php'); }"></div>


</div>