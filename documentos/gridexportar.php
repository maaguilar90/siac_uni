<?php include ('../Mysqllocal.php'); ?>
<?php
session_start('authcontrol');
@$codusuarioss=$_GET['cdu'];
@$fechainicio=$_GET['fechainicio'];
@$fechafin=$_GET['fechafin'];
@$tiporep=$_GET['tipo'];
?>
<!--
<div style="overflow-x:hidden; overflow-y:scroll; height:430px;">
	<span class="titulo">Proyectos</span><br>
	<div id="errorMessage" class="errorMessage" style="line-height: 15px;"></div>
		<div id="error" class="error" style="display:none;    line-height: 15px;"></div>
		<div id="registrado" style="display:none ;    line-height: 15px; margin-bottom: 10px; padding: 5px 5px 3px; width: 240px;    margin-top: 6px;   border: 1px solid#ccc; background: -webkit-gradient(linear,left top,left bottom,from(#FAFAFA),to(#eaeaea));" >
		</div>-->


	
	<script type="text/javascript">
		$(function(){
			loadLocal();
		});
		function loadLocal(){
			var rows = [];
			for(var i=1; i<=8000; i++){
				var amount = Math.floor(Math.random()*1000);
				var price = Math.floor(Math.random()*1000);
				rows.push({
					inv: 'Inv No '+i,
					date: $.fn.datebox.defaults.formatter(new Date()),
					name: 'Name '+i,
					amount: amount,
					price: price,
					cost: amount*price,
					note: 'Note '+i
				});
			}
			$('#tt').datagrid('loadData', rows);
		}
		function load(mode){
			if (mode == 'local'){
				loadLocal();
			} else {
				$('#tt').datagrid({
					url:'reporteexportar.php'
				});
			}
		}
	</script>
</head>
<body>
	<!--<div style="margin-bottom:10px">
		<select onchange="load(this.value)">
			<option value="remote">Load Remote Data</option>
			<option value="local">Load Local Data</option>
		</select>
	</div>
	-->
	<?php if ($tiporep==0) { ?>
	<table id="tt" class="easyui-datagrid" style="width:100%;height:100%;border:none;" sortName="fechainicio" sortOrder="desc"
			title="Resumen"
			data-options="view:scrollview,rownumbers:true,singleSelect:true,
				url:'proyectos/reporteexportar.php?cdu=<?=$codusuarioss?>&fechainicio=<?=$fechainicio?>&fechafin=<?=$fechafin?>',noheader:true,autoRowHeight:false,pageSize:50,loadMsg:'Procesando su requerimiento, espere...'">
		<thead>
			<tr>

				<th field="nombre" width="200"  sortable="true">Proyecto</th>
				<th field="descripcion" width="200"  sortable="true">Proyecto</th>
				<th field="descripcion" width="200"  sortable="true">Responsable</th>
				<th field="fechainicio" width="100" align="center" sortable="true">Fecha Inicial</th>
				<th field="fechafinal" width="100" align="center" sortable="true">Fecha Final</th>
				<th field="avance" width="120" align="center">Avance</th>
				<th field="nomestatus" width="160" align="center">Estatus</th>
					
			</tr>
		</thead>
	</table>
	<?php } else { ?>

		<table id="tt" class="easyui-datagrid" style="width:100%;height:100%;border:none;" sortName="fechainicio" sortOrder="desc"
			title="Resumen"
			data-options="view:scrollview,rownumbers:true,singleSelect:true,
				url:'proyectos/reporteexportartarea.php?cdu=<?=$codusuarioss?>&fechainicio=<?=$fechainicio?>&fechafin=<?=$fechafin?>',noheader:true,autoRowHeight:false,pageSize:50,loadMsg:'Procesando su requerimiento, espere...'">
		<thead>
			<tr>
				<th field="nombre" width="150"  sortable="true">Tarea</th>
				<th field="nomproyecto" width="150"  sortable="true">Proyecto</th>
				<th field="fechainicio" width="100" align="center" sortable="true">Fecha Inicial</th>
				<th field="fechafinal" width="100" align="center" sortable="true">Fecha Final</th>
				
				<th field="nomusuario" width="120" align="center">Responsable</th>
				<th field="avance" width="120" align="center">Avance</th>		
				<th field="nomestatus" width="160" align="center">Estatus</th>
			</tr>
		</thead>
	</table>

	<?php } ?>

</body>

