
<script type="text/javascript">
    function coloract(id)
    {
        if (id==1)
        {
            $('#anoiniciados').css('background-color','#1461C6');$('#anoiniciados').css('color','white');
            $('#aenproceso').css('background-color','white');$('#aenproceso').css('color','black');
            $('#aatrasados').css('background-color','white');$('#aatrasados').css('color','black');
            $('#atatrasados').css('background-color','white');$('#atatrasados').css('color','black');
            $('#aterminados').css('background-color','white');$('#aterminados').css('color','black');
        }
        
        if (id==2)
        {
            $('#anoiniciados').css('background-color','white');$('#anoiniciados').css('color','black');
            $('#aenproceso').css('background-color','#1461C6');$('#aenproceso').css('color','white');
            $('#aatrasados').css('background-color','white');$('#aatrasados').css('color','black');
            $('#atatrasados').css('background-color','white');$('#atatrasados').css('color','black');
            $('#aterminados').css('background-color','white');$('#aterminados').css('color','black');
        }
        
        if (id==3)
        {
            $('#anoiniciados').css('background-color','white');$('#anoiniciados').css('color','black');
            $('#aenproceso').css('background-color','white');$('#aenproceso').css('color','black');
            $('#aatrasados').css('background-color','#1461C6');$('#aatrasados').css('color','white');
            $('#atatrasados').css('background-color','white');$('#atatrasados').css('color','black');
            $('#aterminados').css('background-color','white');$('#aterminados').css('color','black');
        }
        
        if (id==4)
        {
            $('#anoiniciados').css('background-color','white');$('#anoiniciados').css('color','black');
            $('#aenproceso').css('background-color','white');$('#aenproceso').css('color','black');
            $('#aatrasados').css('background-color','white');$('#aatrasados').css('color','black');
            $('#atatrasados').css('background-color','#1461C6');$('#atatrasados').css('color','white');
            $('#aterminados').css('background-color','white');$('#aterminados').css('color','black');
        }

        if (id==5)
        {
            $('#anoiniciados').css('background-color','white');$('#anoiniciados').css('color','black');
            $('#aenproceso').css('background-color','white');$('#aenproceso').css('color','black');
            $('#aatrasados').css('background-color','white');$('#aatrasados').css('color','black');
            $('#atatrasados').css('background-color','white');$('#atatrasados').css('color','black');
            $('#aterminados').css('background-color','#1461C6');$('#aterminados').css('color','white');
        }

    }

</script>
<div id="ww" style="height:10px;"></div>
<span class="titulo">Gestión de Proyectos</span> <br><br>

<?php
    if ($_SESSION['superadmin']){

        if ($_SESSION['codusuario']==19){
?>

<div style="overflow-x:hidden; overflow-y:scroll; height:89%;max-height:89%;" >
    <div style="margin:0px 0 5px 0;"></div>
    <div class="easyui-accordion" data-options="multiple:true" style="width:97%;height1:300px;">
        <div title="" style="padding:0px; display:none; height:1px !important; padding:0px; border:0px;">  
        </div>
        <div title="Setec" data-options="href:'proyectos/proyectoempresa.php?emp=1'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
        <div title="Global Reinsurance" data-options="href:'proyectos/proyectoempresa.php?emp=2'" style="padding:2px;background: #f3f3f3">
            <p>Conectando...</p>
        </div>
        <div title="National Petroleum Company" data-options="href:'proyectos/proyectoempresa.php?emp=3'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
        <div title="Ricardo Reyes" data-options="href:'proyectos/proyectoempresa.php?emp=4'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
        <div title="Loli Beltrán" data-options="href:'proyectos/proyectoempresa.php?emp=5'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
            <div title="Freddy Maldonado" data-options="href:'proyectos/proyectoempresa.php?emp=6'" style="padding:2px">
         <p>Conectando...</p>
        </div>
        <div title="Ericka Deleg" data-options="href:'proyectos/proyectoempresa.php?emp=7'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
        <div title="Pablo Morales" data-options="href:'proyectos/proyectoempresa.php?emp=8'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
        <div title="Asistente General" data-options="href:'proyectos/proyectoempresa.php?emp=9'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
</div>

<?php 
}else{
?>
<script type="text/javascript">a=0;b=0;</script>
<!--<div style="margin-top: 5px;">
    <a href="javascript:$('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep=1');a=1;coloract(1);" id="anoiniciados" class="easyui-linkbutton" data-options="plain:true,iconCls:''" style="     border: 1px solid #b7d2ff;border-radius: 4px 4px 4px 4px;line-height: 24px;padding: 0; margin: 0 0px;">No iniciados</a>
    <a href="javascript:$('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep=2');a=2;coloract(2);" id="aenproceso" class="easyui-linkbutton" data-options="plain:true,iconCls:''" style="     border: 1px solid #b7d2ff;border-radius: 4px 4px 4px 4px;line-height: 24px;padding: 0; margin: 0 0px;">En proceso</a>
    <a href="javascript:$('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep=7');a=7;coloract(3);" id="aatrasados" class="easyui-linkbutton" data-options="plain:true,iconCls:''" style="     border: 1px solid #b7d2ff;border-radius: 4px 4px 4px 4px;line-height: 24px;padding: 0; margin: 0 0px;">Atrasados</a>
    <a href="javascript:$('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep=10');a=10;coloract(4);" id="atatrasados" class="easyui-linkbutton" data-options="plain:true,iconCls:''" style="     border: 1px solid #b7d2ff;border-radius: 4px 4px 4px 4px;line-height: 24px;padding: 0; margin: 0 0px;">Terminados Atrasados</a>
    <a href="javascript:$('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep=6');a=6;coloract(5);" id="aterminados" class="easyui-linkbutton" data-options="plain:true,iconCls:''" style="     border: 1px solid #b7d2ff;border-radius: 4px 4px 4px 4px;line-height: 24px;padding: 0; margin: 0 0px;">Terminados</a>
</div>-->
<?php
   // if (@$_GET['cdu']){ echo require ('grid.php?cdu='.$_SESSION["codusuario"].'&iep='.$_GET['cdu']); } 
?>

 <div class="easyui-layout" style="width:98%;height:90%;">
       
        <div data-options="region:'west',split:true" title="Usuarios" style="width:209px;">
            <ul id="dg" class="easyui-datalist"  data-options="fit: true,
                  lines: true,
                  url: 'proyectos/listaempleger.php',
                  method: 'get',
                  valueField: 'id_usuario', textField: 'nombrec',textFormatter: function(value){
                      return '<a href=\'javascript:void(0);\' class=\'datalist-link\'>' + value + '</a>';
                  },
                  onClickRow: function(index,row){
                      b=row.id_usuario;
                      cargar_proyger(row.id_usuario,a);
                      
                  }
                   " title="" lines="true" style="width:200px;height:97%">

        
            </ul>

        </div>
     
            <div id="p" class="easyui-panel" title="Proyectos" style="width:70%;height:79%;padding:0px; "
                data-options="
                    tools:[{
                        iconCls:'icon-reload',
                        noheader:true,
                        handler:function(){
                            $('#p').panel('refresh', 'proyectos/grid.php?cdu='+b+'&iep='+a);
                        }
                    }],region:'center'
                ">
        </div>
       
    </div>

<div id="ww" style="height:5px;"></div>
   



<?php
    
}
    }else{
?>

<div style="margin-top: 5px;">
    <a href="javascript:$('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep=1');a=1;coloract(1);" id="anoiniciados" class="easyui-linkbutton" data-options="plain:true,iconCls:''" style="     border: 1px solid #b7d2ff;border-radius: 4px 4px 4px 4px;line-height: 24px;padding: 0; margin: 0 0px;">No iniciados</a>
    <a href="javascript:$('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep=2');a=2;coloract(2);" id="aenproceso" class="easyui-linkbutton" data-options="plain:true,iconCls:''" style="     border: 1px solid #b7d2ff;border-radius: 4px 4px 4px 4px;line-height: 24px;padding: 0; margin: 0 0px;">En proceso</a>
    <a href="javascript:$('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep=7');a=7;coloract(3);" id="aatrasados" class="easyui-linkbutton" data-options="plain:true,iconCls:''" style="     border: 1px solid #b7d2ff;border-radius: 4px 4px 4px 4px;line-height: 24px;padding: 0; margin: 0 0px;">Atrasados</a>
    <a href="javascript:$('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep=10');a=10;coloract(4);" id="atatrasados" class="easyui-linkbutton" data-options="plain:true,iconCls:''" style="     border: 1px solid #b7d2ff;border-radius: 4px 4px 4px 4px;line-height: 24px;padding: 0; margin: 0 0px;">Terminados Atrasados</a>
    <a href="javascript:$('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep=6');a=6;coloract(5);" id="aterminados" class="easyui-linkbutton" data-options="plain:true,iconCls:''" style="     border: 1px solid #b7d2ff;border-radius: 4px 4px 4px 4px;line-height: 24px;padding: 0; margin: 0 0px;">Terminados</a>
</div>
<?php
   // if (@$_GET['cdu']){ echo require ('grid.php?cdu='.$_SESSION["codusuario"].'&iep='.$_GET['cdu']); } 
?>

<div id="ww" style="height:5px;"></div>
    <div id="p" class="easyui-panel" title="Proyectos" style="width:100%;height:79%;padding:0px; "
                data-options="
                    tools:[{
                        iconCls:'icon-reload',
                        noheader:true,
                        handler:function(){
                            $('#p').panel('refresh', 'proyectos/grid.php?cdu=<?=$_SESSION["codusuario"]?>&iep='+a);
                        }
                    }]
                ">
        </div>




<br>
<span style="color: 78A6DB;">
    * Ahora puedes ordenar los campos "Proyecto", "Fecha Inicial", "Fecha Final", ascendente o descendentemente dando clic sobre la columna indicada.
</span>

<!--<div style="overflow-x:hidden; overflow-y:scroll; height:89%;max-height:89%;" >
    <div style="margin:0px 0 10px 0;"></div>
    <div class="easyui-accordion" data-options="multiple:true" style="width:97%;height1:300px;">
        <div title="" style="padding:0px; display:none; height:1px !important; padding:0px; border:0px;">  
        </div>
        <div title="Setec" data-options="href:'proyectos/proyectoempresa.php?emp=1'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
        <div title="Global Reinsurance" data-options="href:'proyectos/proyectoempresa.php?emp=2'" style="padding:2px;background: #f3f3f3">
            <p>Conectando...</p>
        </div>
        <div title="National Petroleum Company" data-options="href:'proyectos/proyectoempresa.php?emp=3'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
        <div title="Ricardo Reyes" data-options="href:'proyectos/proyectoempresa.php?emp=4'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
        <div title="Loli Beltrán" data-options="href:'proyectos/proyectoempresa.php?emp=5'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
            <div title="Freddy Maldonado" data-options="href:'proyectos/proyectoempresa.php?emp=6'" style="padding:2px">
         <p>Conectando...</p>
        </div>
        <div title="Ericka Deleg" data-options="href:'proyectos/proyectoempresa.php?emp=7'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
        <div title="Pablo Morales" data-options="href:'proyectos/proyectoempresa.php?emp=8'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
        <div title="Asistente General" data-options="href:'proyectos/proyectoempresa.php?emp=9'" style="padding:2px;">
            <p>Conectando...</p>
        </div>
</div>-->
<?php
    }
?>

<br>
<span style="color: 78A6DB;">
    * Ahora puedes ordenar los campos "Proyecto", "Fecha Inicial", "Fecha Final", ascendente o descendentemente dando clic sobre la columna indicada.
</span>
