<?php
session_start('authcontrol'); 
@$idproy=$_GET['id'];

$registros=1;
 if ($registros > 0) {
   require_once('../clases/clasesgenerales.class.php');
   require_once '../excel/Classes/PHPExcel.php';
   include_once('../Mysqllocal.php');
  

   @$tipopar= new funciones();
   /*@$fechainicio=$tipopar->formatoFechaYMD($fechainicio);
   @$fechafin=$tipopar->formatoFechaYMD($fechafin);*/
   $objPHPExcel = new PHPExcel();
   $avancep=0;

      $sql="CALL SP_SL_PROYECTOS(".$idproy.",1);";
       $res=executeQuery($sql);
         while($Datos = mysqli_fetch_object($res))
        {
          $idproyecto=$Datos->idproyectos;
          $nombreproyecto=$Datos->nombre;
          $fechainiciop=$Datos->fechainicio;
          $fechafinp=$Datos->fechafinal;
          $descripcionp=$Datos->descripcion;
          $estatusp=$Datos->nomestatus;

          $queryav="CALL SP_SL_AVANCEPROYECTO($idproyecto);";
          $sqlqueryav= executeQuery($queryav,"");
          while($filaav=mysqli_fetch_object($sqlqueryav)){
            $avancep=$filaav->avance;
          }
          if ($avancep == null || $avancep==""){ $avancep=0; } 
        }

       
       $nombresc=$_SESSION['nombres'].' '.$_SESSION['apellidos'];
       //Informacion del excel
       $objPHPExcel->
        getProperties()
            ->setCreator("Sistema Grupo Global")
            ->setLastModifiedBy("Sistema Grupo Global")
            ->setTitle("Reporte de Proyectos")
            ->setSubject("Reporte de Proyectos")
            ->setDescription("Documento generado con PHPExcel")
            ->setKeywords("phpexcel")
            ->setCategory("Proyectos");    

     $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Reporte')
                ->setCellValue('E2', 'Fecha Generado:')
                ->setCellValue('F2', date('d-m-Y h:i:s'))
                ->setCellValue('C4', 'Estatus')
                ->setCellValue('D4', $estatusp)
                ->setCellValue('C5', 'Avance')
                ->setCellValue('D5', $avancep)
                ->setCellValue('A7', 'Proyecto')
                ->setCellValue('A8', $nombreproyecto)
                ->setCellValue('E7', 'Fecha Inicio')
                ->setCellValue('E8', $tipopar->formatoFechaPer($fechainiciop,'/'))
                ->setCellValue('F7', 'Fecha Fin')
                ->setCellValue('F8', $tipopar->formatoFechaPer($fechafinp,'/'))
                ->setCellValue('A8', 'Descripcion')
                ->setCellValue('B8', $descripcionp);

                $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle('C4:D4')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle('C5:D5')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle('A10'.':F10')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle('A7'.':F7')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle('A8'.':F8')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle('A10'.':F10')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("A7:A8")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("C4:C5")->getFont()->setBold(true);

                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B7:D7');
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B8:D8');

                $objPHPExcel->getActiveSheet()->getStyle('C4:C5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('C4:C5')->getFill()->getStartColor()->setRGB('FCD5B4');
                $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setRGB('FCD5B4');
                $objPHPExcel->getActiveSheet()->getStyle('A7:A8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A7:A8')->getFill()->getStartColor()->setRGB('FCD5B4');
                $objPHPExcel->getActiveSheet()->getStyle('E7:F7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('E7:F7')->getFill()->getStartColor()->setRGB('FCD5B4');

                $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $i = 10;
        $queryTarea="CALL SP_SL_TAREA(0,".$idproyecto.",0,1);";
        $resultTarea=executeQuery($queryTarea);
        while($DatosTareas = mysqli_fetch_object($resultTarea))
        {
           $idtarea=$DatosTareas->idtarea;
           $nombretarea=$DatosTareas->nombre;
           $fechainiciot=$DatosTareas->fechainicio;
           $fechafint=$DatosTareas->fechafinal;
           $estatustarea=$DatosTareas->nomestatus;
           $avancetarea=$DatosTareas->avance;
           $responsabletarea=$DatosTareas->nombreu;
           if ($avancetarea == null || $avancetarea==""){ $avancetarea=0; } 

               $objPHPExcel->setActiveSheetIndex(0)
               ->setCellValue('A'.$i, 'Tareas')
                ->setCellValue('B'.$i, 'Fecha Inicio')
                ->setCellValue('C'.$i, 'Fecha Fin')
                ->setCellValue('D'.$i, 'Estatus')
                ->setCellValue('E'.$i, 'Avance')
                ->setCellValue('F'.$i, 'Asignado');

                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                 $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getFill()->getStartColor()->setRGB('FCD5B4');
                $i++;
             $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $nombretarea)
                ->setCellValue('B'.$i, $tipopar->formatoFechaPer($fechainiciot,'/'))
                ->setCellValue('C'.$i, $tipopar->formatoFechaPer($fechafint,'/'))
                ->setCellValue('D'.$i, $estatustarea)
                ->setCellValue('E'.$i, $avancetarea)
                ->setCellValue('F'.$i, $responsabletarea);
               
               // ->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);    
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $i++;
            $contact=0;
            $queryactividad="CALL SP_SL_ACTIVIDADES(".$idtarea.",0,1,1);";
            $resultactividad=executeQuery($queryactividad);
            while($DatosActividad = mysqli_fetch_object($resultactividad))
            {
              $contact++;
              if ($contact==1)
                {
                     $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, 'Actividad')
                ->setCellValue('B'.$i, 'Descripción')
                ->setCellValue('C'.$i, 'Avance');
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                 $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getFill()->getStartColor()->setRGB('FDE9D9');
 
                $i++;

                }

              $idactividad=$DatosActividad->idactividad;
              $nombreact=$DatosActividad->nombre;
              $descripcionact=$DatosActividad->descripcion;
              $avanceactividad=$DatosActividad->avance;
              if ($avanceactividad == null || $avanceactividad==""){ $avanceactividad=0; } 

                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $nombreact)
                ->setCellValue('B'.$i, $descripcionact)
                ->setCellValue('C'.$i, $avanceactividad);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                
  
              $i++;
            }
        }

      // while ($registro = mysql_fetch_object ($resultado)) {
           
        //  $objPHPExcel->setActiveSheetIndex(0)
          //      ->setCellValue('A'.$i, 'Proyectos');
     
         // $i++;
          
       //}
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('15');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('15');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('15');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('15');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('18');


   //7 for ($col = 'A'; $col != 'B'; $col++) {
     ///$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
   // }
    // for ($col = 'C'; $col != 'G'; $col++) {
     //$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
    //}
    /*  $objPHPExcel->getActiveSheet()->getStyle("A1:A2")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $objPHPExcel->getActiveSheet()->getStyle('A2:B2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

      $objPHPExcel->getActiveSheet()->getStyle('A1:A2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A1:A2')->getFill()->getStartColor()->setARGB('FCD5B4');

      $objPHPExcel->getActiveSheet()->getStyle("C1")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('C1')->getFill()->getStartColor()->setARGB('fde9d9');
      $objPHPExcel->getActiveSheet()->getStyle('C1:D1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);  

      $objPHPExcel->getActiveSheet()->getStyle("C2")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('C2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('C2')->getFill()->getStartColor()->setARGB('fde9d9');
      $objPHPExcel->getActiveSheet()->getStyle('C2:D2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);  


      $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:H4');
      $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFill()->getStartColor()->setARGB('fde9d9');
      $objPHPExcel->getActiveSheet()->getStyle("A4:H4")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                     // $objPHPExcel->getActiveSheet()->setCellValue('A4', $this->lang->line('history_excel_title'));
      $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A5:H5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A5:H5')->getFill()->getStartColor()->setARGB('fde9d9');
      // Add some data
      $objPHPExcel->getActiveSheet()->getStyle("A5:H5")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A5:H5')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    /*$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
    var_dump($sheetData);*/

  
   }
   

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Reporteproyectos.xls"');
// if (@$tiporep==1){ header('Content-Disposition: attachment;filename="Reportestareas.xlsx"'); }
header('Cache-Control: max-age=0');

//$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
//ob_start();
$objWriter->save('php://output');
exit;
//$xlsData = ob_get_contents();
//ob_end_clean();
//exit;

/*$response =  array(
        'op' => 'ok',
        'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
    );

die(json_encode($response));*/

//mysql_close ();



?>