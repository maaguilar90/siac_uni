<?php include ('../../Mysqllocal.php'); ?>
<?php
	session_start('authcontrol');
	@$codusuarioss=$_GET['cdu'];
	@$idestatusp=$_GET['iep'];

	$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'itemid';
	$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';

	if (!$sort)
	{
		$sort='fechainicio';
	}

	if (!$order)
	{
		$order='ASC';
	}

	$query="CALL SP_SL_TAREAS_AS_ORDEN(".$codusuarioss.",".$idestatusp.",'".$sort."','".$order."');";


	$i=0;
	$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;

	$items = array();
	date_default_timezone_set('UTC');

	$sqlquery= executeQuery($query,"");
	$cont=0;
	while($fila=mysqli_fetch_object($sqlquery)){
  		           $cont=$cont+1;
  		           $i++;
		           $idtarea=$fila->idtarea; 
		           $idproyecto=$fila->idproyecto; 
				   $nombre=$fila->nombre;
				   $nomproyecto=$fila->nomproyecto;
				   $nomusuario=$fila->nomusuario;
				   $idusuarioc=$fila->idusuarioc;
				   $fechacreacion=$fila->fechacreacion;
				   $descripcion=$fila->descripcion;
				   $fechainicial=$fila->fechainicio;
				   $fechafinal=$fila->fechafinal;
				   $horainicial=$fila->horainicial;
				   $horafinal=$fila->horafinal;
				   $tiempo=$fila->tiempo;
				   $tiempocat=$fila->tiempocat;
				   $avance=$fila->avance;
				   $nombreestatus=$fila->nomestatus;
				   $colorestatus=$fila->colorestatus;
				   if ($avance=="") { $avance=0; }
				   //$fechanac=str_replace("-", "/", $fechanac);
					$diafechaf= substr($fechafinal, 8,2);
					$mesfechaf= substr($fechafinal, 5,2);
					$aniofechaf= substr($fechafinal, 0,4);

					$diafechai= substr($fechainicial, 8,2);
					$mesfechai= substr($fechainicial, 5,2);
					$aniofechai= substr($fechainicial, 0,4);

					$diafechac= substr($fechacreacion, 8,2);
					$mesfechac= substr($fechacreacion, 5,2);
					$aniofechac= substr($fechacreacion, 0,4);
					
					switch($mesfechac){
						    case "01": $dia_textoc = "Ene"; break;
						    case "02": $dia_textoc = "Feb"; break;
						    case "03": $dia_textoc = "Mar"; break;
						    case "04": $dia_textoc = "Abr"; break;
						    case "05": $dia_textoc = "May"; break;
						    case "06": $dia_textoc = "Jun"; break;
						    case "07": $dia_textoc = "Jul"; break;
						    case "08": $dia_textoc = "Ago"; break;
						    case "09": $dia_textoc = "Sep"; break;
						    case "10": $dia_textoc = "Oct"; break;
						    case "11": $dia_textoc = "Nov"; break;
						    case "12": $dia_textoc = "Dic"; break;
						    default: $dia_textoc = "-";    
					}
					switch($mesfechaf){
					    case "01": $dia_texto = "Ene"; break;
					    case "02": $dia_texto = "Feb"; break;
					    case "03": $dia_texto = "Mar"; break;
					    case "04": $dia_texto = "Abr"; break;
					    case "05": $dia_texto = "May"; break;
					    case "06": $dia_texto = "Jun"; break;
					    case "07": $dia_texto = "Jul"; break;
					    case "08": $dia_texto = "Ago"; break;
					    case "09": $dia_texto = "Sep"; break;
					    case "10": $dia_texto = "Oct"; break;
					    case "11": $dia_texto = "Nov"; break;
					    case "12": $dia_texto = "Dic"; break;
					    default: $dia_texto = "-";    
					}
					switch($mesfechai){
					    case "01": $mes_texto = "Ene"; break;
					    case "02": $mes_texto = "Feb"; break;
					    case "03": $mes_texto = "Mar"; break;
					    case "04": $mes_texto = "Abr"; break;
					    case "05": $mes_texto = "May"; break;
					    case "06": $mes_texto = "Jun"; break;
					    case "07": $mes_texto = "Jul"; break;
					    case "08": $mes_texto = "Ago"; break;
					    case "09": $mes_texto = "Sep"; break;
					    case "10": $mes_texto = "Oct"; break;
					    case "11": $mes_texto = "Nov"; break;
					    case "12": $mes_texto = "Dic"; break;
					    default: $mes_texto = "-";    
					} 
					 
					$nombremesfechaf= $dia_texto;
					$fechafinalm=$diafechaf." ".$nombremesfechaf." ".$aniofechaf;

					$nombremesfechai= $mes_texto;
					$fechainicialm=$diafechai." ".$nombremesfechai." ".$aniofechai;

					$nombremesfechac= $dia_textoc;
					$fechac=$diafechac." ".$nombremesfechac." ".$aniofechac;


		if ($avance == null || $avance==""){ $avance=0; }

		//$avance=50;

		if ($avance>=0 and $avance<=25){ $color='#E04548'; }
		if ($avance>=26 and $avance<=50){ $color='#EDDA49'; }
		if ($avance>=51 and $avance<=80){ $color='#D6A242'; }
		if ($avance>=81 and $avance<=100){ $color='#6ACE40'; }				



		//$barraprogreso='<div id="p'.$idtarea.'" class="easyui-progressbar" data-options="" style="width:100%;height:17px;  "></div> ';

		$barraprogreso= '<div id="p" class="easyui-progressbar progressbar" style="width: 111px;; height: 15px;"><div class="progressbar-text" style="width: 111px; height: 15px; line-height: 15px;">'.$avance.'%</div><div class="progressbar-value" style="width: '.$avance.'%; height: 15px; line-height: 15px;"><div class="progressbar-text" style="width: 111px;; height: 15px; line-height: 15px;     background-color: '.$color.';">'.$avance.'%</div></div></div>';

		//$barraprogreso=$barraprogreso."<script> $('#p".$idtarea."').progressbar({ value: ".$avance.", background: '".$color."' });  </script> ";
		//$barraprogreso=$barraprogreso.'<script> $("#p".$idtarea")'.".attr('class', 'newClass'); </script> ";
	//	$barraprogreso=$barraprogreso."<script> $('.progressbar-value').css('background-color', '".$color."');  </script> ";
	//	$barraprogreso=$barraprogreso."<script> $('.progressbar-text').css('background-color', 'transparent');  </script> ";


		$botones="";
		//$colorestatus	='red';
		$estatus='<span style="color:'.$colorestatus.';font-weight:bold;">'.$nombreestatus.'</span>';

		/*function abc($file){
		    ob_start();
		    require($file);
		    return ob_get_clean();
		}*/
		//$mensajeslib=require("mensajes/index.php");
		if ($_SESSION['codusuario']!=19)
		{
			//$botones=$botones.'<form id="ff'.$idtarea.'" name="ff'.$idtarea.'" style="    margin-bottom: 0px;" action="proyectos/actualizar/index.php" method="post" enctype="multipart/form-data">';	
			$botones=$botones.'<img src="images/ver.png" style="margin-right:5px; " width="16" height="16" title="Agregar Actividades" onclick="javascript:ir_inter_tarea('.$idtarea.');" />';	
		//	$botones=$botones.'<a href="#modal'.$idtarea.'"><img src="images/mensaje.png" style="margin-right:5px; "  height="16" title="Agregar Mensaje" onclick="javascript:ir_mensaje_tarea('.$idtarea.');" /></a>';
	//		$botones=$botones.'<div id="modal'.$idtarea.'" class="modalmask2"><div class="modalbox2 movedown"><div style="position: absolute;width: 100%;padding: 0px; */" class="barracentro verdeamarela"><div style="float:left; padding:5px; ">Agregar Mensaje</div><a href="#close" style="  margin-right: 2px;margin-top:2px;" onclick="javascript:;" title="Close" class="close">X</a></div><div style="margin-top:23px;">'.$mensajeslib.'</div></div></div>';
			//$botones=$botones.'<img src="images/elim.png" style="float:;" width="16" height="16" style="margin-left:15px;position: fixed;" title="Eliminar Proyecto" onclick="javascript:eliminar_proyecto('.$idtarea.');" />';

			//$botones=$botones.' <script type="text/javascript"> ';
			//$botones=$botones.'$(function(){ $(\'#ff'.$idtarea.'\').form({ success:function(data){ var m = \'\'; $.messager.alert(\'Información del Proyecto\', m+data).window({ width:820,left:\'25%\' }); } }); }); $( "#btnsubmit'.$idtarea.'" ).click(function() { $( "#ff'.$idtarea.'" ).submit(); }); </script>';

			//$botones=$botones.'</form>';	
							
							
						
						/*
							$(function(){ $('#ff<?=$idproyecto?>').form({ success:function(data){ var m = ''; $.messager.alert('Información del Proyecto', m+data).window({ width:820,left:'25%' }); }); }); $( \"#btnsubmit<?=$idproyecto?>\" ).click(function() { $( \"#ff<?=$idproyecto?>\" ).submit(); }); </script> </form>";
*/		}

		$tarnom=$nombre;
		$nombre='<div id="tittarea'.$idtarea.'" title="'.$nombre.'" class="easyui-tooltip">'.$nombre.'</div>';
		$nombre=$nombre.' <script type="text/javascript"> $(function(){ $(\'#tittarea'.$idtarea.'\').tooltip({ position: \'bottom\', content: \'<span style="padding:5px;color:#000">'.$tarnom.'</span>\',  }) }); </script>';
	
		$tarproy=$nomproyecto;
		$nomproyecto='<div id="titproy'.$idtarea.'" title="'.$nomproyecto.'" class="easyui-tooltip">'.$nomproyecto.'</div>';
		$nomproyecto=$nomproyecto.' <script type="text/javascript"> $(function(){ $(\'#titproy'.$idtarea.'\').tooltip({ position: \'bottom\', content: \'<span style="padding:5px;color:#000">'.$tarproy.'</span>\',  }) }); </script>';


		//$i++;
		$index = $i+($page-1)*$rows;
		$amount = rand(50,100);
		$price = rand(10000,20000)/100;
		$items[] = array(
			'nombre' => $nombre,
			'nomproyecto' => $nomproyecto,
			'fechainicio' => $fechainicialm,
			'fechafinal' => $fechafinalm,
			'nomestatus' => $estatus,
			'nomusuario' => $nomusuario,
			'avance' => $barraprogreso,
			'acciones' => $botones
		);
	
	}

	$result = array();
	$result['total'] = $i;
	$result['rows'] = $items;
	//$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'itemid';
	//$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
	echo json_encode($result);



?>