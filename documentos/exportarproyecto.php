<?php
session_start('authcontrol'); 
@$idproy=$_GET['id'];
@$sort = 'itemid';
@$order = 'asc';




$registros=1;
 if ($registros > 0) {
   require_once('../clases/clasesgenerales.class.php');
   require_once '../excel/Classes/PHPExcel.php';
   include_once('../Mysqllocal.php');
  

   @$tipopar= new funciones();
   @$fechainicio=$tipopar->formatoFechaYMD($fechainicio);
   @$fechafin=$tipopar->formatoFechaYMD($fechafin);
   $objPHPExcel = new PHPExcel();

      $sql="CALL SP_SL_PROYECTOS(".$idproy.",1);";
       $res=executeQuery($sql);

       $nombresc=$_SESSION['nombres'].' '.$_SESSION['apellidos'];
       //Informacion del excel
       $objPHPExcel->
        getProperties()
            ->setCreator("Sistema Grupo Global")
            ->setLastModifiedBy("Sistema Grupo Global")
            ->setTitle("Reporte de Proyectos")
            ->setSubject("Reporte de Proyectos")
            ->setDescription("Documento generado con PHPExcel")
            ->setKeywords("phpexcel")
            ->setCategory("Proyectos");    

     $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', $nombresc)
                ->setCellValue('A2', 'Empresa')
                ->setCellValue('B2', $_SESSION['empresa'])
                ->setCellValue('C1', 'Desde - Hasta')
                ->setCellValue('D1', $tipopar->formatoFechaPer($fechainicio,'/').' - '.$tipopar->formatoFechaPer($fechafin,'/'))
                ->setCellValue('C2', 'Fecha Generado:')
                ->setCellValue('D2', date('d-m-Y h:i:s'))

                ->setCellValue('A4', 'Reporte Consolidado')
                ->setCellValue('A5', 'Proyecto')
                ->setCellValue('B5', 'Tema')
                ->setCellValue('C5', 'Actividad')
                ->setCellValue('D5', 'Responsable')
                ->setCellValue('E5', 'Fecha Inicio')
                ->setCellValue('F5', 'Fecha Fin')
                ->setCellValue('G5', 'Avance')
                ->setCellValue('H5', 'Estatus')
              /*  ->setCellValue('H5', 'Observaciones')
                 ->setCellValue('I5', 'Pendientes')*/;


       $i = 6;
        while($Datos = mysqli_fetch_object($res))
        {
           $idproyecto=$Datos->idproyectos;
            $queryav="CALL SP_SL_AVANCEPROYECTO($idproyecto);";
            $sqlqueryav= executeQuery($queryav,"");
              while($filaav=mysqli_fetch_object($sqlqueryav)){
                $avance=$filaav->avance;
              }
              if ($avance == null || $avance==""){ $avance=0; }

             $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $Datos->nombre)
                ->setCellValue('B'.$i, $Datos->descripcion)
                ->setCellValue('C'.$i, $Datos->nombre)
                ->setCellValue('D'.$i, $_SESSION['nameusuario'])
                ->setCellValue('E'.$i, $tipopar->formatoFechaPer($Datos->fechainicio,'/'))
                ->setCellValue('F'.$i, $tipopar->formatoFechaPer($Datos->fechafinal,'/'))
                ->setCellValue('G'.$i, $avance.'%')
                ->setCellValue('H'.$i, $Datos->nomestatus)
                ->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
               $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
               $i++;
        }

      // while ($registro = mysql_fetch_object ($resultado)) {
           
        //  $objPHPExcel->setActiveSheetIndex(0)
          //      ->setCellValue('A'.$i, 'Proyectos');
     
         // $i++;
          
       //}
    

    for ($col = 'A'; $col != 'J'; $col++) {
     $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
    }
      $objPHPExcel->getActiveSheet()->getStyle("A1:A2")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $objPHPExcel->getActiveSheet()->getStyle('A2:B2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

      $objPHPExcel->getActiveSheet()->getStyle('A1:A2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A1:A2')->getFill()->getStartColor()->setARGB('fde9d9');

      $objPHPExcel->getActiveSheet()->getStyle("C1")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('C1')->getFill()->getStartColor()->setARGB('fde9d9');
      $objPHPExcel->getActiveSheet()->getStyle('C1:D1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);  

      $objPHPExcel->getActiveSheet()->getStyle("C2")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('C2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('C2')->getFill()->getStartColor()->setARGB('fde9d9');
      $objPHPExcel->getActiveSheet()->getStyle('C2:D2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);  


      $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:H4');
      $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFill()->getStartColor()->setARGB('fde9d9');
      $objPHPExcel->getActiveSheet()->getStyle("A4:H4")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                     // $objPHPExcel->getActiveSheet()->setCellValue('A4', $this->lang->line('history_excel_title'));
      $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A5:H5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $objPHPExcel->getActiveSheet()->getStyle('A5:H5')->getFill()->getStartColor()->setARGB('fde9d9');
      // Add some data
      $objPHPExcel->getActiveSheet()->getStyle("A5:H5")->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A5:H5')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    /*$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
    var_dump($sheetData);*/

  
   }
   

header('Content-Type: application/vnd.ms-excel');
 if (@$tiporep==0){ header('Content-Disposition: attachment;filename="Reporteproyectos.xlsx"'); }
 if (@$tiporep==1){ header('Content-Disposition: attachment;filename="Reportestareas.xlsx"'); }
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
//ob_start();
$objWriter->save('php://output');
exit;
//$xlsData = ob_get_contents();
//ob_end_clean();
//exit;

/*$response =  array(
        'op' => 'ok',
        'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
    );

die(json_encode($response));*/

//mysql_close ();



?>