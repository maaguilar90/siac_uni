<head>

</head>

<div>
	
	<div>
		<div id="errorMessage" class="errorMessage" style="line-height: 15px;"></div>
		<div id="error" class="error" style="display:none;    line-height: 15px;"></div>
		<div id="registrado" style="display:none ;    line-height: 15px; margin-bottom: 10px; padding: 5px 5px 3px; width: 240px;    margin-top: 6px;   border: 1px solid#ccc; background: -webkit-gradient(linear,left top,left bottom,from(#FAFAFA),to(#eaeaea));" >
		</div>
		<div style="line-height: 25px;  float: left; padding-right: 30px;">
			<div>
				<span class="titulo">Agregar Alarma</span>
			</div>
			<table style="    ">
				<tr>
					<td>
						<span style="font-weight:bold;">Proyecto</span>
					</td>
					<td>
						<span style="font-weight:bold;">Periodo</span>
					</td>
					<td>
						<span style="font-weight:bold;">%</span>
					</td>
					<td>
						<span style="font-weight:bold;">Usuario</span>
					</td>
					<td>
						<span style="font-weight:bold;">Acción</span>
					</td>
				</tr>
				<tr>
					<td>
						<select id="sltproyecto" name="sltproyecto">
							<?php
									if ($_SESSION['codusuario']==19) 
									{
									$query="CALL SP_SL_PROYECTOS(0,1);";
									}else{
									$query="CALL SP_SL_PROYECTOSCR(".$_SESSION['codusuario'].",0);";
									}
								//$query="CALL SP_SL_PROYECTOS(0,1);";
								$sqlquery= executeQuery($query,"");
								$cont=0;
									while($fila=mysqli_fetch_object($sqlquery))
									{
								           $cont=$cont+1;
								           $idproyecto=$fila->idproyectos; 
										   $nombre=$fila->nombre;
										   $idusuarioc=$fila->idusuarioc;
										   $fechacreacion=$fila->fechacreacion;
										   $descripcion=$fila->descripcion;
										   $fechainicial=$fila->fechainicio;
										   $fechafinal=$fila->fechafinal;
										   $horainicial=$fila->horainicial;
										   $horafinal=$fila->horafinal;
										   $tiempo=$fila->tiempo;
										   $tiempocat=$fila->tiempocat;
										   $estatus=$fila->nomestatus;
										   $colorestatus=$fila->colorestatus;
										   //$fechanac=str_replace("-", "/", $fechanac);
									?>
										<option value="<?=$idproyecto?>"><?=$nombre?></option>
									<?php	   
									}
							?>
						</select>
					</td>
					<td>
						<span style="font-weight:bold;"></span>
						<select id="sltperiodop">
							
							<option value="0">Al Inicio</option>
							<option value="1">Al Porcentaje</option>
							<option value="2">Al Terminar</option>
							<option value="3">Al 50% de Avance</option>
							<option value="4">Al Atraso</option>
							
						</select>
					</td>
					<td>
						<input type="number" size="5" min="1" max="100" style="width:45px;"  id="txtporcentajep" name="txtporcentajep"/>
					</td>
					<td>
						<select id="sltasig1" disabled>
							<?php
								$query="CALL SL_USUARIO(1);";
								$sqlquery= executeQuery($query,"");
								
								$cont=0;
									while($fila=mysqli_fetch_object($sqlquery)){
								           $cont=$cont+1;
								           $idusuario=$fila->id_usuario; 
										   $usuario=$fila->usuario;
										   $nombres=$fila->nombres;
										   $apellidos=$fila->apellidos;
										   $correos=$fila->correo;
										   $roldes=$fila->descripcion;
										   $rol=$fila->id_rol;
										   //$optioncont=$optioncont."<option value=\"$idusuario\">$usuario</option>";
							?>
								<option value="<?=$idusuario?>" <?php if ($_SESSION['codusuario']==$idusuario) echo 'selected="selected"' ?>  ><?=$usuario?></option>
							<?php
								}
							?>
						</select>											
					</td>
					<td style="padding-left:10px;">
						<img src="images/guardar.png" title="Guadar y Crear Alarma en Proyecto" onclick="javascript:crear_alarma(1);" style="padding-right: 10px;" />
					</td>
				</tr>
			</table>

			<table style="    font-size: 12px;font-family: sans-serif; margin-top:5px;">
				<tr>
					<td>
						<span style="font-weight:bold;">Tarea</span>
					</td>
					<td>
						<span style="font-weight:bold;">Periodo</span>
					</td>
					<td>
						<span style="font-weight:bold;">%</span>
					</td>
					<td>
						<span style="font-weight:bold;">Usuario</span>
					</td>
					<td>
						<span style="font-weight:bold;">Acción</span>
						
					</td>
				</tr>
				<tr>
					<td>
						<select id="slttarea" name="slttarea">
							<?php
								if ($_SESSION['codusuario']==19) 
								{
								$query="CALL SP_SL_TAREAS(0,0,0,1);";
								}else{
								$query="CALL SP_SL_TAREAS_AS(".$_SESSION['codusuario'].",1);";
								}
								
								$sqlquery= executeQuery($query,"");
								$cont=0;
									while($fila=mysqli_fetch_object($sqlquery))
									{
								           $cont=$cont+1;
								           $idtarea=$fila->idtarea; 
								           $idproyecto=$fila->idproyecto; 
										   $nombre=$fila->nombre;
										   $idusuarioc=$fila->idusuarioc;
										   $fechacreacion=$fila->fechacreacion;
										   $descripcion=$fila->descripcion;
										   $fechainicial=$fila->fechainicio;
										   $fechafinal=$fila->fechafinal;
										   $horainicial=$fila->horainicial;
										   $horafinal=$fila->horafinal;
										   $tiempo=$fila->tiempo;
										   $tiempocat=$fila->tiempocat;
										   $avance=$fila->avance;
										   $nombreestatus=$fila->nomestatus;
									?>
										<option value="<?=$idtarea?>"><?=$nombre?></option>
									<?php	   
									}
							?>
						</select>
					</td>
					<td>
						<span style="font-weight:bold;"></span>
						<select id="sltperiodot">
							<option value="0">Al Inicio</option>
							<option value="1">Al Porcentaje</option>
							<option value="2">Al Terminar</option>
							<option value="3">Al 50% de Avance</option>
							<option value="4">Al Atraso</option>
						</select>
					</td>
					<td>
						<input type="number" size="5" min="1" max="100" style="width:45px;"  id="txtporcentajet" name="txtporcentajet"/>
					</td>
					<td>
						<select id="sltasig2"  disabled>
							<?php
								$query="CALL SL_USUARIO(1);";
								$sqlquery= executeQuery($query,"");
								
								$cont=0;
									while($fila=mysqli_fetch_object($sqlquery)){
								           $cont=$cont+1;
								           $idusuario=$fila->id_usuario; 
										   $usuario=$fila->usuario;
										   $nombres=$fila->nombres;
										   $apellidos=$fila->apellidos;
										   $correos=$fila->correo;
										   $roldes=$fila->descripcion;
										   $rol=$fila->id_rol;
										   $optioncont=$optioncont."<option value=\"$idusuario\">$usuario</option>";
							?>
								<option value="<?=$idusuario?>" <?php if ($_SESSION['codusuario']==$idusuario) echo 'selected="selected"' ?>><?=$usuario?></option>
							<?php
								}
							?>
						</select>											
					</td>
					<td style="padding-left:10px;">
						<img src="images/guardar.png" title="Guadar y Crear Alarma en Tarea" onclick="javascript:crear_alarma(2);" style="padding-right: 10px;" />
					</td>
				</tr>
			</table>

		</div>
		<div id="dvbusqueda" style="float: left; padding-left: 20px; border-left: 1px dashed #ccc; height: 80%; display: none; line-height: 25px;">
			<div>
				<span class="titulo">Búsqueda</span>
			</div>
			<div style="padding-top: 5px;">
				<span>Nombre de la alarma</span>
				<input type="text" size="25" />
				
			</div>
			<div id="dvbusquedacont" style="padding-top: 5px;">
				<span class="titulo" style="color: #666;">Resultado de búsqueda</span>
				<div id="dvresultadobus">
					
				</div>
			</div>
		</div>
	</div>	
</div>