<!--<div style="border: 1px solid #ccc; padding-top: 5px;padding-right: 8px;padding-left: 8px;padding-bottom: 5px;">
	<?php include ('botones.php'); ?>
</div>-->
 <!--
<div>
	<span class="titulo">--------- Proyectos -----------</span><br>
	<?php
		$query="CALL SP_SL_PROYECTOSCR(".$_SESSION['codusuario'].",1);";
		$sqlquery= executeQuery($query,"");
		$cont=0;
			while($fila=mysqli_fetch_object($sqlquery)){
		           $cont=$cont+1;
		           $idproyecto=$fila->idproyectos; 
				   $nombre=$fila->nombre;
				   $idusuarioc=$fila->idusuarioc;
				   $fechacreacion=$fila->fechacreacion;
				   $descripcion=$fila->descripcion;
				   $fechainicial=$fila->fechainicio;
				   $fechafinal=$fila->fechafinal;
				   $horainicial=$fila->horainicial;
				   $horafinal=$fila->horafinal;
				   $tiempo=$fila->tiempo;
				   $tiempocat=$fila->tiempocat;
				   //$fechanac=str_replace("-", "/", $fechanac);
	?>
			<div style="height:45px; padding-top:5px;">
				<div style="padding: 5px; width: 250px;   border: 1px solid#ccc; background: -webkit-gradient(linear,left top,left bottom,from(#FAFAFA),to(#eaeaea));" >
					<span style="font-weight:bold;">
						<?=$nombre?>
					</span>
					<br>
					<span style="font-weight:;">
						Estatus: 
					</span>
					<span style="color:green;font-weight:bold;">
						Nuevo 
					</span>-->
					<!--<img src="images/agregar.png" width="16" height="16" style="margin-left:10px;position: fixed;" title="Agregar Tareas" onclick="javascript:ir_agregar_tarea(2);" />-->
					<!--<<img src="images/ver.png" style="float:right;margin-right:5px; " width="16" height="16" title="Ver tareas del Proyecto" onclick="javascript:ir_agregar_tarea(<?=$idproyecto?>);" />
				</div>
			</div>
			<?php
			}

			if ($cont==0)
			{
				?>
					<div>
						No tienes ningún proyectos Creados.
					</div>
				<?php
			} 
		?>
	
	<br><br>-->
	<span class="titulo">Tareas Asignados</span><br>
	<div style="overflow-x:hidden; overflow-y:scroll; height:500px;" >
	<table style="margin-top:10px;">
				<tr>
					<td>
						<span style="font-weight:bold;">Tarea</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Proyecto</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Creador</span>	
					</td>
					<td>
						<span style="font-weight:bold;">Fecha Fin </span>	
					</td>
					<td>
						<span style="font-weight:bold;">Estatus </span>
					</td>
					<td>
						<span style="font-weight: bold; color:;">Avance </span>
					</td>

					<td>
						<span style="font-weight: bold; color:#666;">Acciones </span>
					</td>
				</tr>
	<?php
		$query="CALL SP_SL_TAREAS_AS(".$_SESSION['codusuario'].",1);";
		$sqlquery= executeQuery($query,"");
		$cont=0;
			while($fila=mysqli_fetch_object($sqlquery)){
		           $cont=$cont+1;
		           $idtarea=$fila->idtarea; 
		           $idproyecto=$fila->idproyecto; 
				   $nombre=$fila->nombre;
				   $nomproyecto=$fila->nomproyecto;
				   $nomusuario=$fila->nomusuario;
				   $idusuarioc=$fila->idusuarioc;
				   $fechacreacion=$fila->fechacreacion;
				   $descripcion=$fila->descripcion;
				   $fechainicial=$fila->fechainicio;
				   $fechafinal=$fila->fechafinal;
				   $horainicial=$fila->horainicial;
				   $horafinal=$fila->horafinal;
				   $tiempo=$fila->tiempo;
				   $tiempocat=$fila->tiempocat;
				   $avance=$fila->avance;
				   $nombreestatus=$fila->nomestatus;
				   if ($avance=="") { $avance=0; }
				   //$fechanac=str_replace("-", "/", $fechanac);
			?>
				
					<tr>
						<td>
							<span style=""><?=$nombre?></span>
						</td>
						<td>
							<span style=""><?=$nomproyecto?></span>
						</td>
						<td>
							<span style=""><?=$nomusuario?></span>
						</td>
						<td>
							<span style="color:;font-weight:;"><?=$fechafinal?></span>
						</td>
						<td>
							<span style="color:<?php if ($nombreestatus=="ATRASADA") { echo 'red'; }else{ if ($nombreestatus=="TERMINADO") { echo 'gray'; }else { echo 'green'; }  } ?>;font-weight:bold;"><?=$nombreestatus?></span>
						</td>	
						<td>
							<?php
							//	$avance=0;
							$posicion="-118";
							if ($avance==0)	{ $posicion="-118"; }
							if ($avance==100) {	$posicion="0"; }
							if ($avance>1 and $avance<100) { $posicion=$posicion+($avance*1.18); }	
		
							if ($avance>=0 and $avance<=25){ $barra='progressbg_red.gif'; }
							if ($avance>=26 and $avance<=50){ $barra='progressbg_orange.gif'; }
							if ($avance>=51 and $avance<=80){ $barra='progressbg_yellow.gif'; }
							if ($avance>=81 and $avance<=100){ $barra='progressbg_green.gif'; }					
							
							?> 	
							<img src="images/progressbar.gif" width="120" height="12" style="background-image: url(images/<?=$barra?>);   background-position: <?=$posicion?>px 50%;" title="" />
							<span style="font-weight:;">
								<?=$avance?>% 
							</span>
						</td>

						<td style="padding-left:10px;">
							<img src="images/ver.png" style="margin-right:5px; " width="16" height="16" title="Agregar Actividades" onclick="javascript:ir_inter_tarea(<?=$idtarea?>);" />
							<a href="#modal<?=$idtarea?>"><img src="images/mensaje.png" style="margin-right:5px; "  height="16" title="Agregar Mensaje" onclick="javascript:ir_mensaje_tarea(<?=$idtarea?>);" /></a>
						</td>
						<div id="modal<?=$idtarea?>" class="modalmask2">
					<div class="modalbox2 movedown">
						<div style="position: absolute;width: 100%;padding: 0px; */" class="barracentro verdeamarela">
								<div style="float:left; padding:5px; ">Agregar Mensaje</div>
								<a href="#close" style="  margin-right: 2px;margin-top:2px;" onclick="javascript:;" title="Close" class="close">X</a>
							</div>
							<div style="margin-top:23px;">
							<?php
								require 'proyectos/tareas/mensajes/index.php';
							?>
							</div>
						</div>
				</div>
				 	</tr>
				
			<?php
			}

			?>
			</table>
			</div>
			<?php
			if ($cont==0)
			{
				?>
					<div style="padding-top: 4px;">
						No tienes ningún proyectos asignados.
					</div>
				<?php
			} 
		?>
	<span class="titulo"></span>


</div>